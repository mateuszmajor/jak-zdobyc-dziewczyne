<?php

/*
Plugin Name: Page Peel
Plugin URI: http://www.avramovic.info/wordpress-plugins/page-peel-wordpress-plugin
Description: Adds page peel advertisement on your blog.
Author: Nemanja Avramovic
Version: 0.3
Author URI: http://www.avramovic.info/
Corrections: Shambix - http://www.shambix.com
*/

/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

	// detect the plugin path
	$pagepeel_path = get_option('siteurl').'/wp-content/plugins/page-peel';

	function pagepeel_addjs() { ?>
		<script type="text/javascript">
var ppimg = new Image();
ppimg.src = '<?php echo get_option(pagepeel_image_big); ?>';

var ppo = new Object();
ppo.ad_url = escape('<?php echo get_option(pagepeel_ad_url); ?>');
ppo.small_path = '<?php echo get_settings('siteurl').'/wp-content/plugins/page-peel/pp_sm.swf'; ?>';
ppo.small_image = escape('<?php echo get_option(pagepeel_image_small); ?>');
ppo.big_path = '<?php echo get_settings('siteurl').'/wp-content/plugins/page-peel/pp_big.swf'; ?>';
ppo.big_image = escape('<?php echo get_option(pagepeel_image_big); ?>');

ppo.small_width = '100';
ppo.small_height = '100';
ppo.small_params = 'ico=' + ppo.small_image;
ppo.big_width = '650';
ppo.big_height = '650';
ppo.big_params = 'big=' + ppo.big_image + '&ad_url=' + ppo.ad_url;
function sizeup987(){
	document.getElementById('ppocornerBig').style.top = '0px';
	document.getElementById('ppocornerSmall').style.top = '-1000px';
}
function sizedown987(){
	document.getElementById("ppocornerSmall").style.top = "0px";
	document.getElementById("ppocornerBig").style.top = "-1000px";
}
ppo.putObjects = function () {
document.write('<div id="ppocornerSmall" style="position:absolute;width:'+ ppo.small_width +'px;height:'+ ppo.small_height +'px;z-index:9999999;right:0px;top:0px;">');
document.write('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"');
document.write('codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0"');
document.write(' id="ppocornerSmallObject" width="'+ppo.small_width+'" height="'+ppo.small_height+'">');
document.write(' <param name="allowScriptAccess" value="always"/> ');
document.write(' <param name="movie" value="'+ ppo.small_path +'?'+ ppo.small_params +'"/>');
document.write(' <param name="wmode" value="transparent" />');
document.write(' <param name="quality" value="high" /> ');
document.write(' <param name="FlashVars" value="'+ppo.small_params+'"/>');
document.write('<embed src="'+ ppo.small_path + '?' + ppo.small_params +'" name="ppocornerSmallObject" wmode="transparent" quality="high" width="'+ ppo.small_width +'" height="'+ ppo.small_height +'" flashvars="'+ ppo.small_params +'" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>');
document.write('</object></div>');
document.write('<div id="ppocornerBig" style="position:absolute;width:'+ ppo.big_width +'px;height:'+ ppo.big_height +'px;z-index:9999999;right:0px;top:0px;">');
document.write('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"');
document.write('codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0"');
document.write(' id="ppocornerBigObject" width="'+ ppo.big_width +'" height="'+ ppo.big_height +'">');
document.write(' <param name="allowScriptAccess" value="always"/> ');
document.write(' <param name="movie" value="'+ ppo.big_path +'?'+ ppo.big_params +'"/>');
document.write(' <param name="wmode" value="transparent"/>');
document.write(' <param name="quality" value="high" /> ');
document.write(' <param name="FlashVars" value="'+ ppo.big_params +'"/>');
document.write('<embed src="'+ ppo.big_path + '?' + ppo.big_params +'" id="ppocornerBigEmbed" name="ppocornerBigObject" wmode="transparent" quality="high" width="'+ ppo.big_width +'" height="'+ ppo.big_height +'" flashvars="'+ ppo.big_params +'" swliveconnect="true" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>');
document.write('</object></div>');
setTimeout('document.getElementById("ppocornerBig").style.top = "-1000px";',1000);
}
ppo.putObjects();
</script>
<?php	} 

	// for version control and installation
	define('PAGEPEEL_VERSION', '0.3');

	// try to always get the values from the database
	$pagepeel_version = get_option(pagepeel_version);
	$pagepeel_image_small = get_option(pagepeel_image_small);
	$pagepeel_image_big = get_option(pagepeel_image_big);
	$pagepeel_ad_url = get_option(pagepeel_ad_url);

	// if the database value returns empty use defaults
	if($pagepeel_version != PAGEPEEL_VERSION)
	{
		$pagepeel_version = PAGEPEEL_VERSION; update_option('pagepeel_version', PAGEPEEL_VERSION);
		$pagepeel_image_small =  $pagepeel_path . '/small.jpg'; update_option('pagepeel_image_small', $pagepeel_image_small);
		$pagepeel_image_big = $pagepeel_path . '/big.jpg'; update_option('pagepeel_image_big', $pagepeel_image_big);
		$pagepeel_ad_url = 'http://www.cubescripts.com/add-a-comment-script.php'; update_option('pagepeel_ad_url', $pagepeel_ad_url);

	}

	function pagepeel_pages() {
	    add_options_page('Page Peel Options', 'Page Peel Options', 5, 'pagepeeloptions', 'pagepeel_options');
	}

	//print options page
	function pagepeel_options()
	{
     	global $pagepeel_path, $pagepeel_version, $pagepeel_image_small,
     		$pagepeel_image_big, $pagepeel_ad_url;

     	// if settings are updated
		if(isset($_POST['update_pagepeel']))
		{
			if(isset($_POST['pagepeel_image_small'])) {
				update_option('pagepeel_image_small', $_POST['pagepeel_image_small']);
				$pagepeel_image_small = $_POST['pagepeel_image_small'];
			}
			if(isset($_POST['pagepeel_image_big'])) {
				update_option('pagepeel_image_big', $_POST['pagepeel_image_big']);
				$pagepeel_image_big = $_POST['pagepeel_image_big'];
			}
			if(isset($_POST['pagepeel_ad_url'])) {
				update_option('pagepeel_ad_url', $_POST['pagepeel_ad_url']);
				$pagepeel_ad_url = $_POST['pagepeel_ad_url'];
			}

		}

		// if the user clicks the uninstall button, clean all options and show good-bye message
		if(isset($_POST['uninstall_pagepeel']))
		{
			delete_option(pagepeel_ad_url);
			delete_option(pagepeel_image_big);
			delete_option(pagepeel_image_small);
			delete_option(pagepeel_version);
			echo '<div class="wrap"><h2>Uninstall</h2><p>All Page Peel settings were removed and you can now go to the <a href="plugins.php">plugin menu</a> and deactivate it.</p><h3>Thank you for using Page Peel '.$pagepeel_version.'!</h3><p style="text-align:right"><small>if this happened by accident, <a href="options-general.php?page=pagepeeloptions">click here</a> to reinstall</small></p></div>';

		}
		else // show the menu
		{
			$pagepeel_version = get_option(pagepeel_version);
			$pagepeel_image_small = get_option(pagepeel_image_small);
			$pagepeel_image_big = get_option(pagepeel_image_big);
			$pagepeel_ad_url = get_option(pagepeel_ad_url);


			// if the pagepeel_version is empty or unequal,
			// write the defaults to the database
			if(trim($pagepeel_version) == '')
			{
				$pagepeel_version = PAGEPEEL_VERSION;
				$pagepeel_image_small =  $pagepeel_path . '/small.jpg';
				$pagepeel_image_big = $pagepeel_path . '/big.jpg';
				$pagepeel_ad_url = 'http://www.cubescripts.com/add-a-comment-script.php';
			}//	*/

			echo '<div class="wrap"><h2>Page Peel Options</h2><small style="display:block;text-align:right">Version: '.$pagepeel_version.'</small><form method="post" action="options-general.php?page=pagepeeloptions">';
			echo '<input type="hidden" name="update_pagepeel" value="true" />';

			echo '<table class="optiontable">';

			echo '<tr valign="top">';
			echo '<th scope="row">Page Peel Ad URL:</th>';
			echo '<td><input type="text" value="'.$pagepeel_ad_url.'" name="pagepeel_ad_url" /><br/>URL to point to when clicked on Ad (begins with <strong>http://</strong>)</td>';
			echo '</tr>';

			echo '<tr valign="top">';
			echo '<th scope="row">Page Peel small image URL:</th>';
			echo '<td><input type="text" value="'.$pagepeel_image_small.'" name="pagepeel_image_small" /><br/>URL of small image (75x75 px)</td>';
			echo '</tr>';

			echo '<tr valign="top">';
			echo '<th scope="row">Page Peel big image URL:</th>';
			echo '<td><input type="text" value="'.$pagepeel_image_big.'" name="pagepeel_image_big" /><br/>URL of "revealed" image (500x500 px)</td>';
			echo '</tr>';

			echo '</table>';
			echo '<p class="submit"><input type="submit" name="Submit" value="Update Options &raquo;" /></p>';

			echo '</form>';


			echo '<h2>Uninstall</h2><form method="post" action="options-general.php?page=pagepeeloptions">';
			echo '<input type="hidden" name="uninstall_pagepeel" value="true" />';
			echo '<p class="submit"><input type="submit" name="Submit" value="Clear Settings &raquo;" /></p>';
			echo '</form>';


			echo '<p>Remember to save your custom big and small image in this folder (your template):<br /></p>';
			echo '<p><strong>'.bloginfo('template_url').'</strong></p></div>';

		}
	}

	add_action('admin_menu', 'pagepeel_pages');
	add_action('wp_footer', 'pagepeel_addjs');


?>