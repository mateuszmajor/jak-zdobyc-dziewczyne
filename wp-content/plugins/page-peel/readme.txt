=== Page Peel ===
Contributors: Avram, Dukessa
Donate link: http://www.avramovic.info/donate
Tags: advertising, flash, page peel, banner, banners, peel, flip
Requires at least: 2.5
Tested up to: 3.0.1
Stable tag: trunk

Adds page peel to your blog.

== Description ==

This plugin adds a Web 2.0 flash Page Peel to your WP powered blog.
It preloads big image so you won't get delay before displaying image when you hover ad like in ordinary Page Peel scripts.

= BEFORE you update from a previous version, if you put your custom images in the plugin folder instead of your template folder, make sure you save them first, or they will be overwritten with the update =

== Installation ==

= BEFORE you update from a previous version, if you put your custom images in the plugin folder instead of your template folder, make sure you save them first, or they will be overwritten with the update =

1. Upload contents of `page-peel.zip` to `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Create 2 files with your custom graphics: small.jpg (75x75 pixels) and big.jpg (500x500 picels), and place them inside your template folder
4. Configure plugin through Settings/Page Peel options: add the links of your custom big.jpg and small.jpg that you previously saved in the template folder and the link where the big image points to


== Frequently Asked Questions ==

= Does it work with IE? =

From version 0.2 it should work in IE also (tested with IE8).

= Is there a way to change target of the link? =

No, the target is hardcoded in SWF file. But maybe if you reverse-engineer that file...?

= Is there a way to move the peel to the left? =

No.

== Screenshots ==

1. Page Peel Ad
2. Page Peel Ad, when hovered with mouse

== Changelog ==

= 0.1 =
* Initial release

= 0.2 =
* Moved the initalization code of plugin to the page footer. Hopefully this will make it work in IE also

= 0.3 =
* Fixed visualization bug