
        <div id="popup_domination_container" class="has-left-sidebar">
            <div style="display:none" id="popup_domination_hdn_div2"></div>
        	<form action="<?php echo $this->opts_url?>" method="post" id="popup_domination_form">
	            <div style="display:none" id="popup_domination_hdn_div"><?php echo $fields?></div>
                <?php
				$text = 'Inactive'; $class = 'inactive'; $text2 = 'Turn on.'; $class2 = 'turn-on';
				if($this->is_enabled()){
					$text = 'Active';
					$text2 = 'Turn off.';
					$class = 'active';
					$class2 = 'turn-off';
				}
				?>
                <div id="popup_domination_active">PopUp Domination is <span class="<?php echo $class ?>"><?php echo $text ?></span>. <a href="#activation" class="<?php echo $class2 ?>"><?php echo $text2 ?></a> <img class="waiting" style="display:none;" src="images/wpspin_light.gif" alt="" /></div>
            	<p id="popup_domination_tabs">
                	<a href="#look_and_feel">Look and feel</a>
                    |
                    <a href="#newsletter_form">Mailing List HTML</a>
                    |
                    <a href="#template_fields">Template Fields</a>
                    |
                    <a href="#list_items">List Points</a>
                    |
                    <a href="#schedule">Schedule</a>
                    |
                    <a href="#preview">Preview</a>
                    |
                    <a href="#promote">Promote PopUp Domination</a>
                    |
                    <a href="#advanced_view">Advanced</a>
                </p>
            	<div class="postbox" id="popup_domination_tab_look_and_feel">
                    <h3 class="hndle"><span>Look and feel</span></h3>
                    <div class="inside">
                    	<div class="inner-sidebar">
                        	<div class="postbox">
                            	<h3 class="hndle"><span>Choose a template</span></h3>
                                <div class="inside">
                                    <p>
                                        <label for="popup_domination_template"><strong>Template:</strong></label>
                                        <select id="popup_domination_template" name="popup_domination[template]"><?php echo $opts?></select>
                                    </p>
                                </div>
                            </div>
                        	<div class="postbox"<?php echo ((!isset($cur_theme['button_colors']))?'':' style="display:none"') ?> id="popup_domination_btns_container">
                            	<h3 class="hndle"><span>Choose a button color</span></h3>
                                <div class="inside">
									<?php
                                        $btns = '';
                                        $button_color = $this->option('button_color');
                                        if(isset($cur_theme['button_colors'])){
                                            foreach($cur_theme['button_colors'] as $c){
                                                $btns .= '<option value="'.$c['color_id'].'"'.(($c['color_id']==$button_color)?' selected="selected"':'').'>'.$c['name'].'</option>';
                                            }
                                        }
                                    ?>
									<p>
                                    	<label for="popup_domination_btn_color"><strong>Button Color:</strong></label>
                                        <select id="popup_domination_btn_color" name="popup_domination[button_color]"><?php echo $btns ?></select>
                                        <input type="hidden" id="popup_domination_btn_color_selected" value="<?php echo $button_color ?>" />
                                    </p>
                                </div>
                            </div>
                        	<div class="postbox"<?php echo ((!empty($opts2))?'':' style="display:none"') ?> id="popup_domination_colors_container">
                            	<h3 class="hndle"><span>Color Options</span></h3>
                                <div class="inside">
                                	<ul><?php echo $opts2 ?></ul>
                                    <input type="hidden" id="popup_domination_color_selected" name="popup_domination[color]" value="<?php echo $this->option('color')?>" />
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div id="normal-sortables">
                        	<div id="popup_domination_preview">
                            	<p class="msg">Theme &amp; Color Preview</p>
                                <?php 
									$style = '';
									if($cur_preview!=''){
										$style .= 'background-image:url(\''.$this->theme_url.$cur_preview.'\')';
										if(count($cur_size) == 2)
											$style .= ';width:'.$cur_size[0].'px;height:'.$cur_size[1].'px';
										$style = ' style="'.$style.'"';
									}
								?>
                                <div class="preview"<?php echo $style ?>></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            	<div class="postbox" id="popup_domination_tab_newsletter_form">
                    <h3 class="hndle"><span>Mailing list html</span></h3>
                    <div class="inside">
                        <div class="col">
                            <p class="msg">Enter your html opt-in code below and we'll hook up your form to the template:</p>
                            <p><textarea cols="60" rows="10" id="popup_domination_formhtml" name="popup_domination[formhtml]"><?php echo $formhtml?></textarea></p>
                    	<p><input type="checkbox" name="popup_domination[new_window]" id="popup_domination_new_window" value="Y"<?php 
							$new_window = $this->option('new_window'); 
							echo ($new_window && $new_window=='Y')?' checked="checked"':''; ?> /> <label for="popup_domination_new_window">Submit the form to a new window</label></p>
                            <p><input type="checkbox" name="popup_domination[disable_name]" id="popup_domination_disable_name" value="Y"<?php 
							$disable_name = $this->option('disable_name'); 
							echo ($disable_name && $disable_name=='Y')?' checked="checked"':''; ?> /> <label for="popup_domination_disable_name">Disable name box?</label></p>
                            <p>
                                <label for="popup_domination_name_box"><strong>Name:</strong></label>
                                <select id="popup_domination_name_box" name="popup_domination[name_box]"<?php echo ($disable_name && $disable_name=='Y')?' disabled="disabled"':''; ?>></select>
                                <input type="hidden" id="popup_domination_name_box_selected" value="<?php echo $name_box?>"<?php echo ($disable_name && $disable_name=='Y')?' disabled="disabled"':''; ?> />
                            </p>
                            <p>
                                <label for="popup_domination_email_box"><strong>Email:</strong></label>
                                <select id="popup_domination_email_box" name="popup_domination[email_box]"></select>
                                <input type="hidden" id="popup_domination_email_box_selected" value="<?php echo $email_box?>" />
                            </p>
                            <p>
                                <label for="popup_domination_action"><strong>Form URL:</strong></label>
                                <input size="60" type="text" id="popup_domination_action" name="popup_domination[action]" value="<?php echo $this->input_val($this->option('action'))?>" />
                            </p>
                        </div>
                        <div class="col descriptions">
                        	<h2>Read this before inputting your HTML</h2>
                            <p>You need to make sure the opt-in code that you have been provided contains the htmo &lt;form&gt; tag. Also make sure that it has both Name and Email address fields.</p>
                            <p>The boxes below the HTML input box should automagically detect the Name and Email address field. If they don't select the correct fields, simply choose the correct name and email address fields from the dropdown.</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="postbox" id="popup_domination_tab_template_fields">
                    <h3 class="hndle"><span>Template Text</span></h3>
                    <div class="inside"style="padding:6px 6px 8px">
                    	<p class="msg"><strong>You're using template: <span class="tpl_name"><?php echo $cur_theme['name'] ?></span></strong></p>
                    	<p class="msg"><strong>IMPORTANT:</strong> We've outlined some guideline character lengths below which will help you get the best conversion rates and more importantly, help it to look great.</p>
                    	<div class="elements">
                    	<?php echo $field_str?>
                        </div>
                    </div>
                </div>
                <div class="postbox" id="popup_domination_tab_list_items">
                    <h3 class="hndle"><span>List points</span></h3>
                    <div class="inside" id="popup_domination_listitems" style="padding:6px 6px 8px">
                    	<p id="list_allowed_size" class="msg"><strong>This theme has a limit of <span><?php echo $cur_theme['list_count'] ?></span> check boxes.</strong><br />Why is there a limit? We want to help you get the best out of your lightbox. Imposing a limit means that the design will remain beautiful and result in high conversions.</p>
                    	<ul><?php echo $listitems?></ul>
   	                    <a href="#addnew" class="button addnew">Add new</a>
                    </div>
                </div>
                <div class="postbox" id="popup_domination_tab_schedule">
                	<h3 class="hndle"><span>Schedule</span></h3>
                    <div class="inside">
                    	<p><br /><a href="#clear" class="button">Clear my cookie</a> <img class="waiting" style="display:none;" src="images/wpspin_light.gif" alt="" /></p>
                    	<p>When the close button is clicked, how long should it be before the lightbox is shown again? <input type="text" name="popup_domination[cookie_time]" value="<?php echo intval($this->option('cookie_time')) ?>" /> (recommended 7 days)</p>
                        <p><strong>When should the lightbox show?</strong></p>
                        <p>Only show the lightbox after a certain amount of impressions? <input type="text" name="popup_domination[impression_count]" value="<?php echo $this->input_val($this->option('impression_count'))?>" /></p>
                        <ul style="margin-left:30px">
                        	<li class="show_opts opt_open">
                            	<p><input type="radio" name="popup_domination[show_opt]" value="open" id="show_opt_open"<?php echo $show_opt == 'open' ? ' checked="checked"':'';?> /> <label for="show_opt_open">On Open</label></p>
                                <p style="margin-left:25px">How long should the delay be before the lightbox is shown? (seconds) <input type="text" name="popup_domination[delay]" value="<?php echo floatval($this->option('delay')) ?>"<?php echo $show_opt == 'open' ? '':' disabled="disabled"';?> /></p>
                            </li>
                            <li class="show_opts opt_mouselave">
                            	<p><input type="radio" name="popup_domination[show_opt]" value="mouseleave" id="show_opt_mouseleave"<?php echo $show_opt == 'mouseleave' ? ' checked="checked"':'';?> /> <label for="show_opt_mouseleave">When mouse leaves the site.</label></p>
                            </li>
                            <li class="show_opts opt_unload">
                            	<p><input type="radio" name="popup_domination[show_opt]" value="unload" id="show_opt_unload"<?php echo $show_opt == 'unload' ? ' checked="checked"':'';?> /> <label for="show_opt_unload">When the user tries to leave the page (This option requires a javascript alert box).</label></p>
                                <p style="margin-left:25px">Javascript alert box text <input type="text" name="popup_domination[unload_msg]" id="popup_domination_unload_msg" value="<?php echo $this->input_val($this->option('unload_msg')) ?>"<?php echo $show_opt == 'unload' ? '' : ' disabled="disabled"';?> /></p>
                            </li>
                        </ul>
                        <p><strong>Please choose where you want the lightbox to display</strong></p>
                        <?php echo $this->page_list() ?>
                    </div>
                </div>
                <div class="postbox" id="popup_domination_tab_promote">
                	<h3 class="hndle"><span>Promote PopUp Domination</span></h3>
                    <div class="inside">
                    	<p><input type="checkbox" name="popup_domination[promote]" id="popup_domination_promote" value="Y"<?php 
							$promote = $this->option('promote'); 
							echo ($promote && $promote=='Y')?' checked="checked"':''; ?> /> <label for="popup_domination_promote">Promote PopUp Domination</label></p>
                    	<p><label for="popup_domination_clickbank">Clickbank nick name:</label>
                           <input type="text" name="popup_domination[clickbank]" id="popup_domination_clickbank" value="<?php echo $this->input_val($this->option('clickbank')) ?>"<?php echo ($promote && $promote=='Y')?'':' disabled="disabled"'; ?> />
                        </p>
                    </div>
                </div>
				<div id="popup_domination_form_submit">
					<p class="submit">
						<?php wp_nonce_field('update-options'); ?>
						<input type="submit" name="update" value="<?php _e('Update options', 'popup_domination'); ?>" />
                        <span><strong>Remember:</strong> When you update, you're changes go live right away if your lightbox is Active</span>
					</p>
				</div>
            </form>
            <div class="postbox" id="popup_domination_tab_advanced_view">
                <h3 class="hndle"><span>Advanced Editor</span></h3>
                <div class="inside" style="padding:6px 6px 8px">
                <?php echo $this->get_theme_editor() ?>
                </div>
            </div>
        </div>
        
        <script type="text/javascript">
		var popup_domination_tpl_info = <?php echo $js?>, popup_domination_admin_ajax = '<?php echo admin_url('admin-ajax.php') ?>', popup_domination_theme_url = '<?php echo $this->theme_url ?>', popup_domination_form_url = '<?php echo $this->opts_url ?>';
		</script>