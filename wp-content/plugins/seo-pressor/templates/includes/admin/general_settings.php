﻿<?php /* This file encoded by Raizlabs PHP Obfuscator http://www.raizlabs.com/software */ ?>
<?php    ?>
<div class="wrap">
<h2><?php _e('General Settings')?></h2>
<?php   include( WPPostsRateKeys::$RB56A5B019A3A9A37AA3775211F83F313 . '/includes/msg.php'); ?>
<form action="" method="post">
<?php wp_nonce_field('WPPostsRateKeys-save-settings');?>
  <table class="form-table">
  	  <tr class="form-field">
  		<th><?php _e('Show SEOPressor Footer','wp-posts-rate-keys');?></th></tr><tr>
  		<td>
			<input type="checkbox" id="allow_seopressor_footer_checkbox" name="allow_seopressor_footer" value="allow_seopressor_footer" <?php echo ($R7318A606A3118D468DAE7078098FBA7B['allow_seopressor_footer']=='1')?'checked="checked"':''?> />
			<label for="allow_seopressor_footer_checkbox"><?php _e('Enable SEOPressor Attribution Link.','wp-posts-rate-keys'); ?></label>
			<br />
		</td>
  	 </tr> 
  	 <tr class="form-field">
  		<td colspan="2"><?php _e('ClickBank ID','wp-posts-rate-keys');?>:
			<input class="regular-text" type="text" id="clickbank_id" name="clickbank_id" value="<?php echo $R7318A606A3118D468DAE7078098FBA7B['clickbank_id']?>" />
			<br />
		</td>
  	 </tr> 
  	<tr class="form-field">
  		<th><?php _e('Automatic Decoration','wp-posts-rate-keys');?></th></tr><tr>
  		<td>
			<input type="checkbox" id="allow_bold_style_to_apply_checkbox" name="allow_bold_style_to_apply" value="allow_bold_style_to_apply" <?php echo ($R7318A606A3118D468DAE7078098FBA7B['allow_bold_style_to_apply']=='1')?'checked="checked"':''?> />
			<label for="allow_bold_style_to_apply_checkbox"><?php _e('Allow SEOPressor to automatically decorate your keyword with bold.','wp-posts-rate-keys'); ?></label>
			<br />
			<input type="checkbox" id="allow_italic_style_to_apply_checkbox" name="allow_italic_style_to_apply" value="allow_italic_style_to_apply" <?php echo ($R7318A606A3118D468DAE7078098FBA7B['allow_italic_style_to_apply']=='1')?'checked="checked"':''?> />
			<label for="allow_italic_style_to_apply_checkbox"><?php _e('Allow SEOPressor to automatically decorate your keyword with italic.','wp-posts-rate-keys'); ?></label>
			<br />
  			<input type="checkbox" id="allow_underline_style_to_apply_checkbox" name="allow_underline_style_to_apply" value="allow_underline_style_to_apply" <?php echo ($R7318A606A3118D468DAE7078098FBA7B['allow_underline_style_to_apply']=='1')?'checked="checked"':''?> />
			<label for="allow_underline_style_to_apply_checkbox"><?php _e('Allow SEOPressor to automatically decorate your keyword with underline.','wp-posts-rate-keys'); ?></label>
			<br />
			<input type="checkbox" id="allow_automatic_adding_alt_keyword_checkbox" name="allow_automatic_adding_alt_keyword" value="allow_automatic_adding_alt_keyword" <?php echo ($R7318A606A3118D468DAE7078098FBA7B['allow_automatic_adding_alt_keyword']=='1')?'checked="checked"':''?> />
			<label for="allow_automatic_adding_alt_keyword_checkbox"><?php _e('Allow SEOPressor to automatically add alt=&quot;keyword&quot; to all images in the content that do not have an alt tag.','wp-posts-rate-keys'); ?></label>
			<br />
			<input type="checkbox" id="allow_add_keyword_in_titles" name="allow_add_keyword_in_titles" value="allow_add_keyword_in_titles" <?php echo ($R7318A606A3118D468DAE7078098FBA7B['allow_add_keyword_in_titles']=='1')?'checked="checked"':''?> />
			<label for="allow_add_keyword_in_titles"><?php _e('Allow SEOPressor to automatically add the Keyword in Posts titles.','wp-posts-rate-keys'); ?></label>
			<br />
			<input type="checkbox" id="allow_site_wide_keyword" name="allow_site_wide_keyword" value="allow_site_wide_keyword" <?php echo ($R7318A606A3118D468DAE7078098FBA7B['allow_site_wide_keyword']=='1')?'checked="checked"':''?> />
			<label for="allow_site_wide_keyword"><?php _e('Allow Site-Wide Keyword Decorations.','wp-posts-rate-keys'); ?></label>
			<br />
			<span class="description" id="allow_site_wide_keyword_description">&nbsp;<?php _e('This is used for autoblogs only. You many enter a keyword on every new line and the plugin will automatically bold, italize and underline your keywords as and when they appear in your content. This is only applicable to autoblogs where you want a site-wide auto-decorations.','wp-posts-rate-keys'); ?></span>
			<br />
			<textarea cols="50" rows="5" name="site_wide_keywords_list" class="large-text code" id="site_wide_keywords_list"><?php echo htmlentities($R7318A606A3118D468DAE7078098FBA7B['site_wide_keywords_list']) ?></textarea>
		</td>
  	 </tr>  		
    <tr class="form-field">    	
      <th><?php _e('Select the Bold Style','wp-posts-rate-keys'); ?></th></tr><tr>
      <td><?php           $R7BBBF1B0F1D3401DCA454606CCFB0652 = 0;          foreach ($RD69D1A0481B9F396B9ECD324CA23C1D5 as $RA71358F949481BC7F4F6E21127CCBE4E) { ?>
	      		<label title="bold_style_to_apply_<?php echo $R7BBBF1B0F1D3401DCA454606CCFB0652?>"><input type="radio" name="bold_style_to_apply" <?php echo ($R7318A606A3118D468DAE7078098FBA7B['bold_style_to_apply']==$R7BBBF1B0F1D3401DCA454606CCFB0652)?'checked="checked"':''?> value="<?php echo $R7BBBF1B0F1D3401DCA454606CCFB0652?>">
	      			<?php echo htmlentities($RA71358F949481BC7F4F6E21127CCBE4E[0]) ?> - <?php echo htmlentities($RA71358F949481BC7F4F6E21127CCBE4E[1]) ?></label>
	      		<br>
	      	<?php            $R7BBBF1B0F1D3401DCA454606CCFB0652++;          } ?>
      		<span class="description">&nbsp;<?php _e('This style will be used to display the keyword in Bold','wp-posts-rate-keys'); ?></span>
      </td>
    </tr>
    <tr class="form-field">
      <th><?php _e('Select the Italic Style','wp-posts-rate-keys'); ?></th></tr><tr>
      <td>
	      <?php           $R7BBBF1B0F1D3401DCA454606CCFB0652 = 0;          foreach ($R4A1714DD6A1EC41C0CC86C93D91C3D82 as $R2219A89473D0840755273E6BE31563A8) { ?>
	      		<label title="italic_style_to_apply_<?php echo $R7BBBF1B0F1D3401DCA454606CCFB0652?>"><input type="radio" name="italic_style_to_apply" <?php echo ($R7318A606A3118D468DAE7078098FBA7B['italic_style_to_apply']==$R7BBBF1B0F1D3401DCA454606CCFB0652)?'checked="checked"':''?> value="<?php echo $R7BBBF1B0F1D3401DCA454606CCFB0652?>">
	      			<?php echo htmlentities($R2219A89473D0840755273E6BE31563A8[0]) ?> - <?php echo htmlentities($R2219A89473D0840755273E6BE31563A8[1]) ?></label>
	      		<br>
	      	<?php            $R7BBBF1B0F1D3401DCA454606CCFB0652++;          } ?>
      		<span class="description">&nbsp;<?php _e('This style will be used to display the keyword in Italic','wp-posts-rate-keys'); ?></span>
      </td>
    </tr>
    <tr class="form-field">
      <th><?php _e('Select the Underline Style','wp-posts-rate-keys'); ?></th></tr><tr>
      <td>
	      <?php           $R7BBBF1B0F1D3401DCA454606CCFB0652 = 0;          foreach ($R491D26FDAE81E72736EE0F2A17CA83B0 as $R3E5A7E9C69C3801D4166B303542E7694) { ?>
	      		<label title="underline_style_to_apply_<?php echo $R7BBBF1B0F1D3401DCA454606CCFB0652?>"><input type="radio" name="underline_style_to_apply" <?php echo ($R7318A606A3118D468DAE7078098FBA7B['underline_style_to_apply']==$R7BBBF1B0F1D3401DCA454606CCFB0652)?'checked="checked"':''?> value="<?php echo $R7BBBF1B0F1D3401DCA454606CCFB0652?>">
	      			<?php echo htmlentities($R3E5A7E9C69C3801D4166B303542E7694[0]) ?> - <?php echo htmlentities($R3E5A7E9C69C3801D4166B303542E7694[1]) ?></label>
	      		<br>
	      	<?php            $R7BBBF1B0F1D3401DCA454606CCFB0652++;          } ?>
      		<span class="description">&nbsp;<?php _e('This style will be used to underline the keyword','wp-posts-rate-keys'); ?></span>
      </td>
    </tr>
  </table>
  <h2 id="activation_settings"><?php _e('Activation Settings')?></h2>
  <table class="form-table">
    <tr class="form-field">
      <th><label for="activation_name"><?php _e('Activation Status','wp-posts-rate-keys'); ?></label></th>
      <td nowrap="nowrap"><?php echo $R961C4F63F353D58FCDDAE6142B13714A; ?></td>
    </tr>
    <tr class="form-field">
      <th><label for="clickbank-receipt-number"><?php _e('ClickBank Receipt Number','wp-posts-rate-keys'); ?></label></th>
      <td><input class="regular-text" type="text" name="clickbank_receipt_number" id="clickbank-receipt-number" value="<?php echo $R7318A606A3118D468DAE7078098FBA7B['clickbank_receipt_number']?>" />
    </tr>
  </table>
	<p class="submit">
	  <?php if ($R7318A606A3118D468DAE7078098FBA7B['active']==0) { ?>
	    <input type="submit" value="<?php _e('Activate Plugin Now') ?>" class="button-primary" name="Submit_activation" />
	    <?php }?>
	    <input type="submit" value="<?php _e('Save changes') ?>" class="button-primary" name="Submit_save_changes" />
	</p>
</form>
</div>