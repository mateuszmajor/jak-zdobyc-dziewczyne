﻿<?php /* This file encoded by Raizlabs PHP Obfuscator http://www.raizlabs.com/software */ ?>
<?php  ?>
<div class="wrap">
<div class="icon32" id="icon-plugins"><br></div>
<h2><?php _e('Connection Information for Plugin upgrade','wp-posts-rate-keys') ?></h2>
<?php include( WPPostsRateKeys::$RB56A5B019A3A9A37AA3775211F83F313 . '/includes/msg.php'); ?>
<?php   if (isset($R4EA3A43755E8A7CFD43C2DD4669D0EE1)) {  ?>
	<form action="" method="post">
	<?php wp_nonce_field('wp-posts-rate-keys-auto-upgrade');?>
	<div class="wrap">
	<p><?php _e('To perform the upgrade using FTP the follow information is required:','wp-posts-rate-keys');?></p>
	<table class="form-table">
	<tbody>
	<tr valign="top">
	<th scope="row"><label for="hostname"><?php _e('Hostname','wp-posts-rate-keys');?></label></th>
	<td><input type="text" size="40" value="<?php echo (isset($_POST['hostname']))?$_POST['hostname']:''?>" id="hostname" name="hostname"></td>
	</tr>
	<tr valign="top">
	<th scope="row"><label for="plugin_path"><?php _e('Plugins Path','wp-posts-rate-keys');?></label></th>
	<td><input type="text" size="40" value="<?php echo (isset($_POST['plugin_path']))?$_POST['plugin_path']:'/wp-content/plugins'?>" id="plugin_path" name="plugin_path">
	<br><span class="description">&nbsp;<?php _e('This is the path to the plugins folder. For example, if the FTP root is the root of WordPress installation, the path is /wp-content/plugins.','wp-posts-rate-keys'); ?></span>
	</td>
	</tr>
	<tr valign="top">
	<th scope="row"><label for="username"><?php _e('Username','wp-posts-rate-keys');?></label></th>
	<td><input type="text" size="40" value="<?php echo (isset($_POST['username']))?$_POST['username']:''?>" id="username" name="username"></td>
	</tr>
	<tr valign="top">
	<th scope="row"><label for="password"><?php _e('Password','wp-posts-rate-keys');?></label></th>
	<td><input type="password" size="40" value="<?php echo (isset($_POST['password']))?$_POST['password']:''?>" id="password" name="password"></td>
	</tr>
	<tr valign="top">
	<th scope="row"><?php _e('Connection Type','wp-posts-rate-keys');?></th>
	<td>
	<fieldset><legend class="screen-reader-text"><span><?php _e('Connection Type','wp-posts-rate-keys');?></span></legend>
	<label><input type="radio" <?php echo ( ! isset($_POST['connection_type']) || (isset($_POST['connection_type']) && $_POST['connection_type']=='ftp'))?'checked="checked"':''?> value="ftp" name="connection_type" id="ftp"> FTP</label>
	<br><label><input type="radio" <?php echo (isset($_POST['connection_type']) && $_POST['connection_type']=='ftps')?'checked="checked"':''?> value="ftps" name="connection_type" id="ftps"> FTPS (SSL)</label>
	</fieldset>
	</td>
	</tr>
	</tbody></table>
	<p class="submit">
	<input type="submit" value="Proceed" class="button" name="ftp_upgrade" id="ftp_upgrade">
	</p>
	</div>
	</form>
	<?php } ?>
</div>