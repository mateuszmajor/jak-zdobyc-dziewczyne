<?php

/**
 * Rejestracja shortcode'ów
 * 
 * ---------------------------
 * Wzór tworzenia szablonów:
 * 
 * 1. Nazwanie pliku szablonu form-nazwaszablonu
 * 2. Załadowanie pliku bazowego szablonu do katalogu includes/templates
 * 3. Nazwanie pliku CSS wg wzoru form-nazwaszablonu
 * 4. Załadowanie pliku CSS do katalogu includes/templates/css
 * 5. Zdefiniowanie nazwy szablonu w tablicy $gm_templates
 * -----------------------------------------
 */
// Zakończ, jeśli połączono bezpośrednio
if (!defined('ABSPATH'))
    exit;

// Zdefiniowanie tablicy, która będzie przechowywać szablony formularza
$gm_templates = array();
global $gm_templates;


$shortcode_id = 0;
global $shortcode_id;


/*
 * Formularz do pozyskania adresu e-mail użytkownika oraz danych dodatkowych
 * 
 * @shordcode  [getmail_form] 
 */
add_shortcode('getmail_form', 'bpmj_getmail_form_shortcode');

function bpmj_getmail_form_shortcode($attr, $content) {
    global $post;
    global $bpmj_gm_options;
    
    // Każde użycie shortcode w jednym widoku będzie miało unikatowe ID
    global $shortcode_id;
    $shortcode_id++;
    
    $post_id = $post->ID;
    //Tworzymy tablicę, która będzie przechowywać błędy
    $errors = array();
    
    
    // Włączamy funkcję sprawdzającą podstawowe dane formularza
    $errors = bpmj_gm_validate_form($shortcode_id);

    // Odebranie wartości zmienny odpowiedzialnych za nadpisanie szablonu

    $theme = array();

    // Nazwa szablonu
    if (isset($attr['theme']) && !empty($attr['theme'])) {
        $theme['theme'] = $attr['theme'];
    }

    // Nagłówek formularza
    if (isset($attr['title']) && !empty($attr['title'])) {
        $theme['title'] = $attr['title'];
    }

    // Nagłówek formularza
    if (isset($attr['description']) && !empty($attr['description'])) {
        $theme['description'] = $attr['description'];
    }


    // Sprawdza, czy podano wymagany atrybut lists
    if (isset($attr['lists'])) {


        //Sprawdza, czy atrybut lists ma prawidłową formę
        if (bpmj_gm_formatted_attr_lists($attr['lists']) == false) {

            // Komunikat o konieczności wyboru listy jako atrybut shordcode
            $wrong_text = '<span class="bpmj_gm_errors">' . __('Błąd shortcode [getmail_form] - nieprawidłowy format atrybutu', 'bpmj_gm') . '</span>';
            return $wrong_text;
        } else {
            // Tworzymy tablicę z ID list do zapisu
            $lists = bpmj_gm_formatted_attr_lists($attr['lists']);
        }
    } else {
        // Komunikat o konieczności wyboru listy w atrybucie shordcode
        $wrong_text = '<span class="bpmj_gm_errors">' . __('Błąd shortcode [getmail_form] - nie podano id listy jako atrybut.', 'bpmj_gm') . '</span>';
        return $wrong_text;
    }

    
    // Sprawdza, czy wejście jest z linku, wysłanego e-mailem. 
    // Jeżeli tak, tworzy sesję zeswalającą na odblokowanie formularza, gdy pokrywają się listy
    echo bpmj_gm_enter_from_link();

    // Formatowanie danych wejściowych 

    /*
     *  Jeżeli użytkownik zapisał się na newsletter, jego dane zostaną
     * zapisane do bazy i wyswietli się komunikat sukcesu. W przeciwnym wypadku,
     * zostanie wyświetlony formularz
     */
        if (isset($_POST['bpmj_gm_sc_id']) && $_POST['bpmj_gm_sc_id'] == $shortcode_id && empty($errors)) {

            //Jeżeli należy potwierdzić adres e-mail
            if (isset($bpmj_gm_options['email_confirm']) && $bpmj_gm_options['email_confirm'] == 'on') {


                //Zapisz dane do bazy. Przekazanie list, na które zostanie zapisany e-mail
                bpmj_gm_save_email($lists);
  
                // Tworzy i wysyła e-maila w celu potwierdzenia
                bpmj_gm_send_verification_email($post->ID, $attr['lists'], $content);
                
                // Wyświetl tekst
                $confirm_text = __('Sprawdź skrzynkę pocztową i potwierdź adres e-mail! ', 'bpmj_gm');

                $success_text = '<span class="bpmj_alert_success">' . $bpmj_gm_options['success_text'] . ' <b>' . $confirm_text . '</b></span>';


                return $success_text;
                
            } else { // Gdy nie trzeba potwierdzać e-maila
                
                //Zapisz dane do bazy. Przekazanie list, na które zostanie zapisany e-mail
                bpmj_gm_save_email($lists);
                
                // Wysyła link odblokowujący wszytskie formularze z listami, na które zapisał się użytkownik.
                bpmj_gm_sent_article_link($lists, array('post_id' => $post_id, 'content' => $content));
 
                
                // Wyświetl tekst
                $success_text = '<span class="bpmj_alert_success">' . $bpmj_gm_options['success_text'] . '</span>';
                
                // Przechowanie w sesji informacji o listach, na które e-mail został zapisany oraz o potwierdzeniu
                
                $_SESSION['confirm'][] = FALSE;
                
                foreach ($lists as $list){
                    $_SESSION['lists'][] = $list;
                }
                
                // Zaczep po prawidłowym wysłaniu e-maila ( bez potwierdzenia )
                $email_name = filter_var($_POST['bpmj_gm_email'], FILTER_VALIDATE_EMAIL) ? $_POST['bpmj_gm_email'] : '';
                if (empty($email_name))
                return $success_text . $content;
                
                $email_id = bpmj_gm_get_email_id_by_name($email_name);
                
                do_action('bpmj_gm_after_save_email_no_confirm', $email_id);
                
                return $success_text . $content;
            }
        } else {

            // Sprawdzamy, które listy są w sesji. jeżeli listy z sesji pokrywają się z listami z atrybutów shortcode, pokaże się treść.
            if (isset($_SESSION['lists'])) {
                
                // Sprawdza, czy listy z shortcode i sesji się pokrywają
                $inters = array_intersect($_SESSION['lists'], $lists);
                
                // Gdy pokrywa się przynajmniej jedna lista, odblokuj formularz
                if(count($inters) > 0){
                    
                    // Gdy wymaga potwierdzenia, wyświetl, formularz
                    if(isset($bpmj_gm_options['email_confirm']) && $bpmj_gm_options['email_confirm'] == 'on'){
                        
                        // Gdy e-mail został potwierdzony i utworzonona została sesja
                        if(isset($_SESSION['confirm']) && $_SESSION['confirm'] === TRUE){
                            return $content;
                        }else{
                             return bpmj_gm_get_template_form($lists, $errors, $theme, $shortcode_id);
                        }
                        
                    }
                    
                    return $content;
                    
                }else{
                    // Wyswietl formularz
                return bpmj_gm_get_template_form($lists, $errors, $theme, $shortcode_id);
                }
                
               
            } else {
                // Wyswietl formularz
                return bpmj_gm_get_template_form($lists, $errors, $theme, $shortcode_id);
            }
        }
}

?>