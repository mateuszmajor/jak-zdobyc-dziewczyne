<?php

/*
 * Pozwala zapisać nowe dane przy tworzeniu listy
 */
add_filter('bpmj_gm_additional_lists', 'bpmj_gm_add_autoresponer_form_do_list');

function bpmj_gm_add_autoresponer_form_do_list($add_list) {

    // Jeśli wpisano dane autorespondera, zapisz je do bazy
    if (isset($_POST['bpmj_gm_list_autoresponder']) && isset($_POST['bpmj_gm_list_autoresponder'])) {

        $form = htmlentities($_POST['bpmj_gm_list_autoresponder'], ENT_COMPAT, 'UTF-8');

        $form_data = array('autoresponder' => array(
                'form' => $form,
        ));

        $form_data = serialize($form_data);

        if (isset($add_list['outer_lists'])) {
            $add_list['outer_lists'] = unserialize($add_list['outer_lists']);
            $add_list['outer_lists'] = serialize(array_merge($add_list['outer_lists'], $form_data));
        } else {
            $add_list['outer_lists'] = $form_data;
        }

        return $add_list;
    } else {
        return $add_list;
    }
}
