<?php

/*
 * Inicjujemy sesję
 */
add_action('init', 'trade_start_session');

function trade_start_session() {
    if (!session_id()) {
        session_start();
    }
}

/*
 * Wyświetla wybrany szablon formularza
 * 
 * @params:
 * $lists - ID list ( jednej lub wielu) string
 * $errors - komunikaty błędu ( array )
 * $theme - informacje potrzebne do nadpisania szablonu ( array )
 * $shortcode_id - id przypisane do shortcode w celu rozróżnienia wielu formularzy w jednym widoku - int
 * @return string aktywny szablon formularza
 */

function bpmj_gm_get_template_form($lists, $errors = '', $theme, $shortcode_id) {
    global $bpmj_gm_options;

    // Definiuje potencjalne błędy
    if (!empty($errors)) {
        $errors = $errors;
    } else {
        $errors = '';
    }

    // Definiujemy zmienną $form. W pliku szablonu, który zostanie wczytany budowana jest
    // zawartośc formularza w tej zmiennej.
    $form = '';

    // Pobieranie Nagłówka formularza
    if (isset($theme['title']) && !empty($theme['title'])) {
        // Jezeli jest podany nagłówke w shordcode - nadpisz go
        $form_title = $theme['title'];
    } else {
        // Jeżeli jest w usatwiona w shordcode jedna lista, pobierz jej ustawienia
        if (count($lists) == 1) {
            $title = bpmj_get_list_title($lists[0]);
            if ($title !== FALSE) {
                // Jezeli jest podany nagłówke w ustawieniach listy - nadpisz go
                $form_title = $title;
            } else {
                $form_title = $bpmj_gm_options['form_title'];
            }
        } else {
            // Domyślny nagłówek
            $form_title = $bpmj_gm_options['form_title'];
        }
    }

    // Pobieranie opisu formularza
    if (isset($theme['description']) && !empty($theme['description'])) {
        $form_description = $theme['description'];
    } else {
        // Jeżeli jest w usatwiona w shordcode jedna lista, pobierz jej ustawienia
        if (count($lists) == 1) {
            $description = bpmj_get_list_description($lists[0]);
            if ($description !== FALSE) {
                // Jezeli jest podany opis formularza w ustawieniach listy - nadpisz go
                $form_description = $description;
            } else {
                $form_description = $bpmj_gm_options['form_caption'];
            }
        } else {
            // Domyślny nagłówek
            $form_description = $bpmj_gm_options['form_caption'];
        }
    }

    // Pobieranie szablonu formularza

    if (isset($theme['theme']) && !empty($theme['theme'])) {

        // Sprawdza czy taki theme istnieje
        $themes = bpmj_gm_get_themes();
        $theme_ok = FALSE;
        foreach ($themes as $name) {
            if ($theme['theme'] == $name) {
                $theme_ok = TRUE;
            }
        }

        if ($theme_ok) {
            $theme_slug = $theme['theme'];
        } else {
            return '<span class="bpmj_gm_errors">' . __('Nie ma takiego szablonu formularza. Popraw shordcode.') . '<br /></span>';
        }
    } else {
        if (count($lists) == 1) {
            // Sprawdza, czy przypisano szablon do tej lity
            $theme = bpmj_get_list_theme($lists[0]);
            if ($theme !== FALSE) {
                $theme_slug = $theme;
            } else {
                $theme_slug = $bpmj_gm_options['form_theme'];
            }
        } else {
            $theme_slug = $bpmj_gm_options['form_theme'];
        }
    }

    // Sprawdza, czy lusty z shortcode pokrywają się z rzeczywistymi listami
    $lists_exists = bpmj_gm_is_lists_exists($lists);

    if (!$lists_exists) {
        return '<span class="bpmj_gm_errors">' . __('<b>Nie można wyświetlić formularza!</b> Shortcode zawiera listy, które nie zostały zdefiniowane w GetMail.') . '<br /></span>';
    }

    // Wczytanie odpowiedniego pliku z kodem formularza
    require_once(BPMJ_GM_TEMPLATES . '/form-' . $theme_slug . '.php');

    return $form;
}

/*
 * Pobiera nazwę szablonu przypisaną do listy
 */

function bpmj_get_list_theme($list_id) {

    global $wpdb;
    global $bpmj_gmdb_lists;

    $query = "SELECT theme FROM $bpmj_gmdb_lists WHERE list_id = '" . $list_id . "'";
    $theme = $wpdb->get_var($query);

    if (empty($theme)) {
        return FALSE;
    }
    return $theme;
}

/*
 * Pobiera nagłówek formularza przypisany do listy
 */

function bpmj_get_list_description($list_id) {

    global $wpdb;
    global $bpmj_gmdb_lists;

    $query = "SELECT description FROM $bpmj_gmdb_lists WHERE list_id = '" . $list_id . "'";
    $description = $wpdb->get_var($query);

    if (empty($description)) {
        return FALSE;
    }
    return $description;
}

/*
 * Pobiera opis formularza przypisany do listy
 */

function bpmj_get_list_title($list_id) {

    global $wpdb;
    global $bpmj_gmdb_lists;

    $query = "SELECT title FROM $bpmj_gmdb_lists WHERE list_id = '" . $list_id . "'";
    $title = $wpdb->get_var($query);

    if (empty($title)) {
        return FALSE;
    }
    return $title;
}

/*
 * Wyświetla błędy podczas walidacji formularza
 * 
 * @return string gdy wykryje błędy
 */

function bpmj_gm_print_form_errors($errors) {

    if (is_array($errors) && !empty($errors)) {
        foreach ($errors as $error) {
            return '<span class="bpmj_gm_errors">' . $error . '<br /></span>';
        };
    } else {
        return;
    }
}

/*
 * Sprawdza poprawność podstawowych pól formularza
 * @params int - unikatowy ID shordcote
 * @return array $errors or bool TRUE
 */

function bpmj_gm_validate_form($shortcode_id) {

    $errors = array();

    // Sprawdzamy który formularz jest obsługiwany
    if (isset($_POST['bpmj_gm_sc_id']) && $_POST['bpmj_gm_sc_id'] == $shortcode_id)

    // Sprawdzamy poprawność adresu e-mail
        if (isset($_POST['bpmj_gm_email'])) {
            if (!is_email($_POST['bpmj_gm_email'])) {
                $errors[] = __('Niepoprawny adres e-mail', 'bpmj_gm');
            }
        }

    return $errors;
}

/*
 * Sprawdza, czy adres e-mail jest już w bazie
 * 
 */

function bpmj_gm_is_duplicate($email_id = '') {

    if (isset($email_id) && !empty($email_id)) {
        $email = $email_id;
    } else {
        $email = sanitize_text_field($_POST['bpmj_gm_email']);
    }
    //Sprawdzamy, czy jest już w bazie
    $mails_exist = bpmj_gm_get_id_by_email($email);

    if (count($mails_exist) > 0) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/*
 * Funja sprawdza, czy e-mail jest już zapisany na liste
 */

function bpmj_gm_email_has_lists($email_id) {

    global $wpdb;
    global $bpmj_gmdb_rel;

    $query = "SELECT email_id FROM $bpmj_gmdb_rel WHERE email_id = '" . $email_id . "'";
    $emails = $wpdb->get_col($query);

    if (count($emails) > 0) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/*
 * Zapisuje do bazy adres e-mail i pola dodatkowe
 * 
 */

function bpmj_gm_save_email($lists) {

    global $wpdb;
    global $bpmj_gmdb; // nazwa tabeli z emialiami
    global $bpmj_gmdb_rel; // nazwa tabeli z relacjami
    global $bpmj_gm_options;

    // Przygotowanie danych z formularza
    $email = sanitize_text_field($_POST['bpmj_gm_email']);
    $details = '';


    // IMIĘ
    if (isset($_POST['bpmj_gm_fname'])) {
        $fname = sanitize_text_field($_POST['bpmj_gm_fname']);
    } else {
        $fname = '';
    }

    // NAZWISKO
    if (isset($_POST['bpmj_gm_lname'])) {
        $lname = sanitize_text_field($_POST['bpmj_gm_lname']);
    } else {
        $lname = '';
    }

    // ULICA I NUMER
    if (isset($_POST['bpmj_gm_street'])) {
        $street = sanitize_text_field($_POST['bpmj_gm_street']);
    } else {
        $street = '';
    }

    // MIEJSCOWOŚĆ
    if (isset($_POST['bpmj_gm_city'])) {
        $city = sanitize_text_field($_POST['bpmj_gm_city']);
    } else {
        $city = '';
    }

    // KOD POCZTOWY
    if (isset($_POST['bpmj_gm_post_code'])) {
        $post_code = sanitize_text_field($_POST['bpmj_gm_post_code']);
    } else {
        $post_code = '';
    }

    // TELEFON
    if (isset($_POST['bpmj_gm_phone'])) {
        $phone = sanitize_text_field($_POST['bpmj_gm_phone']);
    } else {
        $phone = '';
    }




    // Tablica w oczyszczonymi danymi z formularza
    $details_data = array(
        'fname' => $fname,
        'lname' => $lname,
        'street' => $street,
        'city' => $city,
        'post_code' => $post_code,
        'phone' => $phone,
    );

    // Przygotowanie tablicy do zapisu
    $details = serialize($details_data);


    // Ustawia status e-maila ( pobiera dane z zainstalowanych autoresponderów za pomoca filtra )
    $status_raw = array();

    // Filtr, dzięki któremu autoresponder może dopisać swój status podczas zapisu e-maila.
    $status = apply_filters('bpmj_gm_set_status', $status_raw);

    // Dane do zapisu w tabeli z emailami
    $data = array(
        'date_add' => current_time('timestamp'),
        'email' => $email,
        'details' => $details,
        'status' => serialize($status)
    );

    // Gdy adres e-mail jest wysłany po raz pierwszy, zapisz dane do bazy
    if (!bpmj_gm_is_duplicate($email)) {
        // Zapis adresu e-mail z danymi do bazy
        $wpdb->insert($bpmj_gmdb, $data);
    } else {
        // Gdy e-mail jest już w bazie, zresetuj tylko status dla autoresponderów
        $data = array(
            'status' => serialize($status)
        );

        // Gdy wymaga potwierdzenia, nic nie rób na tym etapie
        if (!isset($bpmj_gm_options['email_confirm']) || $bpmj_gm_options['email_confirm'] != 'on')
            $wpdb->update($bpmj_gmdb, $data, array('email' => $email));
    }

    // Pobranie ID emaila
    $email_id = bpmj_gm_get_id_by_email($email);


    // Przygotowanie do zapisu do tabeli relacyjnej
    foreach ($lists as $list_id) {

        // Sprawdza, czy e-mail został już przypisany do list. Jeżeli tak doda nowe listy
        if (bpmj_gm_email_has_lists($email_id)) {

            $lists_names = bpmj_gm_get_default_lists($email_id);

            $lists_ids = bpmj_gm_conbert_list_name_to_id($lists_names);

            $on_list = FALSE;

            foreach ($lists_ids as $email_list) {
                if ($email_list == $list_id) {
                    // Sprawdz, czy email jest już przypisany do tej listy
                    $on_list = TRUE;
                }
            }

            // Gdy e-mail nie był przypisany do tej listy, zdrób to teraz.
            if ($on_list === FALSE) {
                $data_rel = array(
                    'email_id' => $email_id,
                    'list_id' => $list_id
                );

                $wpdb->insert($bpmj_gmdb_rel, $data_rel);
            }
        } else {
            // Przygotowanie danych relacyjnych do zapisu

            $data_rel = array(
                'email_id' => $email_id,
                'list_id' => $list_id
            );
            $wpdb->insert($bpmj_gmdb_rel, $data_rel);
        }
    }

    // Zaczep akcji dla rozszerzeń. Przekazuje listy, na które e-mail został zapisany.
    do_action('bpmj_gm_after_email_save', $lists);
}

/*
 * Funckcja zamienia tablicę z nazwami list na tablicę z ID list
 * 
 * @param
 * @ array
 */

function bpmj_gm_conbert_list_name_to_id($lists_names) {

    global $wpdb;
    global $bpmj_gmdb_lists;

    if (is_array($lists_names)) {

        $lists_id = array();

        foreach ($lists_names as $names) {

            $query = "SELECT list_id FROM $bpmj_gmdb_lists WHERE name = '" . $names->name . "'";
            $results = $wpdb->get_var($query);

            $lists_id[] = $results;
        }

        return $lists_id;
    } else {
        return FALSE;
    }
}

/*
 * Potwierdzenie adresu e-mail
 * 
 * @params 
 *      $post_id - id wpisu, z którego jest wysyłany formularz
 */

function bpmj_gm_send_verification_email($post_id, $lists, $content) {

    global $wpdb;
    global $bpmj_gmdb; // nazwa tabeli
    global $bpmj_gm_options;

    // Pobranie e-maila
    $email = sanitize_text_field($_POST['bpmj_gm_email']);

    //Pobranie daty dodania
    $date_add = bpmj_gm_get_date_add_by_email($email);

    //Pobranie id emaila
    $email_id = bpmj_gm_get_id_by_email($email);


    // Jeżeli e-mail jest w koszu, nie wysyłaj linka potwierdzającego
    if (bpmj_gm_is_trash($email_id)) {
        return;
    }

    // Jeżeli e-mail jest już aktywowany, nie wysyłaj linka z aktywacją
//    if(bpmj_gm_get_activated_email_date($email_id) !== FALSE){
//        return;
//    }

    $add_params = '';

    // Spradwza, czy formularz odblokowuje treść, czy nie
    if (isset($content) || !empty($content)) {
        $add_params .= '&amp;c=1';
    }

    // Dodanie ID posta jako parametr
    $add_params .= '&amp;p=' . $post_id;

    // Tworzy token md5( NONCE_KEY + $date_add + $email + $email_id + $lists )
    $token = md5(NONCE_KEY . $date_add . $email . $email_id . $lists);

    // Tworzy link aktywacyjny
    $url = get_permalink($post_id) . '?id=' . $email_id . '&amp;lists=' . $lists . '&amp;bpmj_gm_confirm_token=' . $token . $add_params;


    // Pobranie tekstu zamiast linka
    if (isset($bpmj_gm_options['anchor_link_activation']) && !empty($bpmj_gm_options['anchor_link_activation'])) {
        $anchor = $bpmj_gm_options['anchor_link_activation'];
    } else {
        $anchor = __('Link aktywacyjny', 'bpmj_gm');
    }

    // Pobranie adresu e-mail nadawcy
    if (isset($bpmj_gm_options['email_adress_sender']) && !empty($bpmj_gm_options['email_adress_sender'])) {
        $email_sender = $bpmj_gm_options['email_adress_sender'];
    } else {
        $email_sender = get_bloginfo('admin_email');
    }

    // Pobranie nazwy nadawcy
    if (isset($bpmj_gm_options['name_sender']) && !empty($bpmj_gm_options['name_sender'])) {
        $sender_name = $bpmj_gm_options['name_sender'];
    } else {

        $admin_email = get_bloginfo('admin_email');
        $admin = get_user_by('email', $admin_email);
        $sender_name = $admin->data->display_name;
    }

    // Pobranie tytułu wiadomości
    if (isset($bpmj_gm_options['title_activation_email']) && !empty($bpmj_gm_options['title_activation_email'])) {
        $subject_email = $bpmj_gm_options['title_activation_email'];
    } else {
        $subject_email = __('Potwierdzenie adresu e-mail', 'bpmj_gm') . ' [' . get_bloginfo('name') . '] ';
    }


    $link = '<a href="' . $url . '">' . $anchor . '</a>';

    // Przygotowanie treści wiadomości
    $content = $bpmj_gm_options['email_message'];
    $body = str_replace("%link%", $link, $content);

    // Zastąpienie pustych lini znacznikami <p>
    $body = wpautop($body);

    // Funkcja wysyłająca. Przygotowanie content_type do odbioru html
    add_filter('wp_mail_content_type', create_function('', 'return "text/html"; '));
    $headers = 'From: ' . $sender_name . ' <' . $email_sender . '>' . "\r\n";
    $subject = $subject_email;
    wp_mail($email, $subject, $body, $headers);
}

/*
 *   Funkcja formatuje atrybut lists z shordcode [getmail_form]
 * 
 * Wartośc atrybutu powinna mieć formę num,num,num, czyli numery odzielone przecinkami
 * 
 *  @return Array lub FALSE
 */

function bpmj_gm_formatted_attr_lists($lists_raw) {

    $lists = explode(',', $lists_raw);

    foreach ($lists as $value) {
        if (!is_numeric($value)) {
            return FALSE;
        }
    }

    return $lists;
}

/*
 *   Zwraca datę doadania emaila
 * 
 */

function bpmj_gm_get_date_add_by_email($email) {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT date_add FROM $bpmj_gmdb WHERE email = '" . $email . "'";
    $date_add = $wpdb->get_var($query);

    return $date_add;
}

/*
 * Zlicza emaile, które znajdują się w koszu
 */

function bpmj_gm_counter_trasu_items() {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT id FROM $bpmj_gmdb WHERE trash = '1'";
    $trash = $wpdb->get_col($query);

    $number = count($trash);

    return $number;
}

/*
 * Zlicza emaile, które nie znajdują się w koszu
 */

function bpmj_gm_counter_items() {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT id FROM $bpmj_gmdb WHERE trash = '0'";
    $trash = $wpdb->get_col($query);

    $number = count($trash);

    return $number;
}

/*
 *   Zwraca ID emaila zapisanego w bazie po podaniu jego nazwy
 * 
 */

function bpmj_gm_get_id_by_email($email) {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT id FROM $bpmj_gmdb WHERE email = '" . $email . "'";
    $email_id = $wpdb->get_var($query);

    return $email_id;
}

/*
 * Zwraca ID listy po podaniu jej nazwy
 * 
 */

function bpmj_gm_get_list_id_by_name($name) {

    global $wpdb;
    global $bpmj_gmdb_lists;

    $query = "SELECT list_id FROM $bpmj_gmdb_lists WHERE name = '" . $name . "'";
    $list_id = $wpdb->get_var($query);

    return $list_id;
}

/*
 *   Zwraca nazwę listy po podaniu jej ID
 * 
 */

function bpmj_gm_get_list_name_by_id($list_id) {

    global $wpdb;
    global $bpmj_gmdb_lists;

    $query = "SELECT name FROM $bpmj_gmdb_lists WHERE list_id = '" . $list_id . "'";
    $list_name = $wpdb->get_var($query);

    return $list_name;
}

/*
 *   Zwraca nazwę emaila po podaniu jego ID
 * 
 */

function bpmj_gm_get_email_name_by_id($email_id) {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT email FROM $bpmj_gmdb WHERE id = '" . $email_id . "'";
    $email_name = $wpdb->get_var($query);

    return $email_name;
}

/*
 *   Zwraca nazwę emaila po podaniu jego ID
 * 
 */

function bpmj_gm_get_email_id_by_name($email_name) {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT id FROM $bpmj_gmdb WHERE email = '" . $email_name . "'";
    $email_id = $wpdb->get_var($query);

    return $email_id;
}

/*
 *   Zwraca date dodania emaila po podaniu jego ID
 * 
 */

function bpmj_gm_get_email_adddate_by_id($email_id) {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT date_add FROM $bpmj_gmdb WHERE id = '" . $email_id . "'";
    $date_add = $wpdb->get_var($query);

    return $date_add;
}

/*
 * Funckcja usuwa e-maila z bazy danych wraz z jego powiązaniami
 * 
 */

function bpmj_gm_del_email($email_id) {

    global $wpdb;
    global $bpmj_gmdb;
    global $bpmj_gmdb_rel;

    // Usuwa z tabeli bpmj_gm_mails
    $wpdb->delete($bpmj_gmdb, array('id' => $email_id));

    // Usuwa wpisy w tabeli relacyjnej
    $wpdb->delete($bpmj_gmdb_rel, array('email_id' => $email_id));
}

/*
 *   Sumuje ilość wszystkich emaili zapisanych na listę
 * 
 * @params ID listy wewnetrznej
 */

function bpmj_gm_get_amount_emails_of_list($list_id) {

    global $wpdb;
    global $bpmj_gmdb_rel;

    $query = "SELECT email_id FROM $bpmj_gmdb_rel WHERE list_id = '" . $list_id . "'";
    $emails_id = $wpdb->get_col($query);

    $acitive_emails = array();

    foreach ($emails_id as $email_id) {
        if (!bpmj_gm_is_trash($email_id)) {
            $acitive_emails[] = $email_id;
        }
    }

    $total_emails = count($acitive_emails);



    return $total_emails;
}

/*
 *  Sprawdza, czy email jest usunięty
 */

function bpmj_gm_is_trash($email_id) {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT id FROM $bpmj_gmdb WHERE id='$email_id' AND trash='1'";
    $removed = $wpdb->get_var($query);

    if (!empty($removed)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/*
 *   Formularza usuwania listy z bazy
 * 
 * @param ID listy do usunięcia
 */

function bpmj_gm_get_delete_list_form($list_id) {

    $form = '<form class="bpmj_gm_form_del_list search-form" method="post" action="' . bpmj_gm_current_url() . '">';
    $form .= '<input class="button button-primary submit" type="submit" value="' . __('Usuń', 'bpmj_gm') . '">';
    $form .= wp_nonce_field('bpmj_gm_lists_del_check', 'bpmj_gm_lists_del_check_nonce', true, false);
    $form .= '<input type="hidden" value="' . $list_id . '" name="bpmj_gm_delate_list_check">';
    $form .= '</form>';

    return $form;
}

/*
 *  Sprawdza, czy lista o podanym id jest usunięta
 * 
 * @param ID listy
 */

function bpmj_gm_get_check_if_removed($list_id) {

    global $wpdb;
    global $bpmj_gmdb_lists;

    $query = "SELECT removed FROM $bpmj_gmdb_lists WHERE list_id = '" . $list_id . "'";
    $removed = $wpdb->get_var($query);

    if ($removed == 1) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/*
 *  Pobiera wszystkie ID zapisanych w bazie list z wykluczeniem usuniętych
 * 
 * @return array
 */

function bpmj_gm_get_lists() {

    global $wpdb;
    global $bpmj_gmdb_lists;
    $query = "SELECT list_id FROM $bpmj_gmdb_lists WHERE removed = '0'";
    $ids = $wpdb->get_col($query);

    return $ids;
}

/*
 *  Pobiera wszystkie ID zapisanych w bazie e-maili
 * 
 * @return array
 */

function bpmj_gm_get_emails($list_id, $sort = 'DESC', $confirm = 'all') {

    global $wpdb;
    global $bpmj_gmdb;
    global $bpmj_gmdb_rel;

    if ($list_id != FALSE) {
        // Pobierz maile przypisane do konkretnej listy
        $query = "SELECT email_id FROM $bpmj_gmdb_rel WHERE list_id = '" . $list_id . "' ORDER BY object_id $sort";
        $ids_rel = $wpdb->get_col($query);

        $ids = array();

        // Sprawdza, czy e-mail jest usunięty
        foreach ($ids_rel as $email_id) {

            $trash = bpmj_gm_is_trash($email_id);

            if ($trash == FALSE) {
                $ids[] = $email_id;
            }
        }
    } else {
        $query = "SELECT id FROM $bpmj_gmdb WHERE trash = '0' ORDER BY date_add $sort";
        $ids = $wpdb->get_col($query);
    }


    if (is_array($ids)) {
        $con_i = 0;
        foreach ($ids as $email_id) {

            // Gdy ma zwrócić tylko potwierdzone emaile
            if ($confirm == 'confirm') {
                if (bpmj_gm_get_activated_email_date($email_id) === FALSE) {
                    unset($ids[$con_i]);
                }
            }elseif($confirm == 'no-confirm'){
               if (bpmj_gm_get_activated_email_date($email_id) !== FALSE) {
                    unset($ids[$con_i]);
                } 
            }


            $con_i++;
        }
    }


    return $ids;
}

/*
 *  Pobiera ID emaila znajdujących się w koszu
 * 
 */

function bpmj_gm_get_emails_trash() {
    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT id FROM $bpmj_gmdb WHERE trash = '1'";
    $trash = $wpdb->get_col($query);

    return $trash;
}

/*
 *  Sprawdza, czy zlecono filtracje emaili przez liste w panelu na stronie z emailami
 * 
 * @return string ID listy lub false
 */

function bpmj_gm_sorted_by_list() {

    // Jeśli zlecono filtracje
    if (isset($_POST['bpmj_gm_emails_sort']) && is_numeric($_POST['bpmj_gm_emails_sort'])) {

        $sort = $_POST['bpmj_gm_emails_sort'];

        return $sort;
    } else {
        return FALSE;
    }
}

/*
 *   Zwraca nazwy list wewnetrznych przypisanych do e-maila
 * 
 */

function bpmj_gm_get_default_lists($email_id) {

    global $wpdb;
    global $bpmj_gmdb;
    global $bpmj_gmdb_rel;
    global $bpmj_gmdb_lists;


    $query = "SELECT name FROM $bpmj_gmdb_lists INNER JOIN $bpmj_gmdb_rel WHERE $bpmj_gmdb_lists.list_id = $bpmj_gmdb_rel.list_id AND $bpmj_gmdb_rel.email_id = '" . $email_id . "'";

    $email_name = $wpdb->get_results($query);

    return $email_name;
}

/*
 *   Pobiera date potwierdzenia adresu e-mail
 * 
 */

function bpmj_gm_get_activated_email_date($email_id, $timestamp = FALSE) {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT confirm FROM $bpmj_gmdb WHERE id = '" . $email_id . "'";
    $confirm = $wpdb->get_var($query);





    if (!empty($confirm)) {
        if ($timestamp) {
            return $confirm;
        } else {
            return date_i18n('j M. Y - G:i', $confirm);
        }
    } else {
        return FALSE;
    }
}

/*
 *   Pobiera dodatkowe dane przypisane do e-maila
 * 
 */

function bpmj_gm_get_additional_email_data($email_id) {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT details FROM $bpmj_gmdb WHERE id = '" . $email_id . "'";
    $data = $wpdb->get_var($query);

    if (!empty($data)) {
        $data = unserialize($data);

        $empty = 0;

        foreach ($data as $field) {
            if (empty($field)) {
                $empty++;
            }
        }

        if ($empty == 6) {
            return FALSE;
        }

        return $data;
    } else {
        return FALSE;
    }
}

/*
 *   Pobiera status e-maila w postaci tablicy
 *  
 *  @params, emial ID oraz nazwa autorespondera.
 *  Jeżeli nie poda się autorespondera, funkcja zwóci surowy wynik ( całą zawartośc wiersza )
 */

function bpmj_gm_get_status($email_id, $autoresponder = '') {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT status FROM $bpmj_gmdb WHERE id = '" . $email_id . "'";
    $status = $wpdb->get_var($query);

    $output = unserialize($status);

    if (!empty($autoresponder)) {

        if (isset($output[$autoresponder])) {
            return $output[$autoresponder];
        } else {
            return FALSE;
        }
    } else {
        return $output;
    }
}

/*
 *   Pobiera notatkę przypisaną do e-maila.
 * 
 */

function bpmj_gm_get_note($email_id) {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT note FROM $bpmj_gmdb WHERE id = '" . $email_id . "'";
    $note = $wpdb->get_var($query);

    if (!empty($note)) {
        $output = unserialize($note);
    } else {
        $output = array();
    }

    return $output;
}

/*
 * Wyświetla wszystkie notatki przypisane do e-maila
 * Gdy podamy nazwe autorespondera, pobierze notatki tylko do niego przypisane
 */

function bpmj_gm_print_sending_log($email_id, $autoresponder = '') {

    global $wpdb;
    global $bpmj_gmdb; // nazwa tabeli z emialiami

    $note = bpmj_gm_get_note($email_id);

    if (!empty($note)) {

        echo '<div class="bpmj_gm_print_status">';
        echo '<h3>' . __('Listy zewnętrzne', 'bpmj_hm') . '</h3>';

        // Gdy nie podamy autorespondera, pobiera wszytskie logi
        if (!empty($autoresponder)) {
            foreach ($note as $value) {
                echo '<ul>';
                foreach ($value as $one_note) {
                    echo '<li>' . $one_note . '</li>';
                }
                echo '<ul>';
            }

            // W ynnym przypadku pobiera tylko noitatki przypisane do autorespondera    
        } else {
            echo '<ul>';
            foreach ($note[$autoresponder] as $one_note) {
                echo '<li>' . $one_note . '</li>';
            }
            echo '<ul>';
        }

        echo '</div>';
    } else {
        return;
    }
}

/*
 * Funkcja odpowiedzialna za zmianę statusu emaila i zapisanie komunikatu
 * 
 * @params
 * requred $email_id
 * required $status - Status, na jaki chcemy zmienić. 
 * Dostępne statusy dla autorespondera -  'new', 'sent', 'error'
 * Dostępne statusy globalne - trash
 * $autoresponder - nazwa autorespondera, np netmailer
 */

function bpmj_gm_set_status($email_id, $status, $autoresponder = '') {

    global $wpdb;
    global $bpmj_gmdb;

    // Gdy nie podano autorespondera, zmień status globalny.
    // jedyny status globalny to 'trash'
    if ($autoresponder == '' && $status == 'trash') {
        // Kolumny do aktualizacji
        $data = array(
            'trash' => 1,
            'note' => ''
        );
        $wpdb->update($bpmj_gmdb, $data, array('id' => $email_id));
        return;
    }

    // Pobranie obecnego statusu
    $status_before = bpmj_gm_get_status($email_id);

    // Ustawienie nowego statusu dla danego autorespondera
    $status_before[$autoresponder] = $status;

    // W przypadku zmiany statusu na 'new', czyści wszystkie notatki przypisane do tego autorespondera
    if ($status == 'new') {

        // Kolumny do aktualizacji
        $status = array(
            'status' => serialize($status_before),
            'note' => ''
        );
    } else {

        // Kolumny do aktualizacji
        $status = array(
            'status' => serialize($status_before)
        );
    }

    $wpdb->update($bpmj_gmdb, $status, array('id' => $email_id));
}

/*
 * Dodaje notatkę do emaila ( potrzebna do informacji 
 * np o niepowodzeniu podczas wysyłki do autorespondera )
 * 
 */

function bpmj_gm_set_note($email_id, $note, $autoresponder) {

    global $wpdb;
    global $bpmj_gmdb;

    // Pobranie obecnej notatki. Gdy nie istnieje zwróci pusta tablicę
    $note_before = bpmj_gm_get_note($email_id);

    $note_before[$autoresponder][] = $note;

    // Dane do aktualizacji
    $note = array(
        'note' => serialize($note_before)
    );

    $wpdb->update($bpmj_gmdb, $note, array('id' => $email_id));
}

/*
 *  Wyświetla formularz, odpowiedzialny za usuwanie e-maili
 */

function bpmj_gm_del_email_form($email_id) {

    $name = bpmj_gm_get_email_name_by_id($email_id);

    $form = '<form onclick="return confirm(\'E-mail ' . $name . ' zostanie całkowicie usunięty z systemu. Jesteś pewien, że chcesz to zrobić?\')" class="bpmj_gm_form" method="post" action="' . bpmj_gm_current_url() . '">';
    $form .= wp_nonce_field('bpmj_gm_email_del_check', 'bpmj_gm_email_del_check_nonce', true, false);
    $form .= '<input type="hidden" value="' . $email_id . '" name="bpmj_gm_email_del">';
    $form .= '<input class="button button-primary submit" type="submit" value="' . __('Usuń', 'bpmj_gm') . '">';
    $form .= '</form>';

    return $form;
}

/*
 * Wyświetla formularz, odpowiedzialny za usuwanie e-maili z listy
 */

function bpmj_gm_refresh_email_form($email_id) {

    $form = '<form class="bpmj_gm_form" method="post" action="' . bpmj_gm_current_url() . '">';
    $form .= wp_nonce_field('bpmj_gm_return_email_form_check', 'bpmj_gm_return_email_form_check_nonce', true, false);
    $form .= '<input type="hidden" value="' . $email_id . '" name="bpmj_gm_return_email_form">';
    $form .= '<input title="Odświeżenie powoduje wysyłkę e-maila do wszystkich przypisanych autoresponderów" class="button button-primary submit" type="submit" value="' . __('Wyślij', 'bpmj_gm') . '">';

    $form .= '</form>';

    return $form;
}

/*
 * Sprawdza, czy są syłączone wszytskie dodatkowe pola w formularzo zapisu e-maila
 * 
 */

function bpmj_gm_only_email_field() {
    global $bpmj_gm_options;

    if (isset($bpmj_gm_options['form_address'])) {
        $adres = $bpmj_gm_options['form_address'];
        foreach ($adres as $item) {
            if ($item == 'on') {
                return FALSE;
            }
        }
        return TRUE;
    } else {
        return TRUE;
    }
}

/*
 * Pobiera nazwy wszytskich aktywnych szablonów
 */

function bpmj_gm_get_themes() {

    $dir = BPMJ_GM_TEMPLATES;

    // Otwarcie kalogu templates i pobranie nazw szablonów
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== FALSE) {

                if ($file != 'css' && $file != '.' && $file != '..') {
                    $name = str_replace(array('form-', '.php'), '', $file);
                    $select_list[$name] = $name;
                }
            }
            closedir($dh);
        }
    }

    if (is_array($select_list)) {
        return $select_list;
    } else {
        return false;
    }
}

/*
 * Sprawdza, czy parametr w linku publikującym ukrytą treść posta jest prawidłowy
 */

function bpmj_gm_enter_from_link() {


    if (isset($_GET['bpmj_gm_token']) && isset($_GET['lists']) && isset($_GET['email'])) {

        // Pobranie i oczyszczenie tokena
        $token_out = sanitize_text_field($_GET['bpmj_gm_token']);

        // Pobranie i oczyszczenie list
        $lists_out = sanitize_text_field($_GET['lists']);

        // Pobranie e-maila
        $email = sanitize_text_field($_GET['email']);

        if (strlen($token_out) != 32)
            return FALSE;

        // Tworzenie klucza dla porównanania z zewnętrznym
        // TOKEN SKŁADA SIĘ Z ( NONCE_KEY + ID LIST PO PRZECINKU )
        // Utworzenie prywatnego klucza do artykułu
        $token_home = md5(NONCE_KEY . $lists_out . $email);

        if ($token_home === $token_out) {

            // Zamiana list na tablice
            $lists = bpmj_gm_formatted_attr_lists($lists_out);

            // Popbiera ID emaila
            $email_id = bpmj_gm_get_id_by_email($email);

            // Sprawdza, czy e-mail jest potwierdzony
            if (bpmj_gm_get_activated_email_date($email_id) === FALSE) {
                $_SESSION['confirm'] = FALSE;
            } else {
                $_SESSION['confirm'] = TRUE;
            }


            // Utworzenie sesji pozwalającej na odblokowanie formularzy z listami zawartymi w tablicty GET "lists"
            foreach ($lists as $list) {
                $_SESSION['lists'][] = $list;
            }

            $_SESSION['lists'] = array_unique($_SESSION['lists']);

            return;
        } else {
            return FALSE;
        }
    } else {
        return FALSE;
    }
}

/*
 * Funcja wyświetla formularz edycji nazwy, opisu i szablonu listy w widoku list.
 * 
 */

function bpmj_gm_print_list_edit_form($list_id) {

    // Gdy zmieniono lub dodano dane, aktualizuje je
    bpmj_gm_update_list_details($list_id);

    // Pobiera szablon
    $theme = bpmj_get_list_theme($list_id);

    // Pobiera tytuł formularza
    $title = bpmj_get_list_title($list_id);
    if ($title === false) {
        $title = '';
    }

    // Pobiera opis formularza
    $caption = bpmj_get_list_description($list_id);
    if ($caption === false) {
        $caption = '';
    }

    $output = '<div class="bpmj_gm_list_edit">';
    $output .= '<form class="bpmj_edit_list_form" action="' . bpmj_gm_current_url() . '" method="POST">';

    $output .= '<h3>' . __('Edycja listy', 'bpmj_gm') . '</h3>';

    // SZABLON
    $output .= '<div class="bpmj_gm_theme_form_edit">';
    $output .= '<label>' . __('Szablon', 'bpmj_gm') . '</label>';
    $output .= '<select name="bpmj_gm_list_theme_field">';
    $output .= '<option value="none">' . __('domyślny') . '</option>';
    $themes = bpmj_gm_get_themes();
    foreach ($themes as $value => $name) {
        $output .= '<option ' . selected($theme, $value, false) . ' value="' . $value . '">' . $name . '</option>';
    }
    $output .= '</select>';
    $output .= '</div>';

    // TYTUŁ
    $output .= '<div class="bpmj_gm_title_form_edit">';
    $output .= '<label>' . __('Tytuł formularza', 'bpmj_gm') . '</label>';
    $output .= '<input type="text" name="bpmj_gm_list_title_field" value="' . $title . '" />';
    $output .= '</div>';

    // OPIS 
    $output .= '<label>' . __('Opis formularza', 'bpmj_gm') . '</label>';
    $output .= '<div class="bpmj_gm_caption_form_edit">';
    $output .= '<textarea name="bpmj_gm_list_caption_field">' . $caption . '</textarea>';
    $output .= '</div>';

    // POZOSTAŁE ELEMENTY
    $output .= '<input type="hidden" name="bpmj_gm_edit_list_check">';
    $output .= '<input type="hidden" value="' . $list_id . '" name="bpmj_gm_edit_list_id">';
    $output .= wp_nonce_field('bpmj_gm_edit_list', 'bpmj_gm_edit_list_wp_nonce', true, false);
    $output .= '<input class="button button-primary submit" type="submit" value="Zmień">';
    $output .= '</form>';
    $output .= '</div>';

    return $output;
}

/*
 * Wyświetla informacje pomocnicze przy tworzeniu shordcode
 * 
 */

function bmpj_gm_print_shortcodes_help($list_id) {

    $output = '<div class="bpmj_gm_shortcodes_help">';
    $output .= '<h3>' . __('Przykłady użycia shortcode dla listy: ', 'bpmj_gm') . bpmj_gm_get_list_name_by_id($list_id) . '</h3>';
    $output .= '<ul>';
    $output .= '<li>' . __('Formularz zapisu na listę', 'bpmj_gm') . ' - <span class="bpmj_gm_code">[getmail_form lists="' . $list_id . '"]</span> ' . __('W przypadku zapisu na więcej list, podajemy ich ID po przecinku.', 'bpmj_gm') . '</li>';
    $output .= '<li>' . __('Formularz zapisu na listę z zablokowaną treścią', 'bpmj_gm') . ' - <span class="bpmj_gm_code">[getmail_form lists="' . $list_id . '"]' . __('Treść widoczna po wypełnieniu formularza', 'bpmj_gm') . '[/getmail_form]</span></li>';
    $output .= '</ul>';
    $output .= '<h4>' . __('Dodatkowe atrybuty nadpisują poprzednie ustawienia.', 'bpmj_gm') . '</h4>';
    $output .= '<ul>';
    $output .= '<li>' . __('Atrybut', 'bpmj_gm') . ' - <span class="bpmj_gm_code">theme</span> - ' . __('Nadpisuje szablon formularza. Jako wartośc należy podac nazwę szablonu.', 'bpmj_gm') . '</li>';
    $output .= '<li>' . __('Atrybut', 'bpmj_gm') . ' - <span class="bpmj_gm_code">title</span> - ' . __('Nadpisuje tytuł formularza.', 'bpmj_gm') . '</li>';
    $output .= '<li>' . __('Atrybut', 'bpmj_gm') . ' - <span class="bpmj_gm_code">description</span> - ' . __('Nadpisuje opis formularza.', 'bpmj_gm') . '</li>';
    $output .= '</ul>';
    $output .= '</div>';

    return $output;
}

/*
 * Funkcja odpowiedzialna za wysłanie e-maila z linkiem oblokowującym formularze
 * Opcja do włączenia w ustawieniach generalnych.
 */

function bpmj_gm_sent_article_link($lists, $args = array()) {

    global $bpmj_gm_options;

    // Wysyłaj e-maila z linkiem tylko dla tych artykułów, które mają ukryta treść w shordcode
    if (!isset($args['content']) || empty($args['content']))
        return;

    if (!isset($bpmj_gm_options['email_content_link']) || $bpmj_gm_options['email_content_link'] != 'on')
        return;

    // Gdy link wysyłany jest zaraz po przesłaniu formularza, pobierz e-maila z tablicy POST
    if (isset($_POST['bpmj_gm_email'])) {
        $email = sanitize_text_field($_POST['bpmj_gm_email']);
    } else { // Gdy link wysyłany po aktywacji e-maila, pobierz e-maila z 
        if (isset($args['email'])) {
            $email = $args['email'];
        }
    }


    // Pobiera TOKEN postu
    $key = NONCE_KEY;

    // Tworzys string z ID list
    $lists = implode(',', $lists);

    // TOKEN SKŁADA SIĘ Z ( NONCE_KEY + ID LIST PO PRZECINKU + $email )
    // Utworzenie prywatnego klucza do artykułu
    $token_link = md5(NONCE_KEY . $lists . $email);

    // Tworzy link aktywacyjny
    if (!isset($args['post_id'])) {
        $url = get_bloginfo('url') . '?bpmj_gm_token=' . $token_link . '&amp;lists=' . $lists . '&amp;email=' . $email;
    } else {
        $url = get_permalink($args['post_id']) . '?bpmj_gm_token=' . $token_link . '&amp;lists=' . $lists . '&amp;email=' . $email;
    }

    $link = '<a href="' . $url . '">' . __('Link.', 'bpmj_gm') . '</a>';

    // Przygotowanie treści wiadomości
    if (!isset($args['post_id'])) {
        $content = __('Po kliknięciu w link zostaną odblokowane niektóre treści na stronie  ', 'bpmj_gm') . get_bloginfo('url') . ' - ' . $link;
    } else {
        $content = __('Po kliknięciu w link zostanie odblokowany artykuł: ', 'bpmj_gm') . get_the_title($args['post_id']) . ' - ' . $link;
    }

    // Pobranie adresu e-mail nadawcy
    if (isset($bpmj_gm_options['email_adress_sender']) && !empty($bpmj_gm_options['email_adress_sender'])) {
        $email_sender = $bpmj_gm_options['email_adress_sender'];
    } else {
        $email_sender = get_bloginfo('admin_email');
    }

    // Pobranie nazwy nadawcy
    if (isset($bpmj_gm_options['name_sender']) && !empty($bpmj_gm_options['name_sender'])) {
        $sender_name = $bpmj_gm_options['name_sender'];
    } else {

        $admin_email = get_bloginfo('admin_email');
        $admin = get_user_by('email', $admin_email);
        $sender_name = $admin->data->display_name;
    }

    // Utworzenie tytułu wiadomości
    if (isset($bpmj_gm_options['title_unblock_email']) && !empty($bpmj_gm_options['title_unblock_email'])) {
        $subject_email = $bpmj_gm_options['title_unblock_email'];
    } else {
        $subject_email = '[' . get_bloginfo('name') . '] ' . __('Oblokowanie treści - ', 'bpmj_gm');
    }



    // Funkcja wysyłająca. Przygotowanie content_type do odbioru html
    add_filter('wp_mail_content_type', create_function('', 'return "text/html"; '));
    $headers = 'From: ' . $sender_name . ' <' . $email_sender . '>' . "\r\n";
    $subject = $subject_email;
    wp_mail($email, $subject, $content, $headers);
}

/*
 * Pobiera zawartość kolumny TEMP w tabeli bpmj_gm_mails
 *  
 * @params
 * $email_id = int ID emiala
 * $autoresponder = string - nazwa autorespondera
 */

function bpmj_gm_get_temp_data($email_id, $autoresponder = '') {

    global $wpdb;
    global $bpmj_gmdb;

    $query = "SELECT temp FROM $bpmj_gmdb WHERE id = '" . $email_id . "'";
    $temp = $wpdb->get_var($query);

    if (isset($temp) && !empty($temp)) {

        if (!isset($autoresponder) || empty($autoresponder)) {

            return unserialize($temp);
        } else {

            $temp = unserialize($temp);

            if (isset($temp[$autoresponder])) {
                return $temp[$autoresponder];
            } else {
                return array();
            }
        }
    } else {
        return array();
    }
}

/*
 * Zapisuje dane do kolumny TEMP
 * 
 * *@params
 * $email_id = int ID emiala
 * $autoresponder = string - nazwa autorespondera
 * $data = array - dane do zapisu
 */

function bpmj_gm_save_temp($email_id, $autoresponder, $data) {

    global $wpdb;
    global $bpmj_gmdb;

    // Pobranie zawartości 
    $existing_data = bpmj_gm_get_temp_data($email_id);

    if (isset($existing_data) && !empty($existing_data)) {

        $all_data = array_merge($existing_data, array($autoresponder => $data));

        $data_base = array(
            'temp' => serialize($all_data)
        );
    } else {
        $data_base = array(
            'temp' => serialize(array($autoresponder => $data))
        );
    }

    $wpdb->update($bpmj_gmdb, $data_base, array('id' => $email_id));
}

/*
 * Funkcja sprawdza, czy ID list podane w shortcode pokrywają się z rzeczywistymi listami
 * 
 * @params - $lists, array
 * @return - bool, 
 */

function bpmj_gm_is_lists_exists($lists) {

    $gm_lists = bpmj_gm_get_lists();

    // Sprawdza, czy utworzono listy w GM
    if (!isset($gm_lists) || empty($gm_lists)) {

        // Nie utworzono list w GM. 
        return FALSE;
    }


    foreach ($lists as $list_id) {

        // Sprawdza, czy ID listy z shortcode zostało utworzone po stronie GM
        if (!in_array($list_id, $gm_lists)) {
            return FALSE;
        }
    }

    return TRUE;
}

/*
 * Aktualizuje formularz autorespondera przypisany do listy.( widok list-> listy zewnętrzne )
 * 
 */

function bpmj_gm_form_outer_lists_update() {

    if (isset($_POST['bpmj_gm_autoresponder_check'])) {

        global $wpdb;
        global $bpmj_gmdb_lists;

        $form = htmlentities($_POST['bpmj_gm_autoresponder_form'], ENT_COMPAT, 'UTF-8');
        $list_id = sanitize_text_field($_POST['bpmj_gm_autoresponder_list_id']);

        //Sprawdzam nonce
        if (!isset($_POST['bpmj_gm_autoresponder_check_wp_nonce'])) {
            return;
        }

        $nonce = $_POST['bpmj_gm_autoresponder_check_wp_nonce'];

// Sprawdź poprawnośc nonce
        if (!wp_verify_nonce($nonce, 'bpmj_gm_autoresponder_check_wp')) {
            return;
        }

        // Pobiera wszytskie listy zewnetrzne przypisane do e-maila
        $query = "SELECT outer_lists FROM $bpmj_gmdb_lists WHERE list_id = '" . $list_id . "'";
        $outer_lists = $wpdb->get_col($query);

        if ($outer_lists != NULL) {

            foreach ($outer_lists as $lists) {
                $un_list = unserialize($lists);

                //Nadpisuje nowe wartości
                $un_list['autoresponder']['form'] = $form;

                $data = array(
                    'outer_lists' => serialize($un_list)
                );
                // Zapisje do bazy
                $wpdb->update($bpmj_gmdb_lists, $data, array('list_id' => $list_id));
            }
        } else {

            //Dodaje nową wartość

            $un_list['autoresponder']['form'] = $form;

            $data = array(
                'outer_lists' => serialize(array(
                    'autoresponder' => array(
                        'form' => $form,
                    )
                ))
            );
            // Zapisje do bazy
            $wpdb->update($bpmj_gmdb_lists, $data, array('list_id' => $list_id));
        }
    } else {
        return;
    }
}

/*
 *   Zwraca kod HTML formularza zewnętrznego autorespindera po podaniu ID listy
 * 
 */

function bpmj_gm_get_autoresponder_list_html($list_id) {

    global $wpdb;
    global $bpmj_gmdb_lists;

    $query = "SELECT outer_lists FROM $bpmj_gmdb_lists WHERE list_id = '" . $list_id . "'";
    $outer_lists = $wpdb->get_col($query);

    $autoresponder = NULL;
    if ($outer_lists != NULL) {

        foreach ($outer_lists as $lists) {
            $un_list = unserialize($lists);
            if (isset($un_list['autoresponder'])) {
                $autoresponder = $un_list['autoresponder'];
            }
        }
    }

    return $autoresponder;
}

/*
 * Zapisuje e-maila do MAIL POET
 */

function bpmj_gm_add_subscriber_to_mailpoet($args) {


    if (!isset($args['wysija[user][email]']) || !isset($args['wysija[user_list][list_ids]'])) {
        return;
    }

    $email_id = bpmj_gm_get_id_by_email($args['wysija[user][email]']);

    $user_data = array(
        'email' => $args['wysija[user][email]'],
    );

    $data_subscriber = array(
        'user' => $user_data,
        'user_list' => array('list_ids' => array($args['wysija[user_list][list_ids]']))
    );


    // Dodaje subskrybenta do MailPoet
    $helper_user = WYSIJA::get('user', 'helper');
    $helper_user->addSubscriber($data_subscriber);

    // Dodatnie komunikatu błędu
    $note = '<b class="bpmj_gm_success_note">Zapisano w Mail Poet</b> - ' . current_time('d.m.Y, H:i');
    $note = serialize($data_subscriber);
    bpmj_gm_set_note($email_id, $note, 'autoresponder');
}

function bpmj_gm_current_url() {
 $pageURL = 'http';
 if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if (isset($_SERVER["SERVER_PORT"]) && $_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}

?>