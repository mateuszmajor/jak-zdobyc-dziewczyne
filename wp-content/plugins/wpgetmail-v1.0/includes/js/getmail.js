

// Walidacja formularza
jQuery.noConflict();
(function($) {

    $(document).ready(function() {

        $('.bpmj_gm_form input[type="submit"]').click(function() {
            var valid = true;
            
            
            var form = $(this).parent().parent().parent();
            var inputs = $(this).parent().parent().parent().find('.required');
            $(inputs).each(function() {
                
                if ($(this).val() === '' ||
                        $(this).val() === 'Twój adres email' ||
                        $(this).val() === 'Imię' ||
                        $(this).val() === 'Nazwisko' ||
                        $(this).val() === 'Ulica z numerem' ||
                        $(this).val() === 'Miejscowość' ||
                        $(this).val() === 'Kod pocztowy' ||
                        $(this).val() === 'Telefon'
                        ) {
                    $(this).addClass('invalid');
                    if ($(this).parent().find('.bpmj_gm_erroro_field_message').length === 0) {
                        $(this).parent().append('<span class="bpmj_gm_erroro_field_message">To pole jest obowiązkowe!</span>');
                    }
                    valid = false;
                } else {
                    
                    // Walidacja e-mail
                    var email_field = form.find('input[name="bpmj_gm_email"]');
                    var email_field_val = form.find('input[name="bpmj_gm_email"]').val();
                    
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    var email = regex.test(email_field_val);
                    if (!email) {
                        if (email_field.parent().find('.bpmj_gm_erroro_field_message_email').length === 0) {
                            email_field.parent().append('<span class="bpmj_gm_erroro_field_message_email">Niepoprawny adres e-mail!</span>');

                        }
                        email_field.addClass('invalid');
                        valid = false;
                    } else {
                         email_field.removeClass('invalid');
                        email_field.parent().find('.bpmj_gm_erroro_field_message_email').remove();
                    }
                    
                    $(this).removeClass('invalid');
                    $(this).parent().find('.bpmj_gm_erroro_field_message').remove();
                }
            });





            if (!valid) {
                return false;
            }
        });


    });







})(jQuery);