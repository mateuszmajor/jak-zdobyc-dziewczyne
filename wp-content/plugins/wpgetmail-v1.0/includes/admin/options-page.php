<div class="wrap bpmj_gm_options">

    <form id="bpmj-gm-plugin-base-form" action="options.php" method="POST">

        <?php settings_fields('bpmj_getmail') ?>
        <?php do_settings_sections('bpmj_gm-options-subpage') ?>

        <input type="submit" class="button button-primary" value="<?php _e("Save", 'bpmj_gm'); ?>" />
    </form> <!-- end of #ahsttemplate-form -->
</div>

