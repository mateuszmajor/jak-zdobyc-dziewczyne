<?php
global $wpdb;
global $bpmj_gmdb;
global $bpmj_gmdb_rel;
global $bpmj_gmdb_lists;

// Zaczep przed załadowaniem się html strony z listami
do_action('bpmj_gm_before_list_page_print');
?>

<div class="wrap">
    <h2 style="margin-bottom: 30px;"><?php _e("Zarządzanie listami wewnętrznymi", 'bpmj_gm'); ?></h2>

    <table cellspacing="0" class="emails widefat fixed comments">
        <style>
            .emails th, .emails td 
            {padding-left: 15px!important;}
        </style>
        <thead>
            <tr>
                <th class="manage-column check-column"  scope="col"><?php _e('ID', 'bpmj_gm') ?></th>
                <th class="manage-column" scope="col"><?php _e('Nazwa', 'bpmj_gm') ?></th>
                <th class="manage-column" scope="col"><?php _e('Szczegóły', 'bpmj_gm') ?></th>
                <th class="manage-column" scope="col"><?php _e('Listy zewnętrzne', 'bpmj_gm') ?></th>
                <th class="manage-column column-100" scope="col"><?php _e('Shortcode', 'bpmj_gm') ?></th>
                <th class="manage-column column-100" scope="col"><?php _e('Akcje', 'bpmj_gm') ?></th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th class="manage-column check-column"  scope="col"><?php _e('ID', 'bpmj_gm') ?></th>
                <th class="manage-column" scope="col"><?php _e('Nazwa', 'bpmj_gm') ?></th>
                <th class="manage-column" scope="col"><?php _e('Szczegóły', 'bpmj_gm') ?></th>
                <th class="manage-column" scope="col"><?php _e('Listy zewnętrzne', 'bpmj_gm') ?></th>
                <th class="manage-column column-100" scope="col"><?php _e('Shortcode', 'bpmj_gm') ?></th>
                <th class="manage-column column-100" scope="col"><?php _e('Akcje', 'bpmj_gm') ?></th>
            </tr>
        </tfoot>

        <tbody data-wp-lists="list:comment" id="the-comment-list">
            <?php
// Pobierz wszytskie listy

            $ids = bpmj_gm_get_lists();

            $count = 0;
            foreach ($ids as $list_id) {
                if (!bpmj_gm_get_check_if_removed($list_id)) {
                    // Tworzenie tabeli
                    echo '<tr>';

                    // KOLUMNA 1 -  ID
                    echo '<td><b>' . $list_id . '</b></td>';

                    // KOLUMNA 2 -  NAZWA
                    echo '<td><b>' . bpmj_gm_get_list_name_by_id($list_id) . '</b> (' . bpmj_gm_get_amount_emails_of_list($list_id) . ')</td>';

                    // KOLUMNA 3 -  Informacje o liście
                    echo '<td>';
                    // Pobiera szablon
                    $theme = bpmj_get_list_theme($list_id);
                    if($theme !== FALSE){
                    echo '<b>' . __('Szablon', 'bpmj_gm') . '</b> - ' . $theme;
                    
                    }
                    
                    
                    // Pobiera tytuł formularza
                    $title = bpmj_get_list_title($list_id);
                    if ($title === false) {
                        $title = '';
                    }

                    // Pobiera opis formularza
                    $caption = bpmj_get_list_description($list_id);
                    if ($caption === false) {
                        $caption = '';
                    }
                    echo '</td>';

                    // KOLUMNA 4 -  Listy zewnętrzne
                    echo '<td><b>';
                    // Zaczep dla dodatków. Miejsce na dodatkowe listy
                    do_action('bpmj_gm_outer_lists', $list_id);
                    echo '</b></td>';

                    // KOLUMNA 5 -  Shortcodes
                    echo '<td><button class="bpmj_gm_shortcode_btn button button-primary submit">' . __('Pokaż', 'bpmj_gm') . '</button></td>';

                    // KOLUMNA 6 -  Akcje
                    echo '<td>';
                    echo bpmj_gm_get_delete_list_form($list_id);
                    echo '<button class="bpmj_gm_settings_list_button button button-primary submit dashicons dashicons-admin-generic"></button>';
                    echo '</td>';
                    echo '</tr>';

                    // ROZWIJANY OBSZAR EDYCJI
                    echo '<tr class="bpmj_gm_edit_area"><td colspan="6">';
                    echo bpmj_gm_print_list_edit_form($list_id);
                    echo '</td></tr>';
                    
                    // ROZWIJANY OBSZAR EDYCJI
                    echo '<tr class="bpmj_gm_shortcode_area"><td colspan="6">';
                    echo bmpj_gm_print_shortcodes_help($list_id);
                    echo '</td></tr>';
                }
                $count++;
            }
            ?>
        </tbody>

    </table>
    <div class="bpmj_gm_form_wrap">
        <form class="search-form" method="post" action="<?php echo bpmj_gm_current_url(); ?>">
            <div class="form-field bpmj_gm_add_list">
<?php
// Zaczep do wyświetlania informacji o błędach przy zapisie listy
do_action('bpmj_gm_validate_list_print');
?>
                <h3><?php _e('Dodaj nową listę', 'bpmj_gm') ?></h3>
                <div class="bpmj_gm_input_wrap">
                    <label for="bpmj_gm_list"><?php _e('Nazwa', 'bpmj_gm') ?>*</label>
                    <input type="text" size="40" class="regular-text" value="" id="bpmj_gm_list" name="bpmj_gm_list">
                </div>
                <h4><?php _e('Ustawienia wyglądu', 'bpmj_gm') ?></h4>
                <div class="bpmj_gm_input_wrap">
                    <label for="bpmj_gm_list_theme"><?php _e('Szablon', 'bpmj_gm') ?></label>
<?php
// Pobiera zainstalowane szablony i wyświetla ich nazwy w selekcie
$themes = bpmj_gm_get_themes();
echo '<select name="bpmj_gm_list_theme">';
echo '<option  value="default">' . __('domyślny', 'bpmj_gm') . '</option>';
foreach ($themes as $value => $name) {
    echo '<option  value="' . $value . '">' . $name . '</option>';
}
echo '</select>';
?>
                </div>
                <div class="bpmj_gm_input_wrap">
                    <label for="bpmj_gm_list_title"><?php _e('Nagłówek formularza', 'bpmj_gm') ?></label>
                    <input type="text" size="100" value="" id="bpmj_gm_list_title" name="bpmj_gm_list_title">
                </div>
                <div class="bpmj_gm_input_wrap">
                    <label for="bpmj_gm_list_description"><?php _e('Opis formularza', 'bpmj_gm') ?></label>
                    <textarea size="100" id="bpmj_gm_list_description" name="bpmj_gm_list_description">
                    </textarea>
                </div>
                <div class="bpmj_gm_autoresponder_list">
                    <span class="bpmj_gm_ar_show"><?php _e('Dodaj autoresponder', 'bpmj_gm') ?> +</span>
                    <div class="bpmj_gm_autoresponder_area">
                        <div class="bpmj_gm_input_wrap">
                    <label for="bpmj_gm_list_autoresponder"><?php _e('Wklej kod HTML formularza autorespondera', 'bpmj_gm') ?></label>
                    <textarea size="100" id="bpmj_gm_list_description" name="bpmj_gm_list_autoresponder"></textarea>
                </div>
                    </div>
                       
                </div>
                
<?php
// Zaczep dla dodatków. Miejsce na dodatkowe pola formularza
do_action('bpmj_gm_outer_lists_inputs');
?>
                <div class="bpmj_gm_input_wrap bpmj_gm_tar">
                    <input class="button button-primary submit" type="submit" value="<?php _e('Dodaj listę', 'bpmj_gm') ?>">
                </div>
            </div>
            
<?php wp_nonce_field('bpmj_gm_lists_check', 'bpmj_gm_lists_check_nonce'); ?>
        </form>
    </div>
</div>
