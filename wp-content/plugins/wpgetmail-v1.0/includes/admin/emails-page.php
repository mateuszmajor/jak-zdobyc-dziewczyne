<?php
global $wpdb;
global $bpmj_gmdb;
global $bpmj_gmdb_rel;
global $bpmj_gmdb_lists;

// Zaczep pozwalający obsługiwać np formularze.
do_action('bpmj_gm_before_email_page_print');
?>
<div class="wrap">
    <h2 style="margin-bottom: 30px;"><?php _e("Lista zapisanych emaili", 'bpmj_gm'); ?></h2>

        <form class="bpmj_gm_left" action="<?php echo bpmj_gm_current_url(); ?>" method="post">
            <select class="postform" name="bpmj_gm_emails_sort">
                <option value=""><?php _e("Wszytskie", 'bpmj_gm'); ?></option>
                <?php
                $lists = bpmj_gm_get_lists();
                foreach ($lists as $list_id) {
                    if (bpmj_gm_sorted_by_list() == $list_id) {
                        $list_selected = 'SELECTED';
                    } else {
                        $list_selected = '';
                    }
                    echo '<option value="' . $list_id . '"' . $list_selected . '>' . bpmj_gm_get_list_name_by_id($list_id) . '</option>';
                }
                ?>
            </select>

            <input type="submit" value="<?php _e("Przefiltruj", 'bpmj_gm'); ?>" class="button" id="post-query-submit" name="">
        </form>


<!--    <span class="bpmj_gm_trash_link"><?php //_e("Suma zapisanych e-maili: ", 'bpmj_gm'); ?> (<?php //echo bpmj_gm_counter_items(); ?>)</span>-->

    <form class="bpmj_gm_right" action="<?php echo bpmj_gm_current_url(); ?>" method="post">
        <span class="bpmj_gm_export_head">Eksportuj do CSV</span>
        <select class="postform" name="bpmj_gm_csv_export_list_confirm">
            <option value="confirm"><?php _e("Potwierdzone", 'bpmj_gm'); ?></option>
            <option value="no-confirm"><?php _e("Niepotwierdzone", 'bpmj_gm'); ?></option>
            <option value="all"><?php _e("Wszystkie e-maile", 'bpmj_gm'); ?></option>
        </select>
        <select class="postform" name="bpmj_gm_csv_export_list">
            <option value="0"><?php _e("Wszystkie listy", 'bpmj_gm'); ?></option>
            <?php
            $lists = bpmj_gm_get_lists();
            foreach ($lists as $list_id) {
                if (bpmj_gm_sorted_by_list() == $list_id) {
                    $list_selected = 'SELECTED';
                } else {
                    $list_selected = '';
                }
                echo '<option value="' . $list_id . '"' . $list_selected . '>' . bpmj_gm_get_list_name_by_id($list_id) . '</option>';
            }
            ?>
        </select>
        <span class="button bpmj_gm_download_csv">
        <input type="submit" value="<?php _e("Pobierz", 'bpmj_gm'); ?>" class="bpmj_gm_no-formai-input" name="bpmj_gm_csv_export">
        </span>
        </form>
    <div class="bpmj_gm_clear"></div>

    <table cellspacing="0" class="emails widefat fixed comments">
        <style>
            .emails th, .emails td 
            {padding-left: 15px!important;}
        </style>
        <thead>
            <tr>
                <th class="manage-column check-column"  scope="col"><?php _e('ID', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Email', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Dodatkowe dane', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Dodany', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Potwierdzony', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Listy / Logi', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Akcje', 'bpmj_eddnet') ?></th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th class="manage-column check-column"  scope="col"><?php _e('ID', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Email', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Dodatkowe dane', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Dodany', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Potwierdzony', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Listy / Logi', 'bpmj_eddnet') ?></th>
                <th class="manage-column" scope="col"><?php _e('Akcje', 'bpmj_eddnet') ?></th>
            </tr>
        </tfoot>

        <tbody data-wp-lists="list:comment" id="the-comment-list">

            <?php
// Lista, według której będą sortowane e-maile. 
            $sorted_list = bpmj_gm_sorted_by_list();

// Pobranie listy e-mail
            // Jeżeli został kliknięty link "kosz", pobierz tylko e-maile znajdujące się w koszu
            $emails = bpmj_gm_get_emails($sorted_list);


            $count = 0;
            foreach ($emails as $email_id) {

                //Pobranie list przypisanych do e-maili
                $lists = bpmj_gm_get_default_lists($email_id);

                // Pobranie danych dodatkowych
                $additional = bpmj_gm_get_additional_email_data($email_id);


                // Tworzenie tabeli
                echo '<tr>';

                echo '<td><b>' . $email_id . '</b></td>';

                echo '<td><b>' . bpmj_gm_get_email_name_by_id($email_id) . '</b></td>';

                // KOLUMNA 3 Z INFORMACJAMI ADRESOWYMI
                echo '<td>';
                if ($additional != FALSE) {
                    echo '<span class="bpmj_gm_list_default_click">' . __('Pokaż', 'bpmj_eddnet') . '</span><ul class="bpmj_gm_list_default">';
                    echo '<li>';
                    if (isset($additional['fname'])) {
                        echo $additional['fname'] . ' ';
                    }
                    if (isset($additional['lname'])) {
                        echo $additional['lname'];
                    }
                    echo '</li>';
                    if (
                            ( isset($additional['street']) && !empty($additional['street']) ) ||
                            ( isset($additional['post_code']) && !empty($additional['post_code']) ) ||
                            ( isset($additional['city']) && !empty($additional['city']) )
                    ) {
                        echo '<li><b>' . __('Adres:', 'bpmj_gm') . '</b></li>';
                    }
                    if (isset($additional['street'])) {
                        echo '<li>' . $additional['street'] . '</li>';
                    }
                    echo '<li>';
                    if (isset($additional['post_code'])) {
                        echo $additional['post_code'] . ' ';
                    }
                    if (isset($additional['city'])) {
                        echo $additional['city'];
                    }
                    echo '</li>';
                    if (isset($additional['phone']) && !empty($additional['phone'])) {
                        echo '<b>' . __('Telefon') . '</b> - ' . $additional['phone'];
                    }


                    echo '</ul>';
                }

                echo '</td>';

                echo '<td><b>' . date_i18n('j M. Y - G:i', bpmj_gm_get_email_adddate_by_id($email_id)) . '</b></td>';

                echo '<td><b>' . bpmj_gm_get_activated_email_date($email_id) . '</b></td>';

                echo '<td>';

                echo '<span class="bpmj_gm_list_default_click">' . __('Pokaż', 'bpmj_eddnet') . '</span><div class="bpmj_gm_list_default"><ul>';
                echo '<h3>' . __('Zapisany na listy:', 'bpmj_hm') . '</h3>';
                if (count($lists) > 0) {
                    foreach ($lists as $list) {
                        echo '<li>' . $list->name . '</li>';
                    }
                    echo '</ul>';
                } else {
                    echo '<li>' . __('Zapisano e-mail na niezdefiniowaną liste', 'bmpj_gm') . '</li>';
                }

                // Pozwala dodatkom wyświetlać swoje logi i statusy
                do_action('bpmj_gm_print_status', $email_id);
                
               bpmj_gm_print_sending_log($email_id, 'autoresponder');

                echo '</div></td>';
                echo '<td>';
                    echo bpmj_gm_refresh_email_form($email_id);
                    echo bpmj_gm_del_email_form($email_id);
                echo '</td>';
                echo '</tr>';

                $count++;
            }
            ?>
        </tbody>

    </table>

</div>