


jQuery(document).ready(function() {

    // Ukrywanie i pokazywanie detali w widoku listy e-maili
    jQuery('.bpmj_gm_list_default_click').toggle(
            function() {
               jQuery(this).text('Ukryj'); 
               jQuery(this).next().slideDown();
            },
            function() {
                 jQuery(this).text('Pokaż');    
                 jQuery(this).next().slideUp();
            }
    );
    
    // Ukrywanie i pokazywanie detali w widoku list
    jQuery('.bpmj_gm_list_default_click_lists').toggle(
            function() {
               var text = jQuery(this).text()
             //  jQuery(this).text(text + ' (Ukryj)'); 
               jQuery(this).next().slideDown();
            },
            function() {
                var text = jQuery(this).text()
                // jQuery(this).text(text + ' (Pokaż)');    
                 jQuery(this).next().slideUp();
            }
    );
    
    
    // Ukrywanie i pokazywanie ustawień listy
    jQuery('.bpmj_gm_settings_list_button').toggle(
            function() {
               jQuery(this).parent().parent().next().slideDown();
               
            },
            function() { 
               jQuery(this).parent().parent().next().hide();
                
            }
    );
    
     // Ukrywanie i pokazywanie shortcode w widoku list
    jQuery('.bpmj_gm_shortcode_btn').toggle(
            function() {
               jQuery(this).parent().parent().next().next().slideDown();
                jQuery(this).text('Ukryj');
            },
            function() { 
               jQuery(this).parent().parent().next().next().hide();
               jQuery(this).text('Pokaż'); 
            }
    );
    
    // Ukrywanie i pokazywanie pola z możliwością dodania kodu HTML formularza autorespondera przy dodawaniu listy
    jQuery('.bpmj_gm_ar_show').toggle(
            function() {
               jQuery(this).next().slideDown();
            },
            function() { 
               jQuery(this).next().slideUp();
            }
    );
    
    
    // Pokazywanie i ukrywanie niektórych opcji w ustawieniach
    if (jQuery("#facebook_login_active").attr("checked") !== "checked") {
                    jQuery("#bpmj_gm_facebook_options").hide();
                }   
                
                jQuery("#facebook_login_active").change(function() {
                    var element = jQuery(this).attr("checked");

                    if (element === "checked") {
                        jQuery("#bpmj_gm_facebook_options").fadeIn();
                    } else {
                        jQuery("#bpmj_gm_facebook_options").fadeOut();
                    }

                });


// Confirm przy usuwania listy
jQuery(".bpmj_gm_form_del_list").submit(function() {
        var bpmj_gm_list_delete = confirm("Jesteś pewień, że chcesz usunąć tą listę?");
        return bpmj_gm_list_delete;
});

});


