<?php

class BPMJ_GM_Plugin_Settings {

    private $bpmj_getmail;

    public function __construct() {
        $this->bpmj_getmail = get_option('bpmj_getmail', '');

        // Rejestracja opcji
        add_action('admin_init', array($this, 'bpmj_gm_register_settings_general'));
    }

    // Rejestracja ustawień
    public function bpmj_gm_register_settings_general() {
        register_setting('bpmj_getmail', 'bpmj_getmail', array($this, 'bpmj_gm_settings_general_validate'));

        add_settings_section(
                'bpmj_gm_settings_section_license', '', array($this, 'bpmj_gm_license_callback'), 'bpmj_gm-options-subpage'
        );

        add_settings_section(
                'bpmj_gm_settings_section_general', '', array($this, 'bpmj_gm_settings_callback'), 'bpmj_gm-options-subpage'
        );

        add_settings_section(
                'bpmj_gm_settings_section_form', '', array($this, 'bpmj_gm_section_form_callback'), 'bpmj_gm-options-subpage'
        );

        add_settings_field(
                'license_key', __("Klucz licencyjny", 'bpmj_gm'), array($this, 'bpmj_gm_license_key_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_license'
        );

        add_settings_field(
                'email_confirm', __("Weryfikacja adresu e-mail", 'bpmj_gm'), array($this, 'bpmj_gm_email_confirm_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_general'
        );
        
        add_settings_field(
                'email_content_link', __("Wysyłaj e-mail z linkiem do pełnej treści artykułu", 'bpmj_gm'), array($this, 'bpmj_gm_email_content_link_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_general'
        );
        

        add_settings_field(
                'email_message', __("Szablon wiadomości e-mail wysłanej w celu potwierdzenia adresu. W miejscu %link% wyświetli się link aktywacyjny.", 'bpmj_gm'), array($this, 'bpmj_gm_email_message_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_general'
        );
        
         add_settings_field(
                'anchor_link_activation', __("Tekst do wyświetlenia zamiast linku w emailu aktywacyjnym.", 'bpmj_gm'), array($this, 'bpmj_gm_anchor_link_activation_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_general'
        );
         
         add_settings_field(
                'email_adress_sender', __("Adres e-mail, który będzie widoczny jako nadawca wiadomości.", 'bpmj_gm'), array($this, 'bpmj_gm_email_adress_sender_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_general'
        );
         
         add_settings_field(
                'name_sender', __("Nazwa nadawcy wiadomości e-mail", 'bpmj_gm'), array($this, 'bpmj_gm_name_sender_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_general'
        );
         
         add_settings_field(
                'title_activation_email', __("Tytuł e-maila aktywacyjnego", 'bpmj_gm'), array($this, 'bpmj_gm_title_activation_email_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_general'
        );
         
         add_settings_field(
                'title_unblock_email', __("Tytuł e-maila, z linkiem odblokowującym treść.", 'bpmj_gm'), array($this, 'bpmj_gm_title_unblock_email_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_general'
        );

        add_settings_field(
                'success_text', __("Domyślna wiadomość po zapisaniu na listę", 'bpmj_gm'), array($this, 'bpmj_gm_success_text_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_general'
        );
        
         add_settings_field(
                'autoresponder_form', __("Autoresponder. Wklej kod formularza", 'bpmj_gm'), array($this, 'bpmj_gm_autoresponder_form_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_general'
        );
     

         
         
        add_settings_field(
                'form_theme', __("Szablon formularza", 'bpmj_gm'), array($this, 'bpmj_gm_form_theme_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
        
                add_settings_field(
                'form_title', __("Domyślny tytuł formularza", 'bpmj_gm'), array($this, 'bpmj_gm_form_title_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
        
        add_settings_field(
                'form_caption', __("Domyślny opis formularza", 'bpmj_gm'), array($this, 'bpmj_gm_form_caption_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
        
         add_settings_field(
                'form_theme_submit_text', __("Napis na przycisku \"Submit\"", 'bpmj_gm'), array($this, 'bpmj_gm_form_submit_text_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );


        add_settings_field(
                'form_theme_bg_color', __("Kolor tła formularza", 'bpmj_gm'), array($this, 'bpmj_gm_form_bg_color_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
        add_settings_field(
                'form_theme_border_color', __("Kolor obramowania", 'bpmj_gm'), array($this, 'bpmj_gm_form_border_color_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
        add_settings_field(
                'form_theme_head_color', __("Kolor nagłówka", 'bpmj_gm'), array($this, 'bpmj_gm_form_head_color_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
        add_settings_field(
                'form_theme_caption_color', __("Kolor opisu", 'bpmj_gm'), array($this, 'bpmj_gm_form_caption_color_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
        
        add_settings_field(
                'form_theme_text_color', __("Kolor tekstu", 'bpmj_gm'), array($this, 'bpmj_gm_form_text_color_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
        add_settings_field(
                'form_theme_submit_bg', __("Kolor przycisku \"Submit\"", 'bpmj_gm'), array($this, 'bpmj_gm_form_submit_bg_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
        add_settings_field(
                'form_theme_submit_color', __("Kolor napisu \"Submit\"", 'bpmj_gm'), array($this, 'bpmj_gm_form_submit_color_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );

        add_settings_field(
                'form_facebook_login', __("Logowanie przez Facebooka", 'bpmj_gm'), array($this, 'bpmj_gm_form_facebook_login_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
        
        add_settings_field(
                'form_address', __("Pola dodatkowe", 'bpmj_gm'), array($this, 'bpmj_gm_form_additional_field_callback'), 'bpmj_gm-options-subpage', 'bpmj_gm_settings_section_form'
        );
    }

    public function bpmj_gm_license_callback() {
        $head = '<h2>' . __("Ustawienia licencji", 'bpmj_gm') . '</h2>';
        echo $head;
    }

    public function bpmj_gm_settings_callback() {
        $head = '<h2>' . __("Ustawienia generalne", 'bpmj_gm') . '</h2>';
        
        // Inicjuje wordpressowego colorpickera
        $head .= '<script>jQuery(document).ready(function(){
            jQuery(".colorpicker").wpColorPicker();
});</script>';
        echo $head;
    }

    public function bpmj_gm_section_form_callback() {
        $head = '<h2>' . __("Ustawienia formularza", 'bpmj_gm') . '</h2>';
        echo $head;
    }

    // Zbudowanie selecta z wyborem szablonu formularza
    public function bpmj_gm_form_theme_callback() {
        $enabled = FALSE;
        $out = '';
        $val = FALSE;

        $select_list = bpmj_gm_get_themes();


        $theme = $this->bpmj_getmail['form_theme'];

        $out = '<select name="bpmj_getmail[form_theme]">';
        foreach ($select_list as $value => $name) {
            $out .= '<option ' . selected($theme, $value, FALSE) . ' value="' . $value . '">' . $name . '</option>';
        }
        $out .= '</select>';

        echo $out;
    }
    
    // Opcja - wysyła email z linkiem do ukrytej treści 
 
    public function bpmj_gm_email_content_link_callback() {

        $enabled = FALSE;
        $out = '';
        $val = FALSE;

        // check if checkbox is checked
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['email_content_link'])) {
            $val = true;
        }

        if ($val) {
            $out = '<input type="checkbox" id="email_confirm" name="bpmj_getmail[email_content_link]" CHECKED  />';
        } else {
            $out = '<input type="checkbox" id="email_confirm" name="bpmj_getmail[email_content_link]" />';
        }

        echo $out;
    }

    // Opcja  - Klucz licencyjny
    public function bpmj_gm_license_key_callback() {

        $license_status = get_option( 'bpmj_getmail_license_status' );

        if(isset($this->bpmj_getmail['license_key'])){
            $value = $this->bpmj_getmail['license_key'];
        }else {
            $value = '';
        }

        $out = '<input type="text" class="middle-text" id="license_key" name="bpmj_getmail[license_key]" value="' . $value . '"  />';
        if($license_status == 'valid')
            $out .= ' <span style="color: green">Licencja aktywna</span>';
        else
            $out .= ' <span style="color: red">Licencja nieaktywna</span>';

        echo $out;
    }
    
    // Opcja  - Weryfikacja adresu e-mail - CHECKOBX
    public function bpmj_gm_email_confirm_callback() {

        $enabled = FALSE;
        $out = '';
        $val = FALSE;

        // check if checkbox is checked
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['email_confirm'])) {
            $val = true;
        }

        if ($val) {
            $out = '<input type="checkbox" id="email_confirm" name="bpmj_getmail[email_confirm]" CHECKED  />';
        } else {
            $out = '<input type="checkbox" id="email_confirm" name="bpmj_getmail[email_confirm]" />';
        }

        echo $out;
    }
    
    
    
    

    // Opcja  - Weryfikacja adresu e-mail - CHECKOBX
    public function bpmj_gm_email_message_callback() {

        $value = $this->bpmj_getmail['email_message'];

        $out = '<textarea class="large-text" id="email_message" name="bpmj_getmail[email_message]">' . $value . '</textarea>';

        echo $out;
    }
    
    // Opcja  - Weryfikacja adresu e-mail - CHECKOBX
    public function bpmj_gm_autoresponder_form_callback() {

        $value = isset($this->bpmj_getmail['autoresponder_form']) ? $this->bpmj_getmail['autoresponder_form'] : '';
        
        $out = '<textarea class="large-text" id="email_message" name="bpmj_getmail[autoresponder_form]">' . $value . '</textarea>';

        echo $out;
    }
    
    
    
    // Opcja  - Tekst linku wyświetlony w wiadomości z potwierdzeniem e-mail
    public function bpmj_gm_anchor_link_activation_callback() {

        if (isset($this->bpmj_getmail['anchor_link_activation'])) {
            $value = $this->bpmj_getmail['anchor_link_activation'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="text" id="anchor_link_activation" name="bpmj_getmail[anchor_link_activation]" value="' . $value . '"  />';

        echo $out;
    }
    
    // Opcja  - Adres emial nadawcy wiadmości aktywacyjnej
    public function bpmj_gm_email_adress_sender_callback() {

        if (isset($this->bpmj_getmail['email_adress_sender'])) {
            $value = $this->bpmj_getmail['email_adress_sender'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="text" id="email_adress_sender" name="bpmj_getmail[email_adress_sender]" value="' . $value . '"  />';

        echo $out;
    }
    
    // Opcja  - Nazwa nadawcy wiadomości aktywaceyjnej
    public function bpmj_gm_name_sender_callback() {

        if (isset($this->bpmj_getmail['name_sender'])) {
            $value = $this->bpmj_getmail['name_sender'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="text" id="name_sender" name="bpmj_getmail[name_sender]" value="' . $value . '"  />';

        echo $out;
    }
    
       
        
    
    // Opcja  - Nazwa nadawcy wiadomości aktywaceyjnej
    public function bpmj_gm_title_activation_email_callback() {

        if (isset($this->bpmj_getmail['title_activation_email'])) {
            $value = $this->bpmj_getmail['title_activation_email'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="large-text" id="title_activation_email" name="bpmj_getmail[title_activation_email]" value="' . $value . '"  />';

        echo $out;
    }
    
    // Opcja  - Nazwa nadawcy wiadomości aktywaceyjnej
    public function bpmj_gm_title_unblock_email_callback() {

        if (isset($this->bpmj_getmail['title_unblock_email'])) {
            $value = $this->bpmj_getmail['title_unblock_email'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="large-text" id="title_unblock_email" name="bpmj_getmail[title_unblock_email]" value="' . $value . '"  />';

        echo $out;
    }
    
    

    // Opcja  - Domyślny tytuł formularza
    public function bpmj_gm_form_title_callback() {

        if (isset($this->bpmj_getmail['form_title'])) {
            $value = $this->bpmj_getmail['form_title'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="text" id="form_title" name="bpmj_getmail[form_title]" value="' . $value . '"  />';

        echo $out;
    }

    // Opcja  - Domyślny opis formularza
    public function bpmj_gm_form_caption_callback() {

        $value = $this->bpmj_getmail['form_caption'];

        $out = '<input type="text" class="large-text" id="form_caption" name="bpmj_getmail[form_caption]" value="' . $value . '"  />';

        echo $out;
    }

    // Opcja  - Domyślna wiadomość po zapisaniu maila
    public function bpmj_gm_success_text_callback() {

        $value = $this->bpmj_getmail['success_text'];

        $out = '<input type="text" class="large-text" id="success_text" name="bpmj_getmail[success_text]" value="' . $value . '"  />';

        echo $out;
    }
    
    // Opcja  - Kolor napisu Submit
    public function bpmj_gm_form_submit_text_callback() {

        if (isset($this->bpmj_getmail['submit_text'])) {
            $value = $this->bpmj_getmail['submit_text'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="text" id="success_text" name="bpmj_getmail[submit_text]" value="' . $value . '"  />';
        
        echo $out;
    }
    
    // Opcja  - Kolor tekstu
    public function bpmj_gm_form_text_color_callback() {

        if (isset($this->bpmj_getmail['text_color'])) {
            $value = $this->bpmj_getmail['text_color'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="small-text colorpicker" id="success_text" name="bpmj_getmail[text_color]" value="' . $value . '"  />';

        echo $out;
    }
    
    // Opcja  - Kolor opisu formularza
    public function bpmj_gm_form_caption_color_callback() {

        if (isset($this->bpmj_getmail['caption_color'])) {
            $value = $this->bpmj_getmail['caption_color'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="small-text colorpicker" id="success_text" name="bpmj_getmail[caption_color]" value="' . $value . '"  />';

        echo $out;
    }
    
    // Opcja  - Kolor nagłówka formularza
    public function bpmj_gm_form_head_color_callback() {

        if (isset($this->bpmj_getmail['head_color'])) {
            $value = $this->bpmj_getmail['head_color'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="small-text colorpicker" id="success_text" name="bpmj_getmail[head_color]" value="' . $value . '"  />';

        echo $out;
    }

      // Opcja  - Kolor tekstu na przycisku Submit
    public function bpmj_gm_form_submit_color_callback() {

        if (isset($this->bpmj_getmail['submit_color'])) {
            $value = $this->bpmj_getmail['submit_color'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="small-text colorpicker" id="success_text" name="bpmj_getmail[submit_color]" value="' . $value . '"  />';

        echo $out;
    }
    
        // Opcja  - Kolor tła przycisku Submit
    public function bpmj_gm_form_submit_bg_callback() {
        
        
        if (isset($this->bpmj_getmail['submit_bg'])) {
            $value = $this->bpmj_getmail['submit_bg'];
        } else {
            $value = '';
        }
        
        $out = '<input type="text" class="small-text colorpicker" id="success_text" name="bpmj_getmail[submit_bg]" value="' . $value . '"  />';
        
        
        
        echo $out;
    }
    
         // Opcja  - Kolor obramowania
    public function bpmj_gm_form_border_color_callback() {

        if (isset($this->bpmj_getmail['border_color'])) {
            $value = $this->bpmj_getmail['border_color'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="small-text colorpicker" id="success_text" name="bpmj_getmail[border_color]" value="' . $value . '"  />';

        echo $out;
    }
    
    // Opcja  - Kolor tła formularza
    public function bpmj_gm_form_bg_color_callback() {

        if (isset($this->bpmj_getmail['bg_color'])) {
            $value = $this->bpmj_getmail['bg_color'];
        } else {
            $value = '';
        }

        $out = '<input type="text" class="small-text colorpicker" id="success_text" name="bpmj_getmail[bg_color]" value="' . $value . '"  />';

        echo $out;
    }
    
    
    
    // Opcja  - Logowanie przez Facebook
    public function bpmj_gm_form_facebook_login_callback() {

        $enabled = FALSE;
        $out = '';
        $val = FALSE;

        // check if checkbox is checked
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_facebook_login']['active'])) {
            $val = true;
        }

        if ($val) {
            $out = '<input type="checkbox" id="facebook_login_active" name="bpmj_getmail[form_facebook_login][active]" CHECKED  />';
        } else {
            $out = '<input type="checkbox" id="facebook_login_active" name="bpmj_getmail[form_facebook_login][active]" />';
        }
        
        $out .= '<div id="bpmj_gm_facebook_options">';
        $out .= '<p>Aby dodać możliwość zapisu na listę za pomoca Facebooka musisz utworzyć aplikację na <a href="https://developers.facebook.com/apps">https://developers.facebook.com/apps</a> i skopiować z niej Fabebook App ID (zobacz <a href="http://support.onepress-media.com/how-to-register-a-facebook-app/">jak założyć aplikację na FB</a>).</p>';
        
        // Zprawdz, czy zaznaczono IMIĘ
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_facebook_login']['id'])) {
            $id = $this->bpmj_getmail['form_facebook_login']['id'];
        }else{
            $id = '';
        }

        // Zprawdz, czy Imię ma być wymagane
//        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_facebook_login']['token'])) {
//            $token = $this->bpmj_getmail['form_facebook_login']['token'];
//        }else {
//            $token = '';
//        }
        
        $out .= '<div>';
        $out .= '<input type="text" class="text" name="bpmj_getmail[form_facebook_login][id]" value="' . $id . '"  />';
        $out .= '<label>' . __("APP ID", 'bpmj_gm') . '</label>';
        $out .= '</div>';
        
//        $out .= '<div>';
//        $out .= '<input type="text" class="text" name="bpmj_getmail[form_facebook_login][token]" value="' . $token . '"  />';
//        $out .= '<label>' . __("APP Secret", 'bpmj_gm') . '</label>';
//        $out .= '</div>';
        
        $out .= '</div>';
        
        echo $out;
    }
 
    
    // Opcja  - Pokaż w formularzu dodatkowe pola
    public function bpmj_gm_form_additional_field_callback() {

        $enabled = FALSE;
        $out = '<div class="bpmj_gm_additional_fields">';

        // IMIĘ
        $fname = FALSE;
        $fname_req = FALSE;

        // NAZWISKO
        $lname = FALSE;
        $lname_req = FALSE;

        // ULICA Z NUMEREM
        $street = FALSE;
        $street_req = FALSE;

        // MIEJSCOWOŚĆ I KOD POCZTOWY
        $city = FALSE;
        $city_req = FALSE;

        // TELEFON
        $phone = FALSE;
        $phone_req = FALSE;

        // Zprawdz, czy zaznaczono IMIĘ
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_address']['fname'])) {
            $fname = TRUE;
        }

        // Zprawdz, czy Imię ma być wymagane
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_address']['fname_req'])) {
            $fname_req = TRUE;
        }

        // Zprawdz, czy zaznaczono IMIĘ
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_address']['lname'])) {
            $lname = TRUE;
        }

        // Zprawdz, czy Imię ma być wymagane
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_address']['lname_req'])) {
            $lname_req = TRUE;
        }

        // Zprawdz, czy zaznaczono ULICĘ Z NUMEREM
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_address']['street'])) {
            $street = TRUE;
        }

        // Zprawdz, czy ULICA Z NUMEREM ma być wymagana
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_address']['street_req'])) {
            $street_req = TRUE;
        }

        // Zprawdz, czy zaznaczono MIASTO I KOD POCZTOWY
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_address']['city'])) {
            $city = TRUE;
        }

        // Zprawdz, czy MIASTO I KOD POCZTOWY ma być wymagana
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_address']['city_req'])) {
            $city_req = TRUE;
        }

        // Zprawdz, czy zaznaczono TELEFON
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_address']['phone'])) {
            $phone = TRUE;
        }

        // Zprawdz, czy TELEFON ma być wymagany
        if (!empty($this->bpmj_getmail) && isset($this->bpmj_getmail['form_address']['phone_req'])) {
            $phone_req = TRUE;
        }

        $out .= '<label><b>Imię</b></label> ';

        // Pole imię
        if ($fname == TRUE) {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][fname]" CHECKED  />';
        } else {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][fname]" />';
        }

        $out .= ' Wymagane ';

        // Pole imię wymagane
        if ($fname_req == TRUE) {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][fname_req]" CHECKED  />';
        } else {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][fname_req]" />';
        }

        $out .= '<br /><label><b>Nazwisko</b></label> ';

        // Pole nazwisko
        if ($lname == TRUE) {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][lname]" CHECKED  />';
        } else {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][lname]" />';
        }

        $out .= ' Wymagane ';

        // Pole nazwisko wymagane
        if ($lname_req == TRUE) {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][lname_req]" CHECKED  />';
        } else {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][lname_req]" />';
        }



        $out .= '<br /><label><b>Ulica i numer</b></label> ';

        // ULICA Z NUMEREM
        if ($street == TRUE) {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][street]" CHECKED  />';
        } else {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][street]" />';
        }

        $out .= ' Wymagane ';

        // Pole ULICA Z NUMEREM wymagane
        if ($street_req == TRUE) {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][street_req]" CHECKED  />';
        } else {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][street_req]" />';
        }

        $out .= '<br /><label><b>Miasto i kod pocztowy</b></label> ';

        // MIASTO I KOD POCZTOWY
        if ($city == TRUE) {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][city]" CHECKED  />';
        } else {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][city]" />';
        }

        $out .= ' Wymagane ';

        // Pole MIASTO I KOD POCZTOWY wymagane
        if ($city_req == TRUE) {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][city_req]" CHECKED  />';
        } else {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][city_req]" />';
        }

        $out .= '<br /><label><b>Telefon</b></label> ';

        // TELEFON
        if ($phone == TRUE) {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][phone]" CHECKED  />';
        } else {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][phone]" />';
        }

        $out .= ' Wymagane ';

        // Pole TELEFON wymagane
        if ($phone_req == TRUE) {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][phone_req]" CHECKED  />';
        } else {
            $out .= '<input type="checkbox" name="bpmj_getmail[form_address][phone_req]" />';
        }


        // KONIEC FORMULARZA
        $out .= '</div>';
        echo $out;
    }

    /**
     * Walidacja formularza.
     * 
     * @param array $input 
     */
    public function bpmj_gm_settings_general_validate($input) {

        global $bpmj_gm_options;

        $input['license_key'] = trim( $input['license_key'] );

	if( !(empty($input['license_key'])) ) {
            $api_params = array(
			'edd_action'=> 'activate_license',
			'license' 	=> $input['license_key'],
			'item_name' => urlencode( BPMJ_GM_NAME)
		);

		$response = wp_remote_get( add_query_arg( $api_params, BPMJ_UPSELL_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

                if ( !is_wp_error( $response ) )
		{
                    $license_data = json_decode( wp_remote_retrieve_body( $response ) );
                    update_option( 'bpmj_getmail_license_status', $license_data->license );
                }
	}   
        else
            update_option( 'bpmj_getmail_license_status', '' );
        
        
        
        // Formatowanie HTML dla textarea autoresponder_form
        
        if(isset($input['autoresponder_form'])){
           $input['autoresponder_form'] = htmlspecialchars($input['autoresponder_form']);  
        }
        
        
        return $input;
    }

}

?>