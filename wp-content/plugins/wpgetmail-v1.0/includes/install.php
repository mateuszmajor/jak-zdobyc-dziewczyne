<?php

//Rejestracja aktywacyjnego zaczepu
register_activation_hook(BPMJ_GM_FILE, 'bpmj_gm_on_activate_callback');

function bpmj_gm_on_activate_callback() {


    //Zdefiniowanie domyslnych ustawień
    $options = get_option('bpmj_getmail');
    if ($options === false) {

        $form_title = 'To tylko fragment artykułu!';
        $form_caption = 'Aby uzyskać dostęp do całej treści, wypełnij poniższy formularz';
        $success_text = 'Dziękuję za zainteresowanie!';
        $email_message = <<<MSG
Witaj!

Dziękuję za wypełnienie formularza.
Aby dokończyć, potwierdź swój adres e-mail klikając poniżej:
>> %link%

--
Wiadomość wygenerowana automatycznie
MSG;

        $default_settings = array(
            'form_theme' => 'clear',
            'anchor_link_activation' => 'Kliknij tutaj aby potwierdzić!',
            'form_title' => $form_title,
            'form_caption' => $form_caption,
            'success_text' => $success_text,
            'email_message' => $email_message,
            'email_adress_sender' => 'nadawca@twojastrona.pl',
            'name_sender' => 'Nadawca :: twojastrona.pl',
            'title_activation_email' => 'Potwierdź swój adres email',
            'email_content_link' => 'off',
            'title_unblock_email' => 'Dostęp do strony',
            'email_confirm' => 'on',
            'submit_text' => 'Dalej'
        );

        add_option('bpmj_getmail', $default_settings);
    }


    // Dodanie noej tabeli w bazie danych
    global $wpdb;
    global $bpmj_gmdb;
    global $bpmj_gmdb_rel;
    global $bpmj_gmdb_lists;
    global $bpmj_gm_db_version;

    $db_ver = get_option('bpmj_gm_db_version');
    
    // Sprawdza wersji bazy danych. 
    if ($db_ver != $bpmj_gm_db_version) {

        // Dodanie tabeli z adresami e-mail
        $sql = "CREATE TABLE `$bpmj_gmdb` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
        `date_add` INT(11) NOT NULL,
        `email` VARCHAR(150) NOT NULL,
        `confirm` INT(11) NOT NULL,
        `status` LONGTEXT NOT NULL,
	`details` LONGTEXT NOT NULL,
        `note` LONGTEXT NOT NULL,
        `temp` LONGTEXT NOT NULL,
        `trash` INT(1) NOT NULL,
	PRIMARY KEY  (id)
	);";

        // status: new, paid
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        global $bpmj_gmdb_lists;
        // Dodanie tabeli z listami ogólnymi
        $sql = "CREATE TABLE `$bpmj_gmdb_lists` (
	`list_id` INT(11) NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(150) NOT NULL,
        `title` LONGTEXT NOT NULL,
        `description` LONGTEXT NOT NULL,
        `theme` VARCHAR(150) NOT NULL,
        `removed` INT(1) NOT NULL,
        `outer_lists` LONGTEXT NOT NULL,
	PRIMARY KEY  (list_id)
	);";

        // status: new, paid
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);


        // Dodanie tabeli z relacjami między emailami, a listami.
        $sql = "CREATE TABLE `$bpmj_gmdb_rel` (
	`object_id` INT(11) NOT NULL AUTO_INCREMENT,
        `email_id` INT(11) NOT NULL,
        `list_id` INT(11) NOT NULL,
	PRIMARY KEY  (object_id)
	);";

        // status: new, paid
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);


        // Opcja aktualizująca wersję bazy danych
        update_option("bpmj_gm_db_version", $bpmj_gm_db_version);
    }



    // Dodaje unikatowy TOKEN do każdego posta na stronie
    $args_posts = array(
        'posts_per_page' => 9999999,
        'post_type' => get_post_types()
    );

    $posts = get_posts($args_posts);

    foreach ($posts as $post) {

        $key = get_post_meta($post->ID, 'bpmj_gm_post_token', true);

        if (empty($key)) {

            // Tworzy unikalny kod
            $token = md5(uniqid(mt_rand(), true));
            add_post_meta($post->ID, 'bpmj_gm_post_token', $token);
        }
    }
}
?>