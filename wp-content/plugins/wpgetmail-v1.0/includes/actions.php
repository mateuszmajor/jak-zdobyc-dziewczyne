<?php
/*
 * Wczytuje plik getmail.js odpowiedzialny m.in za validację formularza
 * 
 */
// Dodawanie skryptów do <head>
add_action('init', 'bpmj_gm_load_getmail_js');

function bpmj_gm_load_getmail_js() {
    if (!is_admin()) {

        wp_register_script('getmail', BPMJ_GM_URL_INCLUDES . '/js/getmail.js', array('jquery'));
        wp_enqueue_script('getmail');
    }
}

/*
 * Załadowanie domyślnych arkuszy styli
 * 
 */
add_action('wp_enqueue_scripts', 'bpmj_gm_add_css');

function bpmj_gm_add_css() {

    wp_register_style('get_mail', BPMJ_GM_URL_INCLUDES . '/css/getmail.css', array(), '1.0', 'screen');
    wp_enqueue_style('get_mail');
}

/**
 * Ładuje skrypt js na stroniach wtyczki w panelu
 * 
 */
add_action('admin_enqueue_scripts', 'bpmj_gm_load_admin_scripts');

function bpmj_gm_load_admin_scripts() {
    if (isset($_GET['page'])) {

        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');

        wp_enqueue_script('bpmj_gm_setting_scripts', BPMJ_GM_URL . '/includes/admin/js/emails.js');
    }
}

/*
 * Odszyfrowanie zwrotnego linku aktywacyjnego i aktualizacja wpisu w bazie
 * 
 */
add_action('wp_loaded', 'bpmj_gm_confirm_listener');

function bpmj_gm_confirm_listener() {

    global $wpdb;
    global $bpmj_gmdb; // nazwa tabeli
    // Sprawdza, czy link aktywacyjny został kliknięty
    if (isset($_GET['bpmj_gm_confirm_token']) && isset($_GET['id']) && isset($_GET['lists'])) {

        // Odczytanie tokenu
        $token_out = sanitize_text_field($_GET['bpmj_gm_confirm_token']);

        // Odczytanie ID e-maila
        $email_id_out = sanitize_text_field($_GET['id']);

        // Odczytanie list
        $lists_out = sanitize_text_field($_GET['lists']);

        // Odebranie ID posta
        if (isset($_GET['p'])) {
            $post_id = sanitize_text_field($_GET['p']);
        } else {
            $post_id = '';
        }

        //pobranie date_add i emiala z bazy
        //Pobranie daty dodania
        $query = "SELECT date_add FROM $bpmj_gmdb WHERE id = '" . $email_id_out . "'";
        $date_add = $wpdb->get_var($query);

        //Pobranie emaila
        $query = "SELECT email FROM $bpmj_gmdb WHERE id = '" . $email_id_out . "'";
        $email = $wpdb->get_var($query);

        // Tworzy token md5( NONCE_KEY + $date_add + $email + $email_id + $lists )
        $token_home = md5(NONCE_KEY . $date_add . $email . $email_id_out . $lists_out);


        //Sprawdzamy poprawność tokena
        if ($token_home === $token_out) {

            // Sprawdza, czy adres został już potwierdzony. Jeżeli tak zablokuj ponowne potwierdzenie na 10 minut.    
            $confirmed = bpmj_gm_get_activated_email_date($email_id_out, TRUE);

            // Ilość sekund, po których będzie można znowu potwierdzić e-mail
            $interval = 6000; // 10 minut

            if ($confirmed !== FALSE) {
                if (($confirmed + $interval) > current_time('timestamp')) {
                    if ($post_id == '') {
                        wp_redirect(get_bloginfo('url'));
                        exit;
                    } else {
                        wp_redirect(get_permalink($post_id));
                        exit;
                    }
                }
            }
            // Aktualizacja bazy danych
            $data = array(
                'confirm' => current_time('timestamp'),
            );

            $wpdb->update($bpmj_gmdb, $data, array('id' => $email_id_out));


            // Resetuje status autoresponoderów
            // Ustawia status e-maila ( pobiera dane z zainstalowanych autoresponderów za pomoca filtra )
            $status_raw = array();

            // Filtr, dzięki któremu autoresponder może dopisać swój status podczas zapisu e-maila.
            $status = apply_filters('bpmj_gm_set_status', $status_raw);

            $data = array(
                'status' => serialize($status)
            );
            $wpdb->update($bpmj_gmdb, $data, array('email' => $email));


            // Sprawdzenie, czy formularz miał obdlokować treść
            if (isset($_GET['c']) && $_GET['c'] == 1) {
                $content = 'has';
            } else {
                $content = '';
            }


            $lists = bpmj_gm_formatted_attr_lists($lists_out);

            // Wysyła link odblokowujący wszytskie formularze z listami, na które zapisał się użytkownik.
            bpmj_gm_sent_article_link($lists, array('email' => $email, 'content' => $content, 'post_id' => $post_id));

            //Aktualizacja tablicy SESSION
            $_SESSION['confirm'] = TRUE;

            $lists = bpmj_gm_formatted_attr_lists($lists_out);
            // Utworzenie sesji pozwalającej na odblokowanie formularzy z listami zawartymi w tablicty GET "lists"
            foreach ($lists as $list) {
                $_SESSION['lists'][] = $list;
            }

            $_SESSION['lists'] = array_unique($_SESSION['lists']);

            // Zaczep akcji dla wtyczek uruchamiany po wprawidłowej weryfikacji e-maila
            // Zostaje przekaze ID e-maila
            $email_id = $email_id_out;
            do_action('bpmj_gm_confirm_success', $email_id);

            if ($post_id == '') {
                wp_redirect(get_bloginfo('url'));
                exit;
            } else {
                wp_redirect(get_permalink($post_id));
                exit;
            }
        }
    }
}

add_action('init', 'bpmj_gm_create_csv');

function bpmj_gm_create_csv() {

    if (is_admin() && isset($_POST['bpmj_gm_csv_export_list'])) {


        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="emails.csv"');

        $list = sanitize_text_field($_POST['bpmj_gm_csv_export_list']);
        
        $confirm = sanitize_text_field($_POST['bpmj_gm_csv_export_list_confirm']);
        
        $emails = bpmj_gm_get_emails($list, 'ASC', $confirm);


        $fp = fopen('php://output', 'w');
        foreach ($emails as $email_id) {

            // Pobranie adresu e-mail
            $name = bpmj_gm_get_email_name_by_id($email_id);

            // Pobranie pól dodatkowych
            $additional = bpmj_gm_get_additional_email_data($email_id);
            if ($additional != FALSE) {

                $fname = $additional['fname'];
                $lname = $additional['lname'];
                $street = $additional['street'];
                $post_code = $additional['post_code'];
                $city = $additional['city'];
                $phone = $additional['phone'];

                $val = array($name, $fname, $lname, $street, $post_code, $city, $phone);
            } else {
                $val = array($name, '', '', '', '', '', '');
            }





            fputcsv($fp, $val);
        }
        fclose($fp);
        exit();
    }
}

/*
 * Akcja obsługuje formularz dodania nowej listy ( zapisuje do bazy nazwę, opis, nagłówek i szablon listy )
 */
add_action('bpmj_gm_before_list_page_print', 'bpmj_gm_add_new_list');

function bpmj_gm_add_new_list() {

    global $wpdb;
    global $bpmj_gmdb_lists;

    // Obsługa dodawania nowej listy
    if (isset($_POST['bpmj_gm_list'])) {

        if (!isset($_POST['bpmj_gm_lists_check_nonce'])) {
            return;
        }

        $nonce = $_POST['bpmj_gm_lists_check_nonce'];

// Sprawdź poprawnośc nonce
        if (!wp_verify_nonce($nonce, 'bpmj_gm_lists_check')) {
            return;
        }

        if (!isset($_POST['bpmj_gm_list']) || empty($_POST['bpmj_gm_list'])) {
            add_action('bpmj_gm_validate_list_print', 'bpmj_gm_list_name_empty');

            function bpmj_gm_list_name_empty() {
                echo '<div class="bpmj_gm_error_alert">' . __('Nazwa listy jest wymagana.', 'bpmj_gm') . '</div>';
            }

            return;
        }

        // Sprawdzanie szablonu formularza
        if ($_POST['bpmj_gm_list_theme'] == 'default') {
            $theme = '';
        } else {
            $theme = sanitize_text_field($_POST['bpmj_gm_list_theme']);
        }

        // Sprawdzanie nagłówka formularza
        if (isset($_POST['bpmj_gm_list_title']) && !empty($_POST['bpmj_gm_list_title'])) {
            $title = sanitize_text_field($_POST['bpmj_gm_list_title']);
        } else {
            $title = '';
        }

        // Sprawdzanie opisu formularza
        if (isset($_POST['bpmj_gm_list_description']) && !empty($_POST['bpmj_gm_list_description'])) {
            $description = sanitize_text_field($_POST['bpmj_gm_list_description']);
        } else {
            $description = '';
        }
        
        
        // Przygotowanie danych relacyjnych do zapisu
        $add_list = array(
            'name' => sanitize_text_field($_POST['bpmj_gm_list']),
            'theme' => $theme,
            'title' => $title,
            'description' => $description
        );

        // filtr, pozwalający dodać nowe dane do bazy przy zapisie listy
        $data = apply_filters('bpmj_gm_additional_lists', $add_list);

        $wpdb->insert($bpmj_gmdb_lists, $data);
    }
}

/*
 * Akcja obsługuje usuwanie listy 
 */
add_action('bpmj_gm_before_list_page_print', 'bpmj_gm_delete_list');

function bpmj_gm_delete_list() {
    global $wpdb;
    global $bpmj_gmdb_lists;
    // Obsługa usuwania listy
    if (isset($_POST['bpmj_gm_delate_list_check'])) {

        if (!isset($_POST['bpmj_gm_lists_del_check_nonce'])) {
            return;
        }

        $nonce = $_POST['bpmj_gm_lists_del_check_nonce'];

// Sprawdź poprawnośc nonce
        if (!wp_verify_nonce($nonce, 'bpmj_gm_lists_del_check')) {
            return;
        }

        $list_id = sanitize_text_field($_POST['bpmj_gm_delate_list_check']);

        // Przygotowanie danych relacyjnych do zapisu
        $add_list = array(
            'removed' => 1
        );

        $wpdb->update($bpmj_gmdb_lists, $add_list, array('list_id' => $list_id));
    }
}

/*
 * Akcja obsługuje usuwanie emaila z listy mailingowej
 */
add_action('bpmj_gm_before_email_page_print', 'bpmj_gm_email_delete');

function bpmj_gm_email_delete() {


    if (isset($_POST['bpmj_gm_email_del'])) {


        if (!isset($_POST['bpmj_gm_email_del_check_nonce'])) {
            return;
        }

        $nonce = $_POST['bpmj_gm_email_del_check_nonce'];

        // Sprawdź poprawnośc nonce
        if (!wp_verify_nonce($nonce, 'bpmj_gm_email_del_check')) {
            return;
        }

        $email_id = sanitize_text_field($_POST['bpmj_gm_email_del']);

        // Permanentne usunięcie e-maila wraz z powiązaniami
        bpmj_gm_del_email($email_id);
    }
}

/*
 * Akcja obsługuje formularz odświeżania e-maila. Zmienia statusy na 'new' oraz wysyła do autorespondera
 */

add_action('bpmj_gm_before_email_page_print', 'bpmj_gm_email_refresh');

function bpmj_gm_email_refresh() {
    global $wpdb;
    global $bpmj_gmdb;

    if (isset($_POST['bpmj_gm_return_email_form'])) {


        if (!isset($_POST['bpmj_gm_return_email_form_check_nonce'])) {
            return;
        }

        $nonce = $_POST['bpmj_gm_return_email_form_check_nonce'];

        // Sprawdź poprawnośc nonce
        if (!wp_verify_nonce($nonce, 'bpmj_gm_return_email_form_check')) {
            return;
        }

        $email_id = sanitize_text_field($_POST['bpmj_gm_return_email_form']);

        // Pobranie obecnego statusu
        $status_before = bpmj_gm_get_status($email_id);

        // Ggy nie ma autoresponderwó, zwróci pustą tablicę.
        if (empty($status_before)) {
            $status_after = array();
        } else {

            foreach ($status_before as $key => $value) {

                $status_after[$key] = 'new';
            }
        }

        $status_after = serialize($status_after);

        // Przygotowanie danych relacyjnych do zapisu
        $data = array(
            'status' => $status_after,
            'note' => '',
            'trash' => 0
        );

        $wpdb->update($bpmj_gmdb, $data, array('id' => $email_id));
        
        do_action('bpmj_gm_after_refresh', $email_id);
    }
}

/*
 * Akcja odpowiedzialna za utworzenie unikalnego tokenu dla każdego z artykułów
 */

add_action('save_post', 'bpmj_gm_create_unique_post_key');

function bpmj_gm_create_unique_post_key($post_id) {

    // If this is just a revision, don't send the email.
    if (wp_is_post_revision($post_id))
        return;

    $key = get_post_meta($post_id, 'bpmj_gm_post_token', true);

    if (empty($key)) {

        // Tworzy unikalny kod
        $token = md5(uniqid(mt_rand(), true));
        add_post_meta($post_id, 'bpmj_gm_post_token', $token);
    }
}

/*
 * Akcja zapisuje do bazy nowe dane podczas edycji szablonu, tytułu lub opisu listy
 */
add_action('bpmj_gm_before_list_page_print', 'bpmj_gm_update_list_details');

function bpmj_gm_update_list_details() {

    if (isset($_POST['bpmj_gm_edit_list_check'])) {

        global $wpdb;
        global $bpmj_gmdb_lists;

        // Odebranie ID listy
        if (isset($_POST['bpmj_gm_edit_list_id']) && !empty($_POST['bpmj_gm_edit_list_id'])) {
            $list_id = sanitize_text_field($_POST['bpmj_gm_edit_list_id']);
        } else {
            return;
        }

        // Odebranie formularza - szablon
        if (isset($_POST['bpmj_gm_list_theme_field']) && !empty($_POST['bpmj_gm_list_theme_field'])) {
            $theme = sanitize_text_field($_POST['bpmj_gm_list_theme_field']);

            if ($theme == 'none') {
                $theme = '';
            }
        } else {
            $theme = '';
        }

        // Odebranie formularza - tytuł
        if (isset($_POST['bpmj_gm_list_title_field']) && !empty($_POST['bpmj_gm_list_title_field'])) {
            $title = sanitize_text_field($_POST['bpmj_gm_list_title_field']);
        } else {
            $title = '';
        }

        // Odebranie formularza - tytuł
        if (isset($_POST['bpmj_gm_list_caption_field']) && !empty($_POST['bpmj_gm_list_caption_field'])) {
            $caption = sanitize_text_field($_POST['bpmj_gm_list_caption_field']);
        } else {
            $caption = '';
        }

        //Sprawdzam nonce
        if (!isset($_POST['bpmj_gm_edit_list_wp_nonce'])) {
            return;
        }

        $nonce = $_POST['bpmj_gm_edit_list_wp_nonce'];

// Sprawdź poprawnośc nonce
        if (!wp_verify_nonce($nonce, 'bpmj_gm_edit_list')) {
            return;
        }


        $data = array(
            'theme' => $theme,
            'title' => $title,
            'description' => $caption,
        );
        // Zapisje do bazy
        $wpdb->update($bpmj_gmdb_lists, $data, array('list_id' => $list_id));
    } else {
        return;
    }
}

// Dodaje formularz w widoku list umożliwiający zdefiniowaniu autorespondera zewnętrznego.
add_action('bpmj_gm_outer_lists', 'bpmj_gm_form_outer_lists_print');

function bpmj_gm_form_outer_lists_print($list_id) {


    // Gdy zmieniono lub dodano dane, aktualizuje je
    bpmj_gm_form_outer_lists_update();

    // Pobiera aktualne dane list dodatkowych
    $lists = bpmj_gm_get_autoresponder_list_html($list_id);

    $autoresponer = isset($lists['form']) && !empty($lists['form']) ? $lists['form'] : '';


    echo '<span class="bpmj_gm_list_default_click_lists bpmj_autor-sd">' . __('Dodaj autoresponder +', 'bpmj_gm') . '</span>';
    echo '<div class="bpmj_gm_list_default">';
    echo '<form action="' . bpmj_gm_current_url() .'" method="POST">';
    echo '<h3>' . __('Formularz HTML', 'bpmj_gm') . '</h3>';
    echo '<div class="bpmj_gm_net_firld_area"><label>' . __('Wygeneruj formularz w swoim autoresponderze i wklej kod poniżej.', 'bpmj_gm') . '</label>';
    echo '<textarea style="width:100%;height:200px;" name="bpmj_gm_autoresponder_form">' . html_entity_decode(stripslashes($autoresponer), ENT_COMPAT, 'UTF-8') . '</textarea></div>';
    echo '<input type="hidden" name="bpmj_gm_autoresponder_check">';
    echo '<input type="hidden" value="' . $list_id . '" name="bpmj_gm_autoresponder_list_id">';
    wp_nonce_field('bpmj_gm_autoresponder_check_wp', 'bpmj_gm_autoresponder_check_wp_nonce');
    echo '<input class="button button-primary submit" type="submit" value="Zmień">';
    echo '</form>';
    echo '</div>';
}

add_action('wp_head', 'bpmj_gm_add_JavaScript_SDK', 99);

function bpmj_gm_add_JavaScript_SDK() {

    global $bpmj_gm_options;

    // Spawdza, czy logowanie prze FB jest włączone
    if (
            isset($bpmj_gm_options['form_facebook_login']['active']) &&
            !empty($bpmj_gm_options['form_facebook_login']['active']) &&
            isset($bpmj_gm_options['form_facebook_login']['id']) &&
            !empty($bpmj_gm_options['form_facebook_login']['id'])
    ) {
        ?>

        <script>

            window.fbAsyncInit = function() {
                FB.init({
                    appId: '<?php echo $bpmj_gm_options['form_facebook_login']['id']; ?>',
                    cookie: true, // enable cookies to allow the server to access 
                    // the session
                    xfbml: true, // parse social plugins on this page
                    version: 'v2.0' // use version 2.0
                });
            };

            function checkLoginState() {
                FB.getLoginStatus(function(response) {
                    statusChangeCallback(response);
                });
            }
            function statusChangeCallback(response) {
                console.log('statusChangeCallback');
                console.log(response);
                if (response.status === 'connected') {
                    // Logged into your app and Facebook.
                    bpmj_gm_get_facebook_data();
                } else if (response.status === 'not_authorized') {
                } else {
                }
            }

            function bpmj_gm_get_facebook_data() {

                FB.api('/me', function(response) {

                    console.log(response);

                    // Dodaje Email
                    if (response.email.length != 0) {
                        jQuery('input[name=bpmj_gm_email]').val(response.email);
                    }
                    ;
                    // Dodajemy Imię
                    if (response.first_name.length != 0) {
                        jQuery('input[name=bpmj_gm_fname]').val(response.first_name);
                    }
                    ;
                    // Dodaje Nazwisko
                    if (response.last_name.length != 0) {
                        jQuery('input[name=bpmj_gm_lname]').val(response.last_name);
                    }
                    ;
                    // Dodaje Miasto
                    if (response.hometown.length != 0) {
                        jQuery('input[name=bpmj_gm_city]').val(response.hometown);
                    }
                    ;
                });
            }
            ;


            // Load the SDK asynchronously
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            // Here we run a very simple test of the Graph API after login is
            // successful.  See statusChangeCallback() for when this call is made.
            function testAPI() {
                console.log('Welcome!  Fetching your information.... ');
                FB.api('/me', function(response) {
                    console.log('Successful login for: ' + response.name);
                    document.getElementById('status').innerHTML =
                            'Thanks for logging in, ' + response.name + '!';
                });
            }

        </script>

        <?php
    }
}


/*
 * Wysłanie e-maila do autorespondera wykorzystując metodę z formularzem HTML
 */

add_action('bpmj_gm_after_save_email_no_confirm', 'bpmj_gm_sent_to_autoresponer', 10, 1);
add_action('bpmj_gm_confirm_success', 'bpmj_gm_sent_to_autoresponer', 10, 1);
add_action('bpmj_gm_after_refresh', 'bpmj_gm_sent_to_autoresponer', 10, 1);

function bpmj_gm_sent_to_autoresponer($email_id) {

    global $bpmj_gm_options;
    

    // IMIĘ
    if(isset($_POST['bpmj_gm_fname'])){
    $imie = isset($_POST['bpmj_gm_fname']) ? esc_attr($_POST['bpmj_gm_fname']) : '';
    }else{
        $email_data = bpmj_gm_get_additional_email_data($email_id);
      $imie = isset($email_data['fname']) ? $email_data['fname'] : '';
    }
    
    // Zakończ, jeżeli parametr email_id nie jest numerem
    if(!is_numeric($email_id))
    return;
    
    $email = bpmj_gm_get_email_name_by_id($email_id);
    
    //Pobiera listy przypisane do e-maila
    $lists = bpmj_gm_get_default_lists($email_id);
    if (!is_array($lists)) {
        return;
    }


    // Tworzy tablicę z wszystkimi ID list wewnętrznych, na które zapisany jest e-mail
    $lists_forms = array();
    foreach ($lists as $list_object) {

        // Pobiera ID listy
        $id = bpmj_gm_get_list_id_by_name($list_object->name);

        // Sprawdza, czy do listy jest przypisany formularz auorespondera
        $ar_html_form = bpmj_gm_get_autoresponder_list_html($id);

        if (isset($ar_html_form['form']) && !empty($ar_html_form['form'])) {
            $lists_forms[] = $ar_html_form['form'];
        }
    }

    // Gdy nie przypisano do list żadnego formularza autorespondera, pobierz z ustawień głównych
    if (empty($lists_forms)) {

        $form_general = isset($bpmj_gm_options['autoresponder_form']) ? $bpmj_gm_options['autoresponder_form'] : '';

        if (!empty($form_general)) {
            $lists_forms = array($form_general);
        } else {
            return; // Jeżeli nie podano formularza autorespondera zarówno dla list oraz w ustawieniach generalnych - ZAKONCZ 
        }
    }

    // W tablicy $lists_id przechowane są wszyyskie kody HTML formularza, na które wysłany ma być e-mail

    foreach ($lists_forms as $html_form) {

        $html_form = html_entity_decode(stripslashes($html_form), ENT_COMPAT, 'UTF-8');

        // Odpalenie biblioteki phpQuery, pozwalającej poruszać się łatwo po drzewie DOM w php
        phpQuery::newDocument($html_form);


        $action = pq('form')->attr('action');
        
        $method = pq('form')->attr('method');

        $params = array();
        foreach (pq('input') as $in) {

            $name = pq($in)->attr('name');
            $value = pq($in)->attr('value');

            // Nazwa
            $params[$name] = $value;

            // Podstwawia w odpowiednie pole adres e-mail 
            if (strpos(strtolower($name), 'mail') !== FALSE) {
                $params[$name] = $email;
            }

            // Podstwawia w odpowiednie pole Imię
            if (strpos(strtolower($name), 'imie') !== FALSE || strpos(strtolower($name), 'name') !== FALSE) {
                $params[$name] = $imie;
            }

            if (!isset($params[$name]) || pq($in)->attr('type') == 'submit') {
                unset($params[$name]);
            }
        }
        
        /*
         * Jeżeli autoresponder to MAIL POET, to użyj innej metody zapisu ni cURL
         */
       if($action === '#wysija'){
            bpmj_gm_add_subscriber_to_mailpoet($params);
            return;
        }
     
        // Przy MailChimpe należy dodac http przed URLem
        if(strpos($action, 'list-manage.com')){
            $action = 'http:' . $action;
        }
        
        
        
        // Budowanie żądania HTTP POST do NetMailera
        if (strtolower($method) == 'post') {
            $response = wp_remote_retrieve_body(wp_remote_post($action, array(
                'timeout' => 45,
                'redirection' => 5,
                'body' => $params
                            )
            ));
        } elseif (strtolower($method) == 'get') {
            $response = wp_remote_retrieve_body(wp_remote_get($action, array(
                'timeout' => 45,
                'redirection' => 5,
                'body' => $params
                            )
            ));
        }
        
        // Sprawdzenie połączenia
        if (is_wp_error($response)) {
            $error_message = $response->get_error_message();
            
        // Dodatnie komunikatu błędu
        $note = '<b class="bpmj_gm_error_note">Błąd ( Autoresponder )</b>Błąd połączenia</b>';
        bpmj_gm_set_note($email_id, $note, 'autoresponder');
            
        exit('Bład podczas połączenia: ' . $error_message);
        }
        
        
        // TWORZENIE LOGÓW DLA ODPOWIEDZI SERWERA PO WYSŁANIU E-MAILI
        
        // Dodatnie komunikatu błędu
        $note = '<b class="bpmj_gm_success_note">Wysłano do autorespondera</b> - ' . current_time('d.m.Y, H:i');
        bpmj_gm_set_note($email_id, $note, 'autoresponder');
        
        
    }
}





?>