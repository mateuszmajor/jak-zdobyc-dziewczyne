<?php

/*
 * Szablon Clear formularza shordcode
 * 
 */

/*
 * ZMIENNE DO UŻYCIA W SZABLONIE ZDEFINIOWANE W INNYM MIEJSCU
 * 
 * $form_title - Tytuł formularza
 * $form_description - Opis formularza
 */

// Zdefiniowanie nazwy szablonu
global $gm_templates;
global $bpmj_gm_options;
$gm_templates['action'] = 'Action';

// Pobranie ustawień
if(isset($bpmj_gm_options['form_address'])){
    $adress = $bpmj_gm_options['form_address'];
}else {
    $adress['fname'] = ''; $adress['lname'] = ''; $adress['lname_req'] = ''; $adress['fname_req'] = ''; $adress['street'] = ''; 
    $adress['street_req'] = ''; $adress['city'] = ''; $adress['city_req'] = ''; $adress['phone'] = ''; $adress['phone_req'] = '';
}

$star = '';

// Kolor tła i obramowania
if(isset($bpmj_gm_options['bg_color']) && !empty($bpmj_gm_options['bg_color'])){
    $bg_color = 'background:' . $bpmj_gm_options['bg_color'].';';
}else{
 $bg_color = '';   
}

// Kolor obramowania formularza
if(isset($bpmj_gm_options['border_color']) && !empty($bpmj_gm_options['border_color'])){
    $border_color = 'border-color:' . $bpmj_gm_options['border_color'].';';
}else{
 $border_color = '';   
}
// Utworzenie atrybutu style dla tła i koloru obramowania formularza
if(isset($bpmj_gm_options['border_color']) || isset($bpmj_gm_options['border_color'])){
    $border_bg_color = ' style="' . $bg_color . $border_color . '"';
}else{
   $border_bg_color = ''; 
}

// Kolor nagłówka formularza
if(isset($bpmj_gm_options['head_color']) && !empty($bpmj_gm_options['head_color'])){
    $head_color = ' style="color:' . $bpmj_gm_options['head_color'].';"';
}else{
 $head_color = '';   
}

// Kolor opisu formularza
if(isset($bpmj_gm_options['caption_color']) && !empty($bpmj_gm_options['caption_color'])){
    $caption_color = ' style="color:' . $bpmj_gm_options['caption_color'].';"';
}else{
 $caption_color = '';   
}

// Kolor tekstu formularza
if(isset($bpmj_gm_options['text_color']) && !empty($bpmj_gm_options['text_color'])){
    $text_color = ' style="color:' . $bpmj_gm_options['text_color'].';"';
}else{
 $text_color = '';   
}

// Kolor tła przycisku SUBMIT
if(isset($bpmj_gm_options['submit_bg']) && !empty($bpmj_gm_options['submit_bg'])){
    $submit_bg = 'background:' . $bpmj_gm_options['submit_bg'].'!important;';
}else{
 $submit_bg = '';   
}

// Kolor napisu na przycisku SUBMIT
if(isset($bpmj_gm_options['submit_color']) && !empty($bpmj_gm_options['submit_bg'])){
    $submit_color = 'color:' . $bpmj_gm_options['submit_color'].';';
}else{
 $submit_color = '';   
}

// Utworzenie atrybutu style dla tła i koloru przycisku submit
if(isset($bpmj_gm_options['submit_color']) || isset($bpmj_gm_options['submit_bg'])){
    $submit_bg_border = ' style="' . $submit_bg . $submit_color . '"';
}

// Domyślny napis na przycisku submit
if(isset($bpmj_gm_options['submit_text']) && !empty($bpmj_gm_options['submit_text'])){
    $submit_text = $bpmj_gm_options['submit_text'];
}else{
 $submit_text = __('Zapisz się', 'bpmj_gm');   
}


// Budujemy szablon formularza Clear

// Dołączenie styli CSS formularza
$form = bpmj_gm_form_action_css();

// Div otwierający
$form .= '<div' . $border_bg_color . ' class="bpmj_form_action">';

// Tytuł formularza
$form .= '<h3' . $head_color . ' class="bpmj-form-title_action">' . $form_title . '</h3>';

// Opis formularza
$form .= '<span' . $caption_color . ' class="bpmj-form-caption_action">' . $form_description . '</span>';

// Formularz HTML
$form .= '<form ' . $text_color . ' id="bpmj_gm_form_' . $shortcode_id . '" class="gm-form-action bpmj_gm_form" action="' . bpmj_gm_current_url() . '" method="POST">';
$form .= '<div class="bpmj_email_wrapper_action">';

// POLE EMAIL

$form .= '<div id="bpmj_gm_email">';
$form .= '<label class="ext-email" for="bpmj_gm_email">' . __('E-mail', 'bpmj_gm'). '*</label>';
$form .= '<input type="text" class="bpmj_gm_email_action required" name="bpmj_gm_email" />';
$form .= '<div class="bpmj_clear"></div></div>';

// POLE IMIĘ
if(isset($adress['fname']) && $adress['fname'] == 'on'){
    if(isset($adress['fname_req']) && $adress['fname_req'] == 'on'){ $fname_class = ' required'; $star = '*';}else{$fname_class = '';}
    $form .= '<div id="bpmj_gm_fname">';
    $form .= '<label for="bpmj_gm_fname">' . __('Imię', 'bpmj_gm') . $star . '</label>';
    $form .= '<input type="text" class="bpmj_gm_fname_action' . $fname_class . '" name="bpmj_gm_fname"  />';
    $form .= '<div class="bpmj_clear"></div></div>';
}

// POLE NAZWISKO
if(isset($adress['lname']) && $adress['lname'] == 'on'){
    if(isset($adress['lname_req']) && $adress['lname_req'] == 'on'){ $lname_class = ' required'; $star = '*';}else{$lname_class = '';$star = '';}
    $form .= '<div id="bpmj_gm_lname">';
    $form .= '<label for="bpmj_gm_lname">' . __('Nazwisko', 'bpmj_gm') . $star . '</label>';
    $form .= '<input type="text" class="bpmj_gm_lname_action' . $lname_class . '" name="bpmj_gm_lname" />';
    $form .= '<div class="bpmj_clear"></div></div>';
}

// POLE ULICA I NUMER
if(isset($adress['street']) && $adress['street'] == 'on'){
    if(isset($adress['street_req']) && $adress['street_req'] == 'on'){ $street_class = ' required'; $star = '*';}else{$street_class = '';$star = '';}
    $form .= '<div id="bpmj_gm_street">';
    $form .= '<label for="bpmj_gm_street">' . __('Ulica z numerem', 'bpmj_gm') . $star . '</label>';
    $form .= '<input type="text" class="bpmj_gm_street_action' . $street_class . '" name="bpmj_gm_street" />';
    $form .= '<div class="bpmj_clear"></div></div>';
}

// POLE MIEJSCOWOŚĆ I KOD POCZTOWY
if(isset($adress['city']) && $adress['city'] == 'on'){
    if(isset($adress['city_req']) && $adress['city_req'] == 'on'){ $city_class = ' required'; $star = '*';}else{$city_class = '';$star = '';}
    
    //MIEJSCOWOŚĆ
    $form .= '<div id="bpmj_gm_city">';
    $form .= '<label for="bpmj_gm_city">' . __('Miejscowość', 'bpmj_gm') . $star . '</label>';
    $form .= '<input type="text" class="bpmj_gm_city_action' . $city_class . '" name="bpmj_gm_city" />';
    $form .= '<div class="bpmj_clear"></div></div>';
    
    // KOD POCZTOWY
    $form .= '<div id="bpmj_gm_post_oode">';
    $form .= '<label for="bpmj_gm_post_code">' . __('Kod pocztowy', 'bpmj_gm') . $star . '</label>';
    $form .= '<input type="text" class="bpmj_gm_post_code_action' . $city_class . '" name="bpmj_gm_post_code" />';
    $form .= '<div class="bpmj_clear"></div></div>'; 
    
}    

// POLE KOD POCZTOWY
if(isset($adress['phone']) && $adress['phone'] == 'on'){
if(isset($adress['phone_req']) && $adress['phone_req'] == 'on'){ $phone_class = ' required'; $star = '*';}else{$phone_class = '';$star = '';}
    $form .= '<div id="id="bpmj_gm_phone>';
    $form .= '<label for="bpmj_gm_phone">' . __('Telefon', 'bpmj_gm') . $star . '</label>';
    $form .= '<input type="text" class="bpmj_gm_phone_action' . $phone_class . '" name="bpmj_gm_phone" />';
    $form .= '<div class="bpmj_clear"></div></div>';
}
if(isset($bpmj_gm_options['form_facebook_login']['active']) && !empty($bpmj_gm_options['form_facebook_login']['active'])){
    $form .= '<div class="bpmj_gm_fb_login_action"><fb:login-button scope="public_profile,email" onlogin="checkLoginState();">Pobierz dane z facebooka
</fb:login-button></div>';
}
$form .= '<div class="bpmj_center"><input' . $submit_bg_border . ' type="submit" class="bpmj_gm_submit_action" id="bpmj_gm_form_submit_action" name="bpmj_gm_form_submit" value="' .  $submit_text . '" /></div>';
$form .= '<div class="bpmj_clear"></div></div>';
$form .= '<input type="hidden" name="bpmj_gm_sc_id" value="' . $shortcode_id . '" />';
$form .= '</form>';
$form .= bpmj_gm_print_form_errors($errors);
$form .= '</div>';








//STYLE CSS

function bpmj_gm_form_action_css(){

?>
<style>
/* 
    Document   : form-action
    Description: Style dla szablonu formularza Action
*/

.bpmj_form_action {
    border:1px solid #999;
    margin: 40px 0;
    padding: 0px 40px 20px 40px;
}


.bpmj-form-title_action {
    background: none repeat scroll 0 0 #FFFFFF;
    display: block;
    margin: -16px auto 30px auto;
    text-align: center;
    width: 47%;
}

.bpmj-form-caption_action {
    margin-bottom: 25px;
    display: block;
    font-weight: bold;
}

.bpmj_form_action input[type="text"]{
   border: 1px solid #999;
   margin-bottom: 10px;
   padding: 3px 10px;
   margin-left: 10px;
}

.gm-form-action label{
    width: 25%;
    text-align: right;
    display: inline-block;
}
.gm-form-action input[type="text"]{
    width: 60%;
}

.bpmj_center {
    text-align: center;
}

#bpmj_gm_form_submit_action {
    background: #d60a02; /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2Q2MGEwMiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjNDA4MDEiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top,  #d60a02 0%, #c40801 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d60a02), color-stop(100%,#c40801)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #d60a02 0%,#c40801 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #d60a02 0%,#c40801 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #d60a02 0%,#c40801 100%); /* IE10+ */
background: linear-gradient(to bottom,  #d60a02 0%,#c40801 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d60a02', endColorstr='#c40801',GradientType=0 ); /* IE6-8 */
-webkit-border-radius: 7px 7px 7px 7px;
border-radius: 7px 7px 7px 7px;
border:1px solid #A50701;
-webkit-box-shadow: 2px 2px 11px 3px #bbb;
box-shadow: 2px 2px 11px 3px #bbb;
color:#fff;
padding: 6px 80px;
margin: 20px 0 20px 0;
-webkit-transition: all 250ms ease-in-out;
-moz-transition: all 250ms ease-in-out;
-ms-transition: all 250ms ease-in-out;
-o-transition: all 250ms ease-in-out;
transition: all 250ms ease-in-out;
}

#bpmj_gm_form_submit_action:hover {
    opacity: 0.7;
}

.bpmj_alert_success {
    background-color: #DFF0D8;
    border:1px solid #D6E9C6;
    color: #3C763D;
    display: block;
    padding: 15px 40px;
    margin:30px 0 ;
}

.bpmj_gm_errors {
    background-color: #fff;
    border:1px solid #ddd;
    color: #A94442;
    display: block;
    padding: 15px 40px;
    margin:10px 0 ;
}

#bpmj_gm_form_action .invalid {
    border:1px solid #A94442;
}

.bpmj_gm_erroro_field_message, .bpmj_gm_erroro_field_message_email {
     background-color: #fff;
    border:1px solid #EBCCD1;
    color: #A94442;
    display: block;
    text-align: center;
    padding: 2px 10px;
    margin-bottom: 30px;
}
.bpmj_gm_fb_login_action {
    text-align: center;
    margin: 7px 0 0 0; 
}
</style>
<?php } ?>
