<?php

/*
 * Szablon Clear formularza shordcode
 * 
 */

/*
 * ZMIENNE DO UŻYCIA W SZABLONIE ZDEFINIOWANE W INNYM MIEJSCU
 * 
 * $form_title - Tytuł formularza
 * $form_description - Opis formularza
 * $shortcode_id - unikatowy ID formularza utworzone w celu uniknięcia konfliktu przy kilku formularzach w jednym widoku. 
 */

// Zdefiniowanie nazwy szablonu
global $gm_templates;
global $bpmj_gm_options;
$gm_templates['clear'] = 'Clear';

// Pobranie ustawień
if(isset($bpmj_gm_options['form_address'])){
    $adress = $bpmj_gm_options['form_address'];
}else {
    $adress['fname'] = ''; $adress['lname'] = ''; $adress['lname_req'] = ''; $adress['fname_req'] = ''; $adress['street'] = ''; 
    $adress['street_req'] = ''; $adress['city'] = ''; $adress['city_req'] = ''; $adress['phone'] = ''; $adress['phone_req'] = '';
}

$star = '';

// Kolor tła i obramowania
if(isset($bpmj_gm_options['bg_color']) && !empty($bpmj_gm_options['bg_color'])){
    $bg_color = 'background:' . $bpmj_gm_options['bg_color'].';';
}else{
 $bg_color = '';   
}

// Kolor obramowania formularza
if(isset($bpmj_gm_options['border_color']) && !empty($bpmj_gm_options['border_color'])){
    $border_color = 'border-color:' . $bpmj_gm_options['border_color'].';';
}else{
 $border_color = '';   
}
// Utworzenie atrybutu style dla tła i koloru obramowania formularza
if(isset($bpmj_gm_options['border_color']) || isset($bpmj_gm_options['border_color'])){
    $border_bg_color = ' style="' . $bg_color . $border_color . '"';
}else{
    $border_bg_color = '';
}

// Kolor nagłówka formularza
if(isset($bpmj_gm_options['head_color']) && !empty($bpmj_gm_options['head_color'])){
    $head_color = ' style="color:' . $bpmj_gm_options['head_color'].';"';
}else{
 $head_color = '';   
}

// Kolor opisu formularza
if(isset($bpmj_gm_options['caption_color']) && !empty($bpmj_gm_options['caption_color'])){
    $caption_color = ' style="color:' . $bpmj_gm_options['caption_color'].';"';
}else{
 $caption_color = '';   
}

// Kolor tekstu formularza
if(isset($bpmj_gm_options['text_color']) && !empty($bpmj_gm_options['text_color'])){
    $text_color = ' style="color:' . $bpmj_gm_options['text_color'].';"';
}else{
 $text_color = '';   
}

// Kolor tła przycisku SUBMIT
if(isset($bpmj_gm_options['submit_bg']) && !empty($bpmj_gm_options['submit_bg'])){
    $submit_bg = 'background:' . $bpmj_gm_options['submit_bg'].'!important;';
}else{
 $submit_bg = '';   
}

// Kolor napisu na przycisku SUBMIT
if(isset($bpmj_gm_options['submit_color']) && !empty($bpmj_gm_options['submit_bg'])){
    $submit_color = 'color:' . $bpmj_gm_options['submit_color'].';';
}else{
 $submit_color = '';   
}

// Utworzenie atrybutu style dla tła i koloru przycisku submit
if(isset($bpmj_gm_options['submit_color']) || isset($bpmj_gm_options['submit_bg'])){
    $submit_bg_border = ' style="' . $submit_bg . $submit_color . '"';
}else{
    $submit_bg_border = '' ;
}

// Domyślny napis na przycisku submit
if(isset($bpmj_gm_options['submit_text']) && !empty($bpmj_gm_options['submit_text'])){
    $submit_text = $bpmj_gm_options['submit_text'];
}else{
 $submit_text = __('Zapisz się', 'bpmj_gm');   
}


// Dodanie klasy rozszerzonego formularza
if(!bpmj_gm_only_email_field()){$css_class = 'bpmj-gm-address_clear';}else{$css_class = '';};

// Budujemy szablon formularza Clear


// Dołączenie styli CSS
$form = bpmj_gm_form_clear_css(); 

// Div otwierający
$form .= '<div' . $border_bg_color . ' class="bpmj-gradient-silver_clear bpmj-gm-form_clear ' . $css_class . '">';

// Tytuł formularza
$form .= '<h3' . $head_color . ' class="bpmj-form-title_clear">' . $form_title . '</h3>';

// Opis formularza
$form .= '<span' . $caption_color . ' class="bpmj-form-caption_clear">' . $form_description . '</span>';

// Formularz HTML
$form .= '<form ' . $text_color . ' id="bpmj_gm_form_' . $shortcode_id . '" class="gm-form-clear_clear bpmj_gm_form" action="' . bpmj_gm_current_url() . '" method="POST">';
$form .= '<div class="bpmj_email_wrapper_clear">';

// POLE EMAIL
$form .= '<div id="bpmj_gm_email_clear">';
$form .= '<input type="text" class="bpmj_gm_email_clear required" name="bpmj_gm_email" placeholder="' . __('Twój adres email', 'bpmj_gm') . '"  />';
$form .= '<input type="submit" class="bpmj_gm_submit_clear" id="bpmj_gm_form_submit_1_clear" name="bpmj_gm_form_submit" value="' .  $submit_text . '" />';
$form .= '<label class="ext-email_clear" for="bpmj_gm_email">' . __('E-mail', 'bpmj_gm'). '*</label>';
$form .= '<div class="bpmj_clear"></div></div>';

// POLE IMIĘ
if(isset($adress['fname']) && $adress['fname'] == 'on'){
    if(isset($adress['fname_req']) && $adress['fname_req'] == 'on'){ $fname_class = ' required'; $star = '*';}else{$fname_class = '';}
    $form .= '<div id="bpmj_gm_fname">';
    $form .= '<input type="text" class="bpmj_gm_fname_clear' . $fname_class . '" name="bpmj_gm_fname" placeholder="' . __('Imię', 'bpmj_gm') . '"  />';
    $form .= '<label for="bpmj_gm_fname">' . __('Imię', 'bpmj_gm') . $star . '</label>';
    $form .= '<div class="bpmj_clear"></div></div>';
}

// POLE NAZWISKO
if(isset($adress['lname']) && $adress['lname'] == 'on'){
    if(isset($adress['lname_req']) && $adress['lname_req'] == 'on'){ $lname_class = ' required'; $star = '*';}else{$lname_class = '';$star = '';}
    $form .= '<div id="bpmj_gm_lname">';
    $form .= '<input type="text" class="bpmj_gm_lname_clear' . $lname_class . '" name="bpmj_gm_lname" placeholder="' . __('Nazwisko', 'bpmj_gm') . '"  />';
    $form .= '<label for="bpmj_gm_lname">' . __('Nazwisko', 'bpmj_gm') . $star . '</label>';
    $form .= '<div class="bpmj_clear"></div></div>';
}

// POLE ULICA I NUMER
if(isset($adress['street']) && $adress['street'] == 'on'){
    if(isset($adress['street_req']) && $adress['street_req'] == 'on'){ $street_class = ' required'; $star = '*';}else{$street_class = '';$star = '';}
    $form .= '<div id="bpmj_gm_street">';
    $form .= '<input type="text" class="bpmj_gm_street_clear' . $street_class . '" name="bpmj_gm_street" placeholder="' . __('Ulica z numerem', 'bpmj_gm') . '"  />';
    $form .= '<label for="bpmj_gm_street">' . __('Ulica z numerem', 'bpmj_gm') . $star . '</label>';
    $form .= '<div class="bpmj_clear"></div></div>';
}

// POLE MIEJSCOWOŚĆ I KOD POCZTOWY
if(isset($adress['city']) && $adress['city'] == 'on'){
    if(isset($adress['city_req']) && $adress['city_req'] == 'on'){ $city_class = ' required'; $star = '*';}else{$city_class = '';$star = '';}
    
    //MIEJSCOWOŚĆ
    $form .= '<div id="bpmj_gm_city">';
    $form .= '<input type="text" class="bpmj_gm_city_clear' . $city_class . '" name="bpmj_gm_city" placeholder="' . __('Miejscowość', 'bpmj_gm') . '"  />';
    $form .= '<label for="bpmj_gm_city">' . __('Miejscowość', 'bpmj_gm') . $star . '</label>';
    $form .= '<div class="bpmj_clear"></div></div>';
    
    // KOD POCZTOWY
    $form .= '<div id="bpmj_gm_post_code">';
    $form .= '<input type="text" class="bpmj_gm_post_code_clear' . $city_class . '" name="bpmj_gm_post_code" placeholder="' . __('Kod pocztowy', 'bpmj_gm') . '"  />';
    $form .= '<label for="bpmj_gm_post_code">' . __('Kod pocztowy', 'bpmj_gm') . $star . '</label>';
    $form .= '<div class="bpmj_clear"></div></div>'; 
    
}    

// POLE KOD POCZTOWY
if(isset($adress['phone']) && $adress['phone'] == 'on'){
if(isset($adress['phone_req']) && $adress['phone_req'] == 'on'){ $phone_class = ' required'; $star = '*';}else{$phone_class = '';$star = '';}
    $form .= '<div id="bpmj_gm_phone">';
    $form .= '<input type="text" class="bpmj_gm_phone_clear' . $phone_class . '" name="bpmj_gm_phone" placeholder="' . __('Telefon', 'bpmj_gm') . '"  />';
    $form .= '<label for="bpmj_gm_phone">' . __('Telefon', 'bpmj_gm') . $star . '</label>';
    $form .= '<div class="bpmj_clear"></div></div>';
}
if(isset($bpmj_gm_options['form_facebook_login']['active']) && !empty($bpmj_gm_options['form_facebook_login']['active'])){
    $form .= '<div class="bpmj_gm_fb_login_clear"><fb:login-button scope="public_profile,email" onlogin="checkLoginState();">Pobierz dane z facebooka
</fb:login-button></div>';
}
$form .= '<div class="bpmj_center"><input' . $submit_bg_border . ' type="submit" class="bpmj_gm_submit_clear" id="bpmj_gm_form_submit_clear" name="bpmj_gm_form_submit" value="' .  $submit_text . '" /></div>';
$form .= '<div class="bpmj_clear"></div></div>';
$form .= '<input type="hidden" name="bpmj_gm_sc_id" value="' . $shortcode_id . '" />';
$form .= '</form>';
$form .= bpmj_gm_print_form_errors($errors);
$form .= '</div>';

//STYLE CSS



function bpmj_gm_form_clear_css(){

?>
<style>
/* 
    Document   : form-clear
    Description: Style dla szablonu formularza Clear
*/

.bpmj-gm-form_clear {
    margin: 20px 0;
    display: block;
    border:1px solid #ddd;
    padding: 15px;
    text-align: center;
}
#gm-form-clear_clear {
    display: block;
    text-align: center;
}
.bpmj_email_wrapper_clear input[type="text"] {
    float: left;
    font-size: 16px;
    color: #777;
    
}

.bpmj-form-title_clear {
    margin: 0;
}
.bpmj-form-caption_clear {
    display: block;
    margin: 0 0 20px 0;
}
.bpmj_clear {
    clear:both;
}

.bpmj_email_wrapper_clear {
    display: inline-block;
}

.bpmj-gm-form_clear  input, .bpmj-gm-form_clear  label {
        border:1px solid #ccc; 
    border-radius: 5px;
    -webkit-border-radius:5px;
    padding: 10px;
    font-size: 18px;
    margin: 5px 0;
    float:left;
}
.bpmj-gm-form_clear  label {
   border:none; 
}
.bpmj-gm-form_clear .ext-email{
    display: none;
}
.bpmj_center {
    text-align: center;
}
.bpmj-gm-address_clear .ext-email{
display: block;
}
#bpmj_gm_form_submit_clear {
    display: none;
} 
.bpmj-gm-address_clear #bpmj_gm_form_submit_clear {
    display: block;
} 
.bpmj-gm-address_clear #bpmj_gm_form_submit_1_clear {
    display: none;
} 
.bpmj-gm-form_clear  input:hover, .bpmj-gm-form_clear  input:focus, .bpmj-gm-form_clear  input:active {
    border:1px solid #ccc; 
}

#bpmj_gm_form_submit_1_clear {
        float: none;
    display: inline-block;
}

.bpmj_gm_submit_clear {
        border:none!important;
    border-radius: 5px!important;;
    -webkit-border-radius:5px!important;;
    background: #777!important;;
    padding: 10px 30px!important;
}



.bpmj_gm_submit_clear:hover, .bpmj_gm_submit_clear:focus, .bpmj_gm_submit_clear:active {
    border:1px solid #ccc;
    opacity: 0.8;
    -webkit-transition: all 150ms ease-in-out;
    -moz-transition: all 150ms ease-in-out;
    -ms-transition: all 150ms ease-in-out;
    -o-transition: all 150ms ease-in-out;
    transition: all 150ms ease-in-out;
}

.bpmj-gradient-silver_clear {
    background: #eeeeee; /* Old browsers */
    background: -moz-linear-gradient(top,  #eeeeee 0%, #d8d8d8 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#eeeeee), color-stop(100%,#d8d8d8)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  #eeeeee 0%,#d8d8d8 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  #eeeeee 0%,#d8d8d8 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  #eeeeee 0%,#d8d8d8 100%); /* IE10+ */
    background: linear-gradient(to bottom,  #eeeeee 0%,#d8d8d8 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#d8d8d8',GradientType=0 ); /* IE6-9 */

}

.bpmj_alert_success_clear {
    background-color: #DFF0D8;
    border:1px solid #D6E9C6;
    color: #3C763D;
    display: block;
    padding: 15px 40px;
    margin:30px 0 ;
}

.bpmj_gm_errors_clear {
    background-color: #fff;
    border:1px solid #ddd;
    color: #A94442;
    display: block;
    padding: 15px 40px;
    margin:10px 0 ;
}

#bpmj_gm_form_clear .invalid {
    border:1px solid #A94442;
}

.bpmj_gm_erroro_field_message_clear, .bpmj_gm_erroro_field_message_email_clear {
     background-color: #fff;
    border:1px solid #EBCCD1;
    color: #A94442;
    display: block;
    text-align: left;
    padding: 2px 10px;
    margin-bottom: 10px;
}
.bpmj_gm_fb_login_clear {
    text-align: left;
    margin: 7px 0;
}
</style>
<?php } ?>