<?php

/*
  Plugin Name: WP GetMail
  Plugin URI: http://upsell.pl/sklep/wp-getmail/
  Description: Wtyczka umożliwia pozyskiwanie danych kontaktowych w zamian za udostępnienie ukrytej cześci strony
  Version: 1.0
  Author: upSell.pl & Better Profits
  Author URI: http://upsell.pl
 */

if (!defined('BPMJ_UPSELL_STORE_URL')) {
    define('BPMJ_UPSELL_STORE_URL', 'http://upsell.pl');
}
define('BPMJ_GM_NAME', 'WP GetMail');

// Definiowanie potrzebnych stałych do ścieżek we wtyczce.
define('BPMJ_GM_PATH', dirname(__FILE__));
define('BPMJ_GM_FILE', __FILE__);
define('BPMJ_GM_INCLUDES', dirname(__FILE__) . '/includes');
define('BPMJ_GM_FOLDER', basename(BPMJ_GM_PATH));
define('BPMJ_GM_URL', plugins_url() . '/' . BPMJ_GM_FOLDER);
define('BPMJ_GM_URL_INCLUDES', BPMJ_GM_URL . '/includes');
define('BPMJ_GM_TEMPLATES', BPMJ_GM_PATH . '/includes/templates');
define('BPMJ_GM_TEMPLATES_URL', BPMJ_GM_URL . '/includes/templates');

// Licencja / Autoaklualizacja
if (!defined('BPMJ_GM_VERSION')) {
    define('BPMJ_GM_VERSION', '1.0');
}

if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	include( BPMJ_GM_PATH . '/includes/EDD_SL_Plugin_Updater.php' );
}

// Wersja bazy danych
global $bpmj_gm_db_version;
$bpmj_gm_db_version = "1.4";

// Zdefiniowanie zmiennej z opcjami
$bpmj_gm_options = get_option('bpmj_getmail');
global $bpmj_gm_options;

// updater
function bpmj_gm_updater() {

        global $bpmj_gm_options;
        
	$license_key = isset($bpmj_gm_options['license_key']) ? trim( $bpmj_gm_options['license_key'] ) : '';

	$bpmj_gm_updater = new EDD_SL_Plugin_Updater( BPMJ_UPSELL_STORE_URL, __FILE__, array(
			'version' 	=> BPMJ_GM_VERSION,
			'license' 	=> $license_key,
			'item_name'     => BPMJ_GM_NAME,
			'author' 	=> 'upSell & Better Profits'
		)
	);

}
add_action( 'admin_init', 'bpmj_gm_updater' );

// Zdefiniowanie tabeli w bazie danych
global $wpdb;

// Tabela z emailami
global $bpmj_gmdb;
$bpmj_gmdb = $wpdb->prefix . 'bpmj_gm_mails';

// Tabela z emailami
global $bpmj_gmdb_lists;
$bpmj_gmdb_lists = $wpdb->prefix . 'bpmj_gm_lists';

// Tabela z emailami
global $bpmj_gmdb_rel;
$bpmj_gmdb_rel = $wpdb->prefix . 'bpmj_gm_rel';


class BPMJ_GM_Plugin_Base {

    
    // Wywołanie potrzebnych akcji w konstruktorze
    function __construct() {
        
        // Wczytuje niezbędne funckje
        include_once(BPMJ_GM_INCLUDES .'/functions.php');
        
        // Wczytuje plik z wszystkimi akcjami
        include_once(BPMJ_GM_INCLUDES .'/actions.php');
        
        // Wczytuje plik z wszystkimi flitrami
        include_once(BPMJ_GM_INCLUDES .'/filters.php');
        
        // Wczytuje plik instalacyjny
        include_once(BPMJ_GM_INCLUDES .'/install.php');
        
        // Ładuje shordcody
        include_once(BPMJ_GM_INCLUDES .'/shortcodes.php');
        
        // Ładuje shordcody
        include_once(BPMJ_GM_INCLUDES .'/assets/phpQuery/phpQuery.php');

        // Tworzy menu w kokpicie
        add_action('admin_menu', array($this, 'bpmj_gm_admin_pages_callback'));

        // Rejestruje zaczep deaktywacji wtyczki
        register_deactivation_hook(BPMJ_GM_FILE, 'bpmj_gm_on_deactivate_callback');

        // Rejestruje tłumaczenia
        add_action('plugins_loaded', array($this, 'bpmj_gm_add_textdomain'));

        // Rejestruje API SETTINGS
        add_action('admin_init', array($this, 'bpmj_gm_register_settings'), 5);

        // Załadowanie CSS
        add_action('admin_enqueue_scripts', array($this, 'bpmj_gm_add_admin_CSS'));
        
        
    }

    
    //Utworzenie menu głównego i 2 menu podrzednych
    function bpmj_gm_admin_pages_callback() {
        add_menu_page(__("WP GetMail", 'bpmj_gm'), __("WP GetMail", 'bpmj_gm'), 'edit_themes', BPMJ_GM_FILE, array($this, 'bpmj_gm_plugin_base'));
        add_submenu_page(BPMJ_GM_FILE, __("E-maile", 'bpmj_gm'), __("E-maile", 'bpmj_gm'), 'edit_themes', BPMJ_GM_FILE, array($this, 'bpmj_gm_plugin_base'));
        add_submenu_page(BPMJ_GM_FILE, __("Listy", 'bpmj_gm'), __("Listy", 'bpmj_gm'), 'edit_themes', 'bpmj_gm-lists-subpage', array($this, 'bpmj_gm_lists_page'));
        add_submenu_page(BPMJ_GM_FILE, __("Ustawienia", 'bpmj_gm'), __("Ustawienia", 'bpmj_gm'), 'edit_themes', 'bpmj_gm-options-subpag', array($this, 'bpmj_gm_options_page'));
    }

    
    // Dołączenie pliku z zawartością strony ze statystykami
    function bpmj_gm_plugin_base() {
        include_once( BPMJ_GM_INCLUDES . '/admin/emails-page.php' );
    }
    
    // Dołączenie pliku z zawartością strony z opcjami
    function bpmj_gm_lists_page() {
        include_once( BPMJ_GM_INCLUDES . '/admin/lists-page.php' );
    }
    
    // Dołączenie pliku z zawartością strony z opcjami
    function bpmj_gm_options_page() {
        include_once( BPMJ_GM_INCLUDES . '/admin/options-page.php' );
    }

    
    //Utworzenie klasy z API Settings
    function bpmj_gm_register_settings() {
        require_once( BPMJ_GM_INCLUDES . '/admin/get-mail-settings.class.php' );
        new BPMJ_GM_Plugin_Settings();
    }

    
    // Rejestracja domeny dla tłumaczeń
    function bpmj_gm_add_textdomain() {
        load_plugin_textdomain('bpmj_gm', false, BPMJ_GM_PATH . '/languages/');
    }

    
    // Rejestracja styli css
    function bpmj_gm_add_admin_CSS() {
        wp_register_style('bpmj-gm-admin-style', BPMJ_GM_URL_INCLUDES . '/admin/css/admin-style.css', array(), '1.0', 'screen');
        wp_enqueue_style('bpmj-gm-admin-style');
        
        if(isset($_GET['page']) && ( $_GET['page'] == 'bpmj_gm-lists-subpage' || $_GET['page'] == 'get-mail/get-mail.php'))
        wp_enqueue_style( 'dashicons' );
    }

}


//Rejestracja deaktywacyjnego zaczepu
function bpmj_gm_on_deactivate_callback() {

}

// Inicjuje wszystko
$bpmj_gm_plugin_base = new BPMJ_GM_Plugin_Base;
?>