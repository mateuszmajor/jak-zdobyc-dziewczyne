function infoBar(settings)
{
    //test to see if we can support this
    if(!document.createElement)
    {
        alert('I\'m sorry, but your browser doesn\'t support this feature.');
        return;
    }
    //create an empty settings object if one isn't provided
    if(!settings)
    {
        settings = {};
    }

    //ie6 doesn't support the 'fixed' css position, so will will have to hack around it
    var ie6 = (document.all && window.external && (typeof document.documentElement.style.maxHeight == 'undefined')) ? true : false;

    //Create and setup the info bar
    var infoBarDiv = document.createElement('div');
    this.currentOpacity = 10;
    this.infoBar = infoBarDiv;
    infoBarDiv.style.backgroundColor = settings.backColor || '#fff68f';
    infoBarDiv.style.opacity = '1';
    infoBarDiv.style.cursor = 'pointer';
    infoBarDiv.style.filter = 'alpha(opacity=100)';
    infoBarDiv.style.top = '0px';
    infoBarDiv.style.left = '0px';
    infoBarDiv.style.padding = '0px';
    infoBarDiv.style.width = '100%';
    infoBarDiv.style.fontColor = settings.fontColor || '#000000';
    //ie6 doesn't support the 'fixed' css position, so will will have to hack around it
    infoBarDiv.style.position = (ie6) ? 'absolute' : 'fixed';
    infoBarDiv.style.display = 'none';
    infoBarDiv.style.border = '1px solid #000000';
    infoBarDiv.style.borderTop = '0px';
    infoBarDiv.style.borderLeft = '0px';
    infoBarDiv.style.borderRight = '0px';
    infoBarDiv.style.zIndex = 9999999;
    infoBarDiv.setAttribute("id", "infoBarTop");
    infoBarDiv.setAttribute("onClick", "closebar();openDialog();");
    //ie6 doesn't support the 'fixed' css position, so will will have to hack around it
    if(ie6)
    {
        window.onscroll = function()
        {
            infoBarDiv.style.top = parseInt(window.pageYOffset || (document.documentElement.scrollTop || 0), 10) + 'px';
            infoBarDiv.style.left = parseInt(window.pageXOffset || (document.documentElement.scrollLeft || 0), 10) + 'px';
        };
    }

    //Create our text entry
    var infoText = document.createElement('div');
    this.infoBar.infoText = infoText;
    infoText.innerHTML = settings.text || '';
    infoText.style.paddingTop = '3px';
    infoText.style.paddingBottom = '3px';

    infoBarDiv.appendChild(infoText);
    document.body.insertBefore(infoBarDiv, document.body.firstChild);
}

infoBar.prototype.show = function(fade)
{
    var that = this;
    if(typeof fade == "number")
    {
        this.currentOpacity = 0;
        this.infoBar.style.opacity = '0';
        this.infoBar.style.filter = 'alpha(opacity=0)';
        this.intervalID = window.setInterval(function()
                                            {
                                                    if(that.currentOpacity < 11)
                                                    {
                                                        that.infoBar.style.opacity = that.currentOpacity / 10;
                                                        that.infoBar.style.filter = 'alpha(opacity=' + that.currentOpacity * 10 + ')';
                                                        that.currentOpacity++;
                                                    }
                                                    else
                                                    {
                                                        window.clearInterval(that.intervalID);
                                                    }
                                            }, fade);
    }
    else
    {
        this.infoBar.style.opacity = '1';
        this.infoBar.style.filter = 'alpha(opacity=100)';
    }
    this.infoBar.style.display = 'block';
};

infoBar.prototype.setText = function(newText)
{
    this.infoBar.infoText.innerHTML = newText;
};

infoBar.prototype.setIcon = function(newIcon)
{
    this.infoBar.icon.src = newIcon + '.png';
};

infoBar.prototype.setFontColor = function(fontColor)
{
    this.infoBar.infoText.style.color = fontColor;
};

infoBar.prototype.setBackColor = function(backColor)
{
    this.infoBar.style.backgroundColor = backColor;
};

infoBar.prototype.hide = function()
{

};

infoBar.prototype.destroy = function()
{

};