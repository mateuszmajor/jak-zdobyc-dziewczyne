<?php
/*
Plugin Name: One-Time Offer Manager
Plugin URI: 
Description: Manage One-Time and Time-Limited Offers
Author: Ralph Pruitt
Version: 0.02.0
Author URI: http://www.iwantmine.com/one-time-offer
Info Location: http://www.iwantmine.com/one-time-offer
*/   
   
/*  Copyright 2010 Ralph L. Pruitt  

*/

/**
* Guess the wp-content and plugin urls/paths
*/
// Pre-2.6 compatibility
if ( ! defined( 'WP_CONTENT_URL' ) )
      define( 'WP_CONTENT_URL', get_option( 'siteurl' ) . '/wp-content' );
if ( ! defined( 'WP_CONTENT_DIR' ) )
      define( 'WP_CONTENT_DIR', ABSPATH . 'wp-content' );
if ( ! defined( 'WP_PLUGIN_URL' ) )
      define( 'WP_PLUGIN_URL', WP_CONTENT_URL. '/plugins' );
if ( ! defined( 'WP_PLUGIN_DIR' ) )
      define( 'WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins' );
define('OTOMGR_PLUGIN_URL', WP_PLUGIN_URL . '/ubam_one_time_offer/');
define('OTOMGR_PLUGIN_DIR', WP_PLUGIN_DIR . '/ubam_one_time_offer/');
define('OTOMGR_TEMPLATE_URL', OTOMGR_PLUGIN_URL . 'templates/');
define('OTOMGR_TEMPLATE_DIR', OTOMGR_PLUGIN_DIR . 'templates/');

require_once dirname(__FILE__) . '/one-time-offer.inc.php';

//instantiate the class
if (class_exists('ubam_oto')) {
    $ubam_oto_var = new ubam_oto();
}


function ubam_oto_install () {
   global $wpdb;

   $ubam_oto_uniques = $wpdb->prefix . "ubam_oto_uniques";
   if($wpdb->get_var("show tables like '$ubam_oto_uniques'") != $ubam_oto_uniques) {
      
      $sql = "CREATE TABLE " . $ubam_oto_uniques . " (
                id int(11) NOT NULL auto_increment,
                offer varchar(20) NOT NULL,
                uv_id varchar(255) NOT NULL,
                ip_addr varchar(15) NULL,
                offer_expires int(11) NULL,
                created_date timestamp NOT NULL default CURRENT_TIMESTAMP,
                PRIMARY KEY  (id),
                UNIQUE KEY idx_" . $ubam_oto_uniques . "_unique (offer, uv_id)
        );";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

   $ubam_oto_offers = $wpdb->prefix . "ubam_oto_offers";
   if($wpdb->get_var("show tables like '$ubam_oto_offers'") != $ubam_oto_offers) {
      
      $sql1 = "CREATE TABLE " . $ubam_oto_offers . " (
                offer_id int(11) NOT NULL auto_increment,
                offer varchar(20) NOT NULL,
                offer_time int(11) NOT NULL default 0,
                offer_period char(1) NOT NULL default 'M',
                offer_page varchar(255) NOT NULL,
                offer_expired_page varchar(255) NOT NULL,
                offer_cookie varchar (20) NOT NULL,
                offer_passwd varchar(20) NULL,
                offer_start_date int(11) default NULL,
                offer_end_date int(11) default NULL,
                PRIMARY KEY  (offer_id),
                UNIQUE KEY idx_" . $ubam_oto_offers . "_unique (offer)
        );";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql1);
    }
}

register_activation_hook(__FILE__,'ubam_oto_install');
 ?>