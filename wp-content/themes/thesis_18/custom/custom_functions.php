<?php

// Using hooks is absolutely the smartest, most bulletproof way to implement things like plugins,
// custom design elements, and ads. You can add your hook calls below, and they should take the 
// following form:
// add_action('thesis_hook_name', 'function_name');
// The function you name above will run at the location of the specified hook. The example
// hook below demonstrates how you can insert Thesis' default recent posts widget above
// the content in Sidebar 1:
// add_action('thesis_hook_before_sidebar_1', 'thesis_widget_recent_posts');

// Delete this line, including the dashes to the left, and add your hooks in its place.

/**
 * function custom_bookmark_links() - outputs an HTML list of bookmarking links
 * NOTE: This only works when called from inside the WordPress loop!
 * SECOND NOTE: This is really just a sample function to show you how to use custom functions!
 *
 * @since 1.0
 * @global object $post
*/

function custom_bookmark_links() {
	global $post;
?>
<ul class="bookmark_links">
	<li><a rel="nofollow" href="http://delicious.com/save?url=<?php urlencode(the_permalink()); ?>&amp;title=<?php urlencode(the_title()); ?>" onclick="window.open('http://delicious.com/save?v=5&amp;noui&amp;jump=close&amp;url=<?php urlencode(the_permalink()); ?>&amp;title=<?php urlencode(the_title()); ?>', 'delicious', 'toolbar=no,width=550,height=550'); return false;" title="Bookmark this post on del.icio.us">Bookmark this article on Delicious</a></li>
</ul>
<?php
}
remove_action('thesis_hook_before_header', 'thesis_nav_menu');
add_action('thesis_hook_after_header', 'thesis_nav_menu');

function post_footer_author() {
if (is_single())
{ ?>
<div class="postauthor">
<img style="width: 80px; height: 80px;" alt=""
src="http://www.gravatar.com/avatar/ed1496a1b215be3eb5494870bc9fa67f?s=80">
<h4>Autor Artykułu: <a href="http://www.jak-zdobyc-dziewczyne.pl/moja-historia/"rel="author">
<?php the_author_firstname(); ?> <?php the_author_lastname(); ?></a></h4>
<p><?php the_author_description(); ?></p>
<p><?php the_author_firstname(); ?> napisał już <span><?php the_author_posts(); ?></span> praktycznych artykułów.</p>
<p>Zobacz <a href="http://www.jak-zdobyc-dziewczyne.pl/video/" rel="nofollow"><b>Video Trening</b></a> o rozpoczynaniu konwersacji i zapisz się na porady mailowe, aby zaoszczędzić sobie lata frustracji i rozczarowań. Zapraszam również do pełnych <a href="http://www.jak-zdobyc-dziewczyne.pl/produkty/" rel="nofollow"><b>Programów Zmiany Osobistej</b></a> oraz do przeczytania historii sukcesu i opinii uczestników.</p>
</div>
<?php }
}
add_action('thesis_hook_after_post_box', 'post_footer_author');

function comment_heading (){?>
<div class="comment-heading">   Zostaw swój komentarz...</div>
<?
}
add_action('thesis_hook_after_post_box' , 'comment_heading');

function limit_front_page() {
	global $query_string;
    if (is_home()) {
        query_posts($query_string.'showposts=10'); //Number of posts you want to show.
    }
}
add_action('thesis_hook_before_content', 'limit_front_page');

/*START CUSTOM FOOTER */

add_action('thesis_hook_footer', 'custom_footer');
function custom_footer() {
?>
    <p>Podrywanie dziewczyn i <a title="jak nabrać pewności siebie" href="https://www.youtube.com/watch?v=FRgy8faTFs0">Jak nabrać pewności siebie </a> | Copyright (C) 2016 Jak-Zdobyc-Dziewczyne.pl<br>
Polistyka prywatności i akceptacja ciasteczek - zobacz <a title="Regulamin serwisu" href="http://www.jak-zdobyc-dziewczyne.pl/regulamin-serwisu/">regulamin serwisu</a><br>
Zacznij zarabiać poprzez <a title="program partnerski" href="http://pp.netigo.pl/?c=pp&f=products&id_cat=2#35" target="_blank" rel="nofollow">program partnerski</a><br>
Przeczytaj opinie i historię o autorze: <a title="Paweł Grzywocz - opinie" href="http://grzywoczpawel.wordpress.com/">Paweł Grzywocz - Jak zdobyć dziewczynę w 12 randek</a></p>
<?php
}

remove_action('thesis_hook_footer', 'thesis_attribution');

/*END CUSTOM FOOTER */

/***BUILD FOOTER WIDGET ***/
register_sidebars(1,
array(
'name' => 'Footer',
'before_widget' => '<li id="%1$s">',
'after_widget' => '</li>',
'before_title' => '<h3>',
'after_title' => '</h3>'
)
);
function widgetized_footer() { ?>
<div id="footer_1">
<ul>
<?php thesis_default_widget(3); ?>
</ul>
</div>
<?php }
add_action('thesis_hook_footer', 'widgetized_footer', '1');
/***FOOTER WIDGET END***/

add_action('pre_get_posts', 'diff_post_count_per_cat');
     
    function diff_post_count_per_cat() {
        if (is_admin()) return;
     
        $cat = get_query_var('category_name');
        switch ($cat) {
            case 'poderwac-dziewczyne':
                set_query_var('posts_per_page', 13);
                break;
            case 'pewnosc-siebie':
                set_query_var('posts_per_page', 13);
                break;
            case 'jak-zagadac-do-dziewczyny':
                set_query_var('posts_per_page', 13);
                break;
            case 'zwiazki':
                set_query_var('posts_per_page', 13);
                break;
            case 'audio-video':
                set_query_var('posts_per_page', 13);
                break;   
            case 'blog':
                set_query_var('posts_per_page', 13);
                break;   
        }
    }

function fb_comments() {
	if (is_single()) { ?>
		<div class="fb_comments">
		<div id="fb-root"></div><script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '2092585290997647',
      xfbml      : true,
      version    : 'v2.4'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script><fb:comments href="<?php the_permalink(); ?>" numposts="5" width="675" publish_feed="true"></fb:comments>
		</div>
	<?php }
}

add_action('thesis_hook_after_post_box', 'fb_comments');

function my_excerpt_length($length) {
   return 50;
}
add_filter('excerpt_length', 'my_excerpt_length');

function no_sidebars() {
	global $post;
	$no_sidebars = get_post_meta($post->ID, 'no_sidebars', true);
	if (is_single() && $no_sidebars)
		return false;
	else
		return true;
}
add_filter('thesis_show_sidebars', 'no_sidebars');
