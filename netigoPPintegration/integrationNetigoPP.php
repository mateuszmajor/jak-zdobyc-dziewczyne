<?php
/* PP Netigo Curl
  * @autor: Daniel Czestki
  * @copyrights: Netigo.pl
 */

require_once($GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH'] . 'baseDBSingleton.php');
require_once($GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH'] . 'netinaPP.php');

class integrationNetigoPP {

	var $partnerNameFromCookie = null;
	var $partnerNameFromUrl = null;
	var $partnerName = null;

	var $kampaniaFromCookie = null;
	var $kampaniaFromUrl = null;
	var $kampaniaName = null;


	var $defaultPartnerName;

	var $cookiePartnerLife;
	var $cookieDomain;

	var $PID = null;
	var $cookiePartnerNameVar;
	var $sessionPartnerNameVar;
	var $urlPartnerNameVar;

	var $oPP = null;
	var $oCurl = null;

	function integrationNetigoPP () {
		$this->cookiePartnerLife = $GLOBALS['NETIGO_PP']['CONFIG']['COOKIE_LIVE_TIME'];

		$this->cookiePartnerNameVar = $GLOBALS['NETIGO_PP']['CONFIG']['cookiePartnerNameVar'];

		$this->cookieKampaniaVar = $this->urlKampaniaVar = "kampania";

		$this->sessionPartnerNameVar = $GLOBALS['NETIGO_PP']['CONFIG']['sessionPartnerNameVar'];
		$this->urlPartnerNameVar = $GLOBALS['NETIGO_PP']['CONFIG']['urlPartnerNameVar'];
		$this->cookieDomain = $GLOBALS['NETIGO_PP']['CONFIG']['COOKIE_DOMAIN'];

		$this->defaultPartnerName = $GLOBALS['NETIGO_PP']['CONFIG']['defaultPartnerName'];

		// nazwa partnera z cookie
		$this->partnerNameFromCookie = (isset($_COOKIE[$this->cookiePartnerNameVar]) ? $_COOKIE[$this->cookiePartnerNameVar] : null);
		$this->partnerNameFromUrl = (isset($_GET[$this->urlPartnerNameVar]) ? $_GET[$this->urlPartnerNameVar] : null);

		$this->partnerName = (($this->partnerNameFromUrl !== null) ? $this->partnerNameFromUrl : $this->partnerNameFromCookie);

		# ustawiamy cookie partnera polecajacego
		if (!empty($this->partnerNameFromUrl)) {
			$this->setAffCookie($this->cookiePartnerNameVar, $this->getPartnerName());
		}

		// kampania
		$this->kampaniaFromCookie = (isset($_COOKIE[$this->cookieKampaniaVar]) ? $_COOKIE[$this->cookieKampaniaVar] : null);
		$this->kampaniaFromUrl = (isset($_GET[$this->urlKampaniaVar]) ? $_GET[$this->urlKampaniaVar] : null);

//echo "x: " . $_GET[$this->urlKampaniaVar];

		$this->kampaniaName = (($this->kampaniaFromUrl !== null) ? $this->kampaniaFromUrl : $this->kampaniaFromCookie);

		# ustawiamy cookie kampanii
		if (!empty($this->kampaniaFromUrl)) {
			$this->setAffCookie($this->cookieKampaniaVar, $this->getKampaniaName());
		}



		if ($GLOBALS['NETIGO_PP']['CONFIG']['NETINA_CURL']) {
			require_once($GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH'] . 'PPCurlClient.php');
			$this->oCurl = new PPCurlClient();
		}
	}

	function getPPInstance() {
		if ($this->oPP === null) {
			if (!empty($GLOBALS['NETIGO_PP']['CONFIG']['DB']['CONNECTION'])) {
			$sql = new SQL_PP();
				$oDB = $sql->getInstance();
				if (PEAR::isError($oDB)) {
					print_r($oDB);
					return;
				}
			}
			$this->oPP = new netinaPP($oDB);
		}
		return $this->oPP;
	}

	function getPartnerName() {
		return empty($this->partnerName) ? null : $this->partnerName;
	}

	function getKampaniaName() {
		return empty($this->kampaniaName) ? "" : $this->kampaniaName;
	}

	function setAffCookie($name, $partnerName) {
		$oPP = $this->getPPInstance();
		$oPP->setAffCookie($name, $partnerName, $this->cookiePartnerLife, $this->cookieDomain);
	}

	

	function setStatCookie($name, $partnerName) {
		$oPP = $this->getPPInstance();
		$oPP->setAffCookie($name, $partnerName, mktime(23,59,59,date("m"),date("d"),date("Y"))-time(), $this->cookieDomain);
	}

	function updateStats($statType, $pid) {
		if (isset($GLOBALS['NETIGO_PP']['CONFIG']['disableStats']) and ($GLOBALS['NETIGO_PP']['CONFIG']['disableStats'])) {
			return;
		}

		$oPP = $this->getPPInstance();
		$partnerName = $this->getPartnerName();


		// wizyta nieunikalna
		if (isset($_COOKIE[$this->cookiePartnerNameVar.''.$statType.$pid]) and ($_COOKIE[$this->cookiePartnerNameVar.''.$statType.$pid] == $partnerName)) {
			if (isset($_SESSION[$this->sessionPartnerNameVar.''.$statType.$pid])) {
				// TODO ??
				//return;
			}
			$unique = false;
		} else {
		// wizyta unikalna
		$unique = true;
		}
		switch ($statType) {
			case 'visits':
				if ($GLOBALS['NETIGO_PP']['CONFIG']['NETINA_CURL']) {
					$this->oCurl->curlInsertVisitsStat($partnerName, $unique ? 'true' : 'false',$this->getKampaniaName());
				} else {
					$oPP->dbInsertVisitsStat($pid, $partnerName, $unique);
				}
				break;
			case 'hits':
				if ($GLOBALS['NETIGO_PP']['CONFIG']['NETINA_CURL']) {
				
					$this->oCurl->curlInsertHitsStat($partnerName, $unique ? 'true' : 'false',$this->getKampaniaName());
					
				} else {
					$oPP->dbInsertHitsStat($pid, $partnerName, $unique);
				}
				break;
			case 'forms':
				if ($GLOBALS['NETIGO_PP']['CONFIG']['NETINA_CURL']) {
					$this->oCurl->curlInsertFormsStat($partnerName, $unique ? 'true' : 'false',$this->getKampaniaName());
				} else {
					$oPP->dbInsertFormsStat($pid, $partnerName, $unique);
				}
				break;
		}
		$_SESSION[$this->sessionPartnerNameVar.''.$statType.$pid] = true;
		$this->setStatCookie($this->cookiePartnerNameVar.''.$statType.$pid, $partnerName);
	}

	function getRedirectUrl() {
		$url = urldecode($_SERVER['REQUEST_URI']);
		preg_match('/(.*)('.PPgetAffRegExp().')(.*)/i', $url, $matches);
		if (!strncmp('http://', $matches[3], 7)) {
			return $matches[3];
		}

		return $matches[1].$matches[3];
	}

	function addCommision($pid, $clientId, $orderId, $wartosc, $partnerName = null) {
		$wartosc = intval($wartosc);
		if ($wartosc <= 0) {
			return false;
		}

		$partnerName = (($partnerName === null) ? $this->getPartnerName() : $partnerName);
		if (!empty($partnerName)) {
			$oPP = $this->getPPInstance();
			$oPP->addAffProFee($pid, $partnerName, $clientId, $orderId, $wartosc, 'new');
		}
	}

	function getPID() {
		return PPgetPID();
	}

	function doStats() {
		// przekierowania z linka partnerskiego
		if ($this->partnerNameFromUrl !== null) {
			// przekierowanie na formularz zamowienia produktu
			if (PPisFormPage()) {
				$this->updateStats('forms', $this->getPID());
			// zwykle przekierowanie
			} else {
				$this->updateStats('hits', $this->getPID());
			}
			if (isset($GLOBALS['NETIGO_PP']['CONFIG']['disableRedirect']) and ($GLOBALS['NETIGO_PP']['CONFIG']['disableRedirect'])) {

			} else {
				$this->redirect($this->getRedirectUrl());
			}
		// zwykla odslona
		} else {
			if (PPisFormPage()) {
				$this->updateStats('forms', $this->getPID());
			// zwykle przekierowanie
			} else {
				$this->updateStats('visits', $this->getPID());
				//$this->updateStats('hits', $this->getPID());
			}

			
		}
	}

	function changeCommisionStatus($pid, $orderId, $status) {
		$oPP = $this->getPPInstance();
		$oPP->dbChangeCommisionStatus($pid, $orderId, $status);
	}

   function redirect($url, $exit = true, $rfc2616 = false) {
		    /**
		     * Absolute URI
		     *
		     * This function returns the absolute URI for the partial URL passed.
		     * The current scheme (HTTP/HTTPS), host server, port, current script
		     * location are used if necessary to resolve any relative URLs.
		     *
		     * Offsets potentially created by PATH_INFO are taken care of to resolve
		     * relative URLs to the current script.
		     *
		     * You can choose a new protocol while resolving the URI.  This is
		     * particularly useful when redirecting a web browser using relative URIs
		     * and to switch from HTTP to HTTPS, or vice-versa, at the same time.
		     *
		     * @author  Philippe Jausions <Philippe.Jausions@11abacus.com>
		     * @static
		     * @access  public
		     * @return  string  The absolute URI.
		     * @param   string  $url Absolute or relative URI the redirect should go to.
		     * @param   string  $protocol Protocol to use when redirecting URIs.
		     * @param   integer $port A new port number.
		     */
		    function absoluteURI($url = null, $protocol = null, $port = null) {
		        // filter CR/LF
		        $url = str_replace(array("\r", "\n"), ' ', $url);

		        // Mess around with already absolute URIs
		        if (preg_match('!^([a-z0-9]+)://!i', $url)) {
		            if (empty($protocol) && empty($port)) {
		                return $url;
		            }
		            if (!empty($protocol)) {
		                $url = $protocol .':'. end($array = explode(':', $url, 2));
		            }
		            if (!empty($port)) {
		                $url = preg_replace('!^(([a-z0-9]+)://[^/:]+)(:[\d]+)?!i',
		                    '\1:'. $port, $url);
		            }
		            return $url;
		        }

		        $host = 'localhost';
		        if (!empty($_SERVER['HTTP_HOST'])) {
		            list($host) = explode(':', $_SERVER['HTTP_HOST']);
		        } elseif (!empty($_SERVER['SERVER_NAME'])) {
		            list($host) = explode(':', $_SERVER['SERVER_NAME']);
		        }

		        if (empty($protocol)) {
		            if (isset($_SERVER['HTTPS']) && !strcasecmp($_SERVER['HTTPS'], 'on')) {
		                $protocol = 'https';
		            } else {
		                $protocol = 'http';
		            }
		            if (!isset($port) || $port != intval($port)) {
		                $port = isset($_SERVER['SERVER_PORT']) ? $_SERVER['SERVER_PORT'] : 80;
		            }
		        }

		        if ($protocol == 'http' && $port == 80) {
		            unset($port);
		        }
		        if ($protocol == 'https' && $port == 443) {
		            unset($port);
		        }

		        $server = $protocol .'://'. $host . (isset($port) ? ':'. $port : '');

		        if (!strlen($url)) {
		            $url = isset($_SERVER['REQUEST_URI']) ?
		                $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
		        }

		        if ($url{0} == '/') {
		            return $server . $url;
		        }

		        // Check for PATH_INFO
		        if (isset($_SERVER['PATH_INFO']) && strlen($_SERVER['PATH_INFO']) &&
		                $_SERVER['PHP_SELF'] != $_SERVER['PATH_INFO']) {
		            $path = dirname(substr($_SERVER['PHP_SELF'], 0, -strlen($_SERVER['PATH_INFO'])));
		        } else {
		            $path = dirname($_SERVER['PHP_SELF']);
		        }

		        if (substr($path = strtr($path, '\\', '/'), -1) != '/') {
		            $path .= '/';
		        }

		        return $server . $path . $url;
		    }
        ob_clean();
        if (headers_sent()) {
            trigger_error('err()');
            if ($exit) {
            	exit;
            } else {
            	return false;
            }
        }

        $url = absoluteURI($url);
        header('Location: '. $url);

        if ($rfc2616 && isset($_SERVER['REQUEST_METHOD']) &&
                $_SERVER['REQUEST_METHOD'] != 'HEAD') {
            printf('Redirecting to: <a href="%s">%s</a>.', $url, $url);
        }
        if ($exit) {
            exit;
        }
        return true;
    }
}

?>