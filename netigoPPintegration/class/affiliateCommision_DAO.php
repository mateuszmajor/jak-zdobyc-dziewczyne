<?php
class AffiliateCommision_DAO {
 
 var $objDB;
 
 function AffiliateCommision_DAO ($db=null) {
  $this->objDB=$db;
 }


 /*
  Pobiera prowizje o podanym id produktu i tranzakcji.
  @param: id produktu
  @param: id tranzakcji		
  @return: tablica obiektu/ów prowizji lub NULL
 */
 
 function doGetCommisionByProductTrans($productId, $transId) {
  
  $sQ="
 SELECT
  *
 FROM
  `pp_commisions_aff`
 WHERE
  `id_product`=?
  AND `idTrans_ext`=?
 ";
  
  $aRes=$this->objDB->getAll($sQ, array($productId, $transId), DB_FETCHMODE_ASSOC);
  #print_pr ($aRes);
  if (PEAR::isError($aRes)) {
   return null;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  if (is_array($aRes) and count($aRes)>0) {
   for ($i=0; $i<count($aRes); $i++) {
    $array[$i]=$this->doReturnCommisionFromArray($aRes[$i]);
	}
	return $array;
  }
  return null;	
 }


 function doReturnCommisionFromArray($aComm) {
  
  return new AffiliateCommision(
  $aComm['id_commision'],
  $aComm['id_product'],
  $aComm['id_affiliate'],
  $aComm['idTrans_ext'],	
  $aComm['name'],
  $aComm['createtime'],
  $aComm['directcom'],
  $aComm['eternalcom'],
  $aComm['level'],	
  $aComm['status']	
  );
 }
}

?>