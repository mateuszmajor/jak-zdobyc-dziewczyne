<?php

class EmailTpl {

 var $id;
 var $action;
 var $info;
 var $signs;
 var $fromname;
 var $fromemail;
 var $replayname;
 var $replayemail;
 var $title;
 var $template;
 var $sending;
 
 function EmailTpl (
  $id=null, 
  $action=null,
  $info=null,
  $signs=null,
  $fromname=null,
  $fromemail=null,
  $replayname=null,
  $replayemail=null,
  $title=null,
  $template=null,
  $sending=null) {
	
  $this->id=$id;
  $this->action=$action;
  $this->info=$info;
  $this->signs=$signs;
  $this->fromname=$fromname;
  $this->fromemail=$fromemail;
  
  $this->replayname=$replayname;	
  $this->replayemail=$replayemail;
  
  $this->title=$title;
  $this->template=$template;
	
  $this->sending=$sending;
 }
}

?>