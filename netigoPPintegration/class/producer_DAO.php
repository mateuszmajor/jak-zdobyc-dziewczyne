<?php

class Producer_DAO {

 var $oDB;
 
 function Producer_DAO ($db=null) {
  $this->oDB=$db;
 }
 
 function doGetProducerFromId($id) {
  $sQ="
   SELECT *
	FROM pp_producers
	WHERE id_producer=?	
  ";
	
  $aRes=$this->oDB->getRow($sQ, array((int)$id), DB_FETCHMODE_ASSOC);
  
  if (PEAR::isError($aRes)) {
   
   return null;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  if (is_array($aRes) and count($aRes)>1) {
   return $this->doReturnProducerFromArray($aRes);
  }
  return null;
 }
 
 function doReturnProducerFromArray($aAff) {
  
  return new Producer(
  $aAff['id_producer'],
  $aAff['identity'],
  $aAff['email'],
  $aAff['pass'],	
  $aAff['hash'],
  $aAff['status'],
  $aAff['producername']
  );
 }
 
}

?>