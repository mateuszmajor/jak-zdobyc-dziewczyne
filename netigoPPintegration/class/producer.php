<?php

class Producer {

 var $id;
 var $identity;
 var $email;
 var $pass;
 var $hash;
 var $status;
 var $name;
 
 function Producer ($id=null, $identity=null, $email=null, $pass=null, $hash=null, $status=null, $producername=null) {
  $this->id=$id;
  $this->identity=$identity;
  $this->email=$email;
  $this->pass=$pass;
  $this->hash=$hash;
  $this->status=$status;
  $this->producername=$producername;
 }
}

?>