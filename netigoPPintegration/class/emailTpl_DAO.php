<?php
class EmailTpl_DAO {
 
 var $objDB;
 
 function EmailTpl_DAO ($db=null) {
  $this->objDB=$db;
 }


 /*
  Pobiera szablon na podstawie akcji.
  @return: obiekt szablonu
 */
 
 function dbGetEmailTplByAction($action) {
  
  $sQ="
 SELECT
  *
 FROM
  pp_config_emailtpl
 WHERE
  action=?
 ";
  
  $aRes=$this->objDB->getRow($sQ, array($action), DB_FETCHMODE_ASSOC);
  #print_pr ($aRes);
  if (PEAR::isError($aRes)) {
   return null;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  if (is_array($aRes) and count($aRes)>1) return $this->doReturnEmailTplFromArray($aRes);
  return null;	
 }


 /*
  Pobiera wszystkie szablony emaili.
  @return: tablica obiektów szablonu
 */
 
 function dbGetAllEmailTpl() {
  
  $sQ="
 SELECT
  *
 FROM
  pp_config_emailtpl
 ";
  
  $aRes=$this->objDB->getAll($sQ, array($action), DB_FETCHMODE_ASSOC);
  #print_pr ($aRes);
  if (PEAR::isError($aRes)) {
   return null;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  if (count($aRes)==0) return null;	
	
  $array=array();
  for ($i=0; $i<count($aRes); $i++) {
   $array[$i]=doReturnEmailTplFromArray($aRes[$i]);
  }
  return $array;
 }
 
 
 
 function doUpdateEmailTpl($oEmailTpl) {
  
  $a=array(
  $oEmailTpl->fromname,
  $oEmailTpl->fromemail,
  $oEmailTpl->replayname,
  $oEmailTpl->replayemail,
  $oEmailTpl->title,
  $oEmailTpl->template,
  $oEmailTpl->sending,
  $oEmailTpl->id			
  ); #print_pr ($a);
  
  $sQ="
 UPDATE
  pp_config_emailtpl
 SET
  fromname=?,
  fromemail=?,
  replayname=?,
  replayemail=?,
  title=?,
  template=?,
  sending=?					  
 WHERE
  id=?
 ";
  
  $aRes=$oDb->query($sQ, $a, DB_FETCHMODE_ASSOC);
  #print_pr ($aRes);
  if (PEAR::isError($aRes)) {
   return null;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  return 1;
 }
 
 
 function doReturnEmailTplFromArray($aEmailTpl) {
  
  return new EmailTpl(
  $aEmailTpl['id'],
  $aEmailTpl['action'],
  $aEmailTpl['info'],
  $aEmailTpl['signs'],	
  $aEmailTpl['fromname'],
  $aEmailTpl['fromemail'],
  $aEmailTpl['replayname'],
  $aEmailTpl['replayemail'],
  $aEmailTpl['title'],
  $aEmailTpl['template'],	
  $aEmailTpl['sending']	
  );
 }
}

?>