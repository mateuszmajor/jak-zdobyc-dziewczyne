<?php
//require_once('DB.php');

/**
 *
 * @author Pawe� Cichorowski <pawel.cichorowski@gmail.com>
 */

$GLOBALS['NETIGO_PP_DB'] = null;
class SQL_PP {
	// ----------------------------------------
	var $oDB=null;

	function getInstance() {

		if (is_null($GLOBALS['NETIGO_PP_DB'])) {
			$GLOBALS['NETIGO_PP_DB'] = DB::connect($GLOBALS['NETIGO_PP']['CONFIG']['DB']['CONNECTION']['engine'].'://'.$GLOBALS['NETIGO_PP']['CONFIG']['DB']['CONNECTION']['user'].':'.$GLOBALS['NETIGO_PP']['CONFIG']['DB']['CONNECTION']['password'].'@'.$GLOBALS['NETIGO_PP']['CONFIG']['DB']['CONNECTION']['host'].'/'.$GLOBALS['NETIGO_PP']['CONFIG']['DB']['CONNECTION']['database']);
			if (PEAR::isError($GLOBALS['NETIGO_PP_DB'])) {
				return $GLOBALS['NETIGO_PP_DB'];
			}
			$GLOBALS['NETIGO_PP_DB']->setFetchMode(DB_FETCHMODE_ASSOC);
			SQL_PP::doSetDBCharacter('mysql', $GLOBALS['NETIGO_PP']['CONFIG']['DB']['CONNECTION']['connection_encoding'], $GLOBALS['NETIGO_PP']['CONFIG']['DB']['CONNECTION']['connection_encoding']);
		}

		return $GLOBALS['NETIGO_PP_DB'];
	}

	function doSetDBCharacter($baseType, $baseCharset, $resultCharset) {
	 	switch (strtolower($baseType)) {
			case "pgsql":
				$q="SET CLIENT_ENCODING TO '$resultCharset';";
				$GLOBALS['NETIGO_PP_DB']->query($q);
				break;
			case "mysql":
				$q="SET character_set_client = `$resultCharset`";
				$GLOBALS['NETIGO_PP_DB']->query($q);

				$q="SET character_set_results = `$resultCharset`";
				$GLOBALS['NETIGO_PP_DB']->query($q);

				$q="SET character_set_connection = `$baseCharset`";
				$GLOBALS['NETIGO_PP_DB']->query($q);
			break;
		}
	}
	// -----------------------

	var $_db;
	var $_res;
	var $_dbType;
	var $_t_char = '`';
	var $_offset = true;
	var $_lastSchema = null;

	function SQL_PP($db=null) {
		if ($db !== null) {
			$this->_db = $db->_db;
			$this->_dbType = $db->_dbType;
			$this->_t_char = $db->_t_char;
		}
	}

	/**
	 * Connects to database.
	 *
	 * @param $dsn string Data Source Name
	 * @return boolean TRUE if connection was made successfully
	 * @author Pawe� Cichorowski <pawel.cichorowski@gmail.com>
	 */
	function connect($dsn) {
		$db = DB::connect($dsn);
		if (DB::isError($db)) {
			//trigger_error($db->toString());
			return false;
		}
		$db->setFetchMode(DB_FETCHMODE_ASSOC);

		$this->_db = $db;

		$array = split(':', $dsn);
		$dbType = strtolower($array[0]);
		$this->_dbType = $dbType;

		switch ($dbType) {
			case 'mysql':
				$this->_t_char = '`';
				$this->_offset = false;
				break;
			case 'pgsql':
				$this->_t_char = '';
				$this->_offset = true;
				break;
		}
		return true;
	}

	/**
	 * Closes database connection.
	 *
	 * @author Pawe� Cichorowski <pawel.cichorowski@gmail.com>
	 */
	function disconnect() {
		$this->_db->disconnect();
	}

	function getCIOperator() {
		return ($this->_dbType == 'pgsql') ? 'ILIKE' : 'LIKE';
	}

	/**
	 * Transaction begin.
	 *
	 * @author Pawe� Cichorowski <pawel.cichorowski@gmail.com>
	 */
	function transactionBegin() {
		$res = $this->_db->query("BEGIN");
		if (DB::isError($res)) {
			trigger_error($this->_res->getMessage()."\r\n".$this->_res->userinfo);
			return false;
		}
		return true;
	}

	/**
	 * Transaction commit.
	 *
	 * @author Pawe� Cichorowski <pawel.cichorowski@gmail.com>
	 */
	function transactionCommit() {
		$res = $this->_db->query("COMMIT");
		if (DB::isError($res)) {
			trigger_error($this->_res->getMessage()."\r\n".$this->_res->userinfo);
			return false;
		}
		return true;
	}

	/**
	 * Transaction rollback.
	 *
	 * @author Pawe� Cichorowski <pawel.cichorowski@gmail.com>
	 */
	function transactionRollback() {
		$res = $this->_db->query("ROLLBACK");
		if (DB::isError($res)) {
			trigger_error($this->_res->getMessage()."\r\n".$this->_res->userinfo);
			return false;
		}
		return true;
	}

	function query($query, $values = array(), $rollback = false) {
		$sth = $this->_db->prepare($query);
		if (DB::isError($sth)) {
			trigger_error($sth->getMessage() ."\r\n".$this->_res->userinfo);
			if ($rollback) {
				$this->transactionRollback();
			}
			showError("B��d w zapytaniu: " . $sth->getMessage());
			return false;
		}
	    $this->_res = $this->_db->execute($sth, $values);
	    $this->_db->freePrepared($sth);
		if (DB::isError($this->_res)) {
			trigger_error($this->_res->getMessage() .":$query", E_USER_NOTICE);
			trigger_error($this->_res->userinfo);
			if ($rollback) {
				$this->transactionRollback();
			}
			showError("B��d w zapytaniu: " . $this->_res->getMessage());
			return false;
		}
		//return $this->_res;
		return true;
	}

	function update($table, $values, $where, $rollback = false) {
		$all_values = array();
		$v_str = array();
		foreach($values as $k => $v) {
			$v_str[] = $this->_prepareFieldName($k).'=?';
			$all_values[] = $v;
		}
		$v_str = implode(', ', $v_str);
		$w_str = array();
		foreach($where as $k => $v) {
			if ($v === null) {
				$w_str[] = $this->_prepareFieldName($k).' is NULL';
			} else {
				$w_str[] = $this->_prepareFieldName($k).'=?';
				$all_values[] = $v;
			}
		}
		$w_str = implode(' AND ', $w_str);
		$query = 'UPDATE '.$this->_prepareFieldName($table).' SET '.$v_str.' WHERE '.$w_str;
		return $this->query($query, $all_values, $rollback);
	}

	function insert($table, $values, $rollback = false) {
		$f_str = array();
		$v_str = array();
		foreach($values as $k => $v) {
			$f_str[] = $this->_prepareFieldName($k);
			$v_str[] = '?';
		}
		$f_str = implode(', ', $f_str);
		$v_str = implode(', ', $v_str);
		$query = 'INSERT INTO '.$this->_prepareFieldName($table).' ('.$f_str.') VALUES ('.$v_str.')';
		return $this->query($query, $values, $rollback);
	}

	function delete($table, $where, $rollback = false) {
		$w_str = array();
		foreach($where as $k => $v) {
			if ($v === null) {
				$w_str[] = $this->_prepareFieldName($k).' is NULL';
			} else {
				$w_str[] = $this->_prepareFieldName($k).'=?';
			}
		}
		$w_str = implode(' AND ', $w_str);
		$query = 'DELETE FROM '.$this->_prepareFieldName($table);
		if (!empty($w_str)) {
			$query .= ' WHERE '.$w_str;
		}
		return $this->query($query, $where, $rollback);
	}

	function affectedRows() {
		return $this->_db->affectedRows();
	}

	function getRows($keyField = null) {
		//TODO multiple keys
//		if (is_array($keyField)) {
//			$keyField = current($keyField);
//		}
		$result = array();
		while ($row = $this->_res->fetchRow()) {
		    if ($keyField === null) {
		    	$result[] = $row;
		    } else
		    if (is_array($keyField)) {
		    	$result[$row[$keyField[0]].'__'.$row[$keyField[1]]] = $row;
			}
		    else {
	    		$result[$row[$keyField]] = $row;
		    }
		}
		return $result;
	}

	function getRowsFlat($field, $keyField = null) {
		$result = array();
		while ($row = $this->_res->fetchRow()) {
		    if ($keyField === null) {
		    	$result[] = $row[$field];
		    } else {
	    		$result[$row[$keyField]] = $row[$field];
		    }
		}
		return $result;
	}

	function getRowsMulti($keyField = null, $fields = array()) {
		$result = array();
		while ($row = $this->_res->fetchRow()) {
		    if ($keyField === null) {
		    	$result[] = $row;
		    } else {
		    	if (in_array($row[$keyField], $fields)) {
		    		$result[$row[$keyField]][] = $row;
		    	} else {
		    		$result[$row[$keyField]] = $row;
		    	}
		    }
		}
		return $result;
	}

	function getRow() {
		return $this->_res->fetchRow();
	}

	function get($field) {
		$row = $this->_res->fetchRow();
		return $row[$field];
	}

	function nextId($seqName, $onDemand = false) {
		$id = $this->_db->nextId($seqName, $onDemand);
		if (DB::isError($id)) {
			return false;
		}
		return 	$id;
	}

	function lastId() {
		$this->query('SELECT LAST_INSERT_ID() as lastId');
		$row = $this->getRow();
		return $row['lastId'];
	}

	function getTables() {
		$this->query('SELECT table_name from information_schema.tables WHERE table_schema=\'public\'');
		$tables = $this->getRows();
		$result = array();
		foreach ($tables as $t) {
			$result[] = $t['table_name'];
		}
		return $result;
	}

	function emptyTable($table) {
		$this->query('DELETE FROM '.$this->_prepareFieldName($table));
	}

	/**
	 * Gets current database time.
	 *
	 * @return string
	 * @author Pawe� Cichorowski <pawel.cichorowski@gmail.com>
	 */
	function getNow() {
		$this->query('SELECT NOW() as now');
		$row = $this->getRow();
		return $row['now'];
	}

	function _likeReplace($string) {
		$result = '';
		for($i = 0; $i < strlen($string); $i++) {
			$char = $string[$i];
			switch ($char) {
				case '%': $result .= '\\%';
					continue;
				case '_': $result .= '\\_';
					continue;
				default: $result .= $char;
			}
		}
		return $result;
	}

	function _prepareFieldName($fieldName) {
/*		$result1 = array();
		$arr1 = preg_split('/[,\s]/', $fieldName, -1, PREG_SPLIT_NO_EMPTY);
		foreach ($arr1 as $v1) {
			$result = array();
			$arr = preg_split('/[.\s]/', $v1, -1, PREG_SPLIT_NO_EMPTY);
			foreach ($arr as $v) {
				$result[] = $this->_t_char.addslashes($v).$this->_t_char;
			}
			$result1[] = implode('.', $result);
		}

		return implode(', ', $result1);
*/
		$result =  preg_replace('/([\w\d_]+)/', $this->_t_char.'$1'.$this->_t_char, $fieldName);
		return addslashes(preg_replace('/('.$this->_t_char.'as'.$this->_t_char.')/', 'as', $result));
	}

	function getCount() {
		$count = 0;
		if (isset($this->_lastSchema['GROUP_BY']) and !empty($this->_lastSchema['GROUP_BY'])) {
			while ($row = $this->getRow()) {
				$count += 1;
			}
		} else {
			$count = $this->get('count');
		}
		return $count;
	}

	function constructTail($query, $schema) {
		if (isset($schema['GROUP_BY']) and !isEmpty($schema['GROUP_BY'])) {
			$query .= 'GROUP BY '.$this->_prepareFieldName(implode(', ', $schema['GROUP_BY'])).' ';
		}

		if (isset($schema['TAIL']) and !isEmpty($schema['TAIL'])) {
			$query .= $schema['TAIL'].' ';
		}

		if (isset($schema['ORDER_BY']) and !isEmpty($schema['ORDER_BY'])) {
			$columns = preg_split('/[, ]/', $schema['ORDER_BY'], -1, PREG_SPLIT_NO_EMPTY);
			$orders = null;
			for ($i = 0; $i < count($columns); $i+=2) {
				$orders[$columns[$i]] = ($columns[$i+1] ? 1: 0);
			}
			if (!empty($orders)) {
				$result['ORDER_BY'] = $orders;
			}

			foreach ($result['ORDER_BY'] as $field => $value) {
				$orderBy[] = $this->_prepareFieldName($field).' '.($value ? 'ASC' : 'DESC');
			}
			if (!empty($orderBy)) {
				$orderBy = implode(', ', $orderBy);
				$query .=  'ORDER BY '.$orderBy.' ';
			}
		}
		if ($this->_offset) {
			if (isset($schema['OFFSET']) and isNatural($schema['OFFSET'])) {
				$query .= 'OFFSET '.$schema['OFFSET'].' ';
			}
			if (isset($schema['LIMIT']) and isNatural($schema['LIMIT'])) {
				$query .= 'LIMIT '.$schema['LIMIT'].' ';
			}
		} else {
			if (isset($schema['LIMIT']) and isNatural($schema['LIMIT'])) {
				if (isset($schema['OFFSET']) and isNatural($schema['OFFSET'])) {
					$query .= 'LIMIT '.$schema['OFFSET'].', '.$schema['LIMIT'].' ';
				} else {
					$query .= 'LIMIT '.$schema['LIMIT'].' ';
				}
			}
		}
		return $query;
	}

	function constructQuery($schema, &$query, &$values) {
		if (isset($schema['COUNT']) and $schema['COUNT']) {
			$query = 'SELECT count(*) as count ';
		} else {
			$query = 'SELECT '.$schema['SELECT'].' ';
		}

		if ($this->_dbType != 'mysql')
		if (isset($schema['GROUP_BY']) and is_array($schema['GROUP_BY']) and isset($schema['ORDER_BY']) and is_array($schema['ORDER_BY'])) {
			$schema['GROUP_BY'] = array_unique(array_merge($schema['GROUP_BY'], array_keys($schema['ORDER_BY'])));
		}

		if (isset($schema['FROM']) and !isEmpty($schema['FROM'])) {
			$query .= ($schema['FROM'][0] == '%') ? ('FROM ('.substr($schema['FROM'], 1).') ') : ('FROM ('.$this->_prepareFieldName($schema['FROM']).') ');
			//$query .= 'FROM '.$this->_prepareFieldName($schema['FROM']).' ';
			//$query .= 'FROM '.$schema['FROM'].' ';
		}

		if (isset($schema['JOIN']) and !isEmpty($schema['JOIN'])) {
			$join = implode(', ', $schema['JOIN']);
			$query .= $join.' ';
		}

		$where = array();
		$values = array();

		if (isset($schema['WHERE_CUSTOM']) and is_array($schema['WHERE_CUSTOM']))
		foreach ($schema['WHERE_CUSTOM'] as $field => $value) {
			$where[] = $field;
			$values = array_merge($values, $value);
		}

		if (isset($schema['WHERE_EQUAL']) and is_array($schema['WHERE_EQUAL']))
		foreach ($schema['WHERE_EQUAL'] as $field => $value)
		if (!isEmpty($value)) {
			if ($value === null) {
				$where[] = $this->_prepareFieldName($field).' is NULL';
			} else {
				$where[] = $this->_prepareFieldName($field).'=?';
				$values[] = $value;
			}
		}

		if (isset($schema['WHERE_LIKE']) and is_array($schema['WHERE_LIKE']))
		foreach ($schema['WHERE_LIKE'] as $field => $value)
		if (!isEmpty($value)) {
			//$where[] = $this->_prepareFieldName($field).' '.$this->getCIOperator().' ((\'%\' || ?) || \'%\')';
			$where[] = $this->_prepareFieldName($field).' '.' LIKE CONCAT(\'%\',?,\'%\')';
			$values[] = $this->_likeReplace($value);
		}

		if (isset($schema['SEARCH']) and (!isEmpty($schema['SEARCH'][0]))) {
			$words = preg_split("/[\\s,]+/", $schema['SEARCH'][0], -1, PREG_SPLIT_NO_EMPTY);
			foreach ($words as $word) {
				$search = array();
				foreach ($schema['SEARCH'][1] as $field) {
					//$search[] = '('.$this->_prepareFieldName($field).' '.$this->getCIOperator().' ((\'%\' || ?) || \'%\'))';
					$search[] = '('.$this->_prepareFieldName($field).' LIKE CONCAT(\'%\',?,\'%\'))';
					$values[] = $this->_likeReplace($word);
				}
				if (!empty($search)) {
					$search = '('.implode(' OR ', $search).')';
					$where[] = $search;
				}
			}
		}

		if (isset($schema['WHERE_IN']) and is_array($schema['WHERE_IN']))
		foreach ($schema['WHERE_IN'] as $field => $value)
		if (!isEmpty($value)) {
			$tmp = array();
			foreach ($value as $v) {
				$tmp[] = '?';
				$values[] = $v;
			}
			$where[] = $this->_prepareFieldName($field).' IN ('.implode(', ', $tmp).')';
		}

		if (!empty($where)) {
			$where = '('.implode(') AND (', $where).')';
			$query .=  'WHERE '.$where.' ';
		}

		$query = $this->constructTail($query, $schema);
	}

	function select($schema, $rollback = true) {
		$this->constructQuery($schema, $query, $values);
		$this->_lastSchema = $schema;
		return $this->query($query, $values, $rollback);
	}

	function free() {
		if (is_object($this->_res)) {
			$this->_res->free();
		}
	}

	function _query($query) {
		$this->_res = $this->_db->query($query);
		if (DB::isError($this->_res)) {
			trigger_error($this->_res->getMessage() .":$query", E_USER_NOTICE);
			trigger_error($this->_res->userinfo);
			return false;
		}
		return true;
	}

	function importFile($fileName) {
		$fp = fopen($fileName, 'rb');
		if (!$fp) {
			return false;
		}
		$line = '';
		while (!feof($fp)) {
			$buffer = fgets($fp, 100000);
			$buffer = trim($buffer);
			if (empty($buffer)) {
				continue;
			}
			if (preg_match("/^--(.*)/", $buffer)) {
				continue;
			}
			$line .= $buffer;
			if (preg_match("/(.*);$/", $buffer)) {
				if (!empty($line)) {
					if (!$this->_query($line)) {
						fclose($fp);
						return false;
					}
				}
				$line = '';
			}
		}

		fclose($fp);

		return true;
	}
}
?>