<?php
class ProducerCommision_DAO {
 
 var $objDB;
 
 function ProducerCommision_DAO ($db=null) {
  $this->objDB=$db;
 }


 /*
  Pobiera prowizje producent.
  @param: id produktu
  @param: id tranzakcji		
  @return: obiekt prowizji
 */
 
 function doGetCommisionByProductTrans($productId, $transId) {
  
  $sQ="
 SELECT
  *
 FROM
  `pp_commisions_pro`
 WHERE
  `id_product`=?
  AND `idTrans_ext`=?
 ";
  
  $aRes=$this->objDB->getRow($sQ, array($productId, $transId), DB_FETCHMODE_ASSOC);
  #print_pr ($aRes);
  if (PEAR::isError($aRes)) {
   return null;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  if (is_array($aRes) and count($aRes)>1) return $this->doReturnCommisionFromArray($aRes);
  return null;	
 }


 function doReturnCommisionFromArray($aComm) {
  
  return new ProducerCommision(
  $aComm['id_commision'],
  $aComm['id_producer'],
  $aComm['id_product'],
  $aComm['idTrans_ext'],	
  $aComm['name'],
  $aComm['createtime'],
  $aComm['commision'],
  $aComm['status']	
  );
 }
}

?>