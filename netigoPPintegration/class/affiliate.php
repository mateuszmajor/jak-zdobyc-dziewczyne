<?php
class Affiliate {
 
 var $id;
 var $identity;
 var $pass;
 var $email;
 var $hash;
 var $joinTime;
 var $name;
 var $surname;
 var $companyName;
 var $street;
 var $postalCode;
 var $city;
 var $country;
 var $vat;
 var $nip;
 var $pesel;
 var $telephone;
 var $fax;
 var $bankName;
 var $bankCode;
 var $bankSwiftCode;
 var $accountNumber;
 var $revenueName;
 var $revenueAddress;
 var $agreement;
 var $urls;
 var $newsletter;
 var $status;
 
 function Affiliate (
 
 $id=null,
 $identity=null,
 $pass=null,
 $email=null,
 $hash=null,
 $joinTime=null,
 $name=null,
 $surname=null,
 $companyName=null,
 $street=null,
 $postalCode=null,
 $city=null,
 $country="Polska",
 $vat="N",
 $nip=null,
 $pesel=null,
 $telephone=null,
 $fax=null,
 $bankName=null,
 $bankCode=null,
 $bankSwiftCode=null,
 $accountNumber=null,
 $revenueName=null,
 $revenueAddress=null,
 $agreement="N",
 $urls=null,
 $newsletter='Y',	
 $status="new"
 ) {
  $this->id=$id;
  $this->identity=$identity;
  $this->pass=$pass;
  $this->email=$email;
  $this->hash=$hash;
  $this->joinTime=$joinTime;
  $this->name=$name;
  $this->surname=$surname;
  $this->companyName=$companyName;
  $this->street=$street;
  $this->postalCode=$postalCode;
  $this->city=$city;
  $this->country=$country;
  $this->vat=$vat;
  $this->nip=$nip;
  $this->pesel=$pesel;
  $this->telephone=$telephone;
  $this->fax=$fax;		
  $this->bankName=$bankName;
  $this->bankCode=$bankCode;
  $this->bankSwiftCode=$bankSwiftCode;
  $this->accountNumber=$accountNumber;
  $this->revenueName=$revenueName;
  $this->revenueAddress=$revenueAddress;
  $this->agreement=$agreement;
  $this->urls=$urls;
  $this->newsletter=$newsletter;	
  $this->status=$status;
 }
}

?>