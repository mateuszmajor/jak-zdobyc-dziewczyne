<?php


require_once($GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH'].'mailer.php');

class netinaPP {
 var $addProducerCommision;
 var $defaultAffiliate;
 var $logPath="logs/";

# obiekt bazy danych peer DB
 var $oDb;
 # tablica z danymi partnera
 var $aAff = array(
 "id_affiliat"=>"",
 "identity"=>"",
 "pass"=>"",
 "email"=>"",
 "hash"=>"",
 "jointime"=>"",
 "status"=>"",
 "name"=>"",
 "surname"=>"",
 "street"=>"",
 "postalcode"=>"",
 "city"=>"",
 "nip"=>"",
 "pesel"=>"",
 "bankname"=>"",
 "accountnumber"=>"",
 "revenuename"=>"",
 "revenueaddress"=>"",
 );
 # tablica z danymi polecanego produktu
 var $aPP=array(
 "id_product"=>"",
 "idProduct_Ext"=>"",
 "name"=>"",
 "afflink"=>"",
 "affcomtype"=>"",
 "afflevel"=>"",
 "procomtype"=>"",
 "prolevel"=>"",
 "info"=>""
 );

 var $iAff=0; # [0,1] 1 - jest polecajacy, 0 - brak polecajacego
 var $iPP=0; # brak produktu

 function netinaPP($oDb=null) {
 	$this->addProducerCommision = $GLOBALS['NETIGO_PP']['CONFIG']['BLOCKCTRL']['enable_producers'];
 	$this->defaultAffiliate = $GLOBALS['NETIGO_PP']['CONFIG']['defaultPartner']['identity'];
  	if ($oDb) $this->oDb=$oDb;
 }

 function setAffCookie($sCookieName, $sAffNameId, $iExpire=0, $sDomain="", $sPath="/") {
  return SetCookie($sCookieName,$sAffNameId,time()+$iExpire,$sPath,$sDomain,0);
 }

 /*
 Funkcja zarzadzajaca calym procesem tworzenia prowizji
 parametry:
 1. id produktu
 2. identyfikator polecajacego
 3. id klienta
 4. unikalny numer tranzakcji dla produktu
 5. wartosc tranzakcji w groszach
 6. status [new,confirmed,canceled]
 */

 function addAffProFee($iProductId, $sPartnerIdentity, $sBuyerId, $sTransId, $iTransPrice, $sStatus="new") {

  #domyslnie brak partnerów
  # partner przypisany
  $aInputableAffComm=0;	#tablica prowizji indywidualnych produktu lub 0 gdy brak
  $aInputableAff=0;		#tablica zawierajaca dane partnera
  $iInputableAff=0;		#czy istnieje partner 0-nie, 1-tak
  $sInputableAff="";		#identyfikator partnera

  # partner z cookie
  $aCookieAffComm=0;
  $aCookieAff=0;
  $iCookieAff=0;
  $sCookieAff="";

//  # II Poziom
//  $iInputableAffLevel2=0;		#czy istnieje partner II poziomu partnera przypisanego 0-nie, 1-tak
//  $aInputableAffLevel2=0;		#tablica zawierajaca dane partnera
//
//  $iCookieAffLevel2=0;			# partner 2 poziomu partnera z cookie
//  $aCookieAffLevel2=0;
  # produkt partnerski
  $this->dbSetPPProduct($iProductId);
  if ($this->aPP['enabled']=='N') return 0;
  if ($this->iPP==0) return 0;

  #print_pr ("$iProductId,$sBuyerId");
  # pobieramy liste "identyfikatorow" partnerow przypisanych do klienta
  $aBuyerAffIdentity=$this->dbGetAffiliatesIdentityFromBuyer($iProductId,$sBuyerId);
  #print_pr ($aBuyerAffIdentity);


  if (is_array($aBuyerAffIdentity) and !empty($sBuyerId)) {
   #biezemy pierwszego partnera
   $sInputableAff=$aBuyerAffIdentity[0]['identity']; #print $sInputableAff;

   #wypelniamy klase danymi partnera
   $this->dbSetAffiliateFromIdentity($sInputableAff);
   $aInputableAff=$this->aAff;
   $iInputableAff=$this->iAff;
   $aInputableAffComm=$this->dbGetAffiliateProductCommision($aInputableAff['id_affiliate'],$iProductId); #print_r ($aAPC);
   #print_pr ($aInputableAff);
   #print_pr ($aInputableAffComm);
  }

  #partner z cookie
  $this->dbSetAffiliateFromIdentity($sPartnerIdentity);
  if ($this->iAff) {

   $aCookieAff=$this->aAff;
   $iCookieAff=$this->iAff;
   $sCookieAff=$aCookieAff['identity'];
   $aCookieAffComm=$this->dbGetAffiliateProductCommision($aCookieAff['id_affiliate'],$iProductId); #print_r ($aAPC);
   #print_pr ($aCookieAff);
   #print_pr ($aCookieAffComm);
  }

  #brak partnera z cookie i brak przypisanego partnera
  if ($iInputableAff==0 and $iCookieAff==0) return 0;

  # jest partner cookie, ale brak partnera przypisanego
  if ($iCookieAff==1 and $iInputableAff==0) {
   #przypisujemy partnera do klienta
   if ($sBuyerId<>'0') $iRes=$this->dbAddAffiliateToBuyer($iProductId,$aCookieAff['id_affiliate'],$sBuyerId);
   #podstawiamy pod dane przypisanego partnera dane partnera z cookie
   $aInputableAffComm=$aCookieAffComm;
   $aInputableAff=$aCookieAff;
   $iInputableAff=$iCookieAff;
   $sInputableAff=$sCookieAff;
  }

  #prowizja producenta
  switch ($this->aPP['procomtype']) {
   case "percent":
   #obliczamy procent z wartosci
   $iProComLevel=round($iTransPrice/100*$this->aPP['procomlevel']);
   break;
   case "single":
   #obliczamy procent z wartosci
   $iProComLevel=$this->aPP['procomlevel'];
   break;
  }

  #prowizje domyslne partnerów
  $iCookieAffComLevel=0;						#partner cookie - prowizja bezposrednia
  $iCookieAffEternalComLevel=0;				#partner cookie - prowizja wieczna

  $iInputableAffComLevel=0;					#partner przypisany - prowizja bezposrednia
  $iInputableAffEternalComLevel=0;			#partner przypisany - prowizja wieczna

  #prowizje domy¶lne partnerów I poziomu

  $iCookieAffLevel1ComLevel=0;				#partner I poziomu partnera z cookie - prowizja bezposrednia
  $iCookieAffLevel1EternalComLevel=0;		#partner I poziomu partnera z cookie - prowizja wieczna

  $iInputableAffLevel1ComLevel=0;			#partner I poziomu partnera przypisanego - prowizja bezposrednia
  $iInputableAffLevel1EternalComLevel=0;	#partner I poziomu partnera przypisanego - prowizja wieczna

  #domyslna prowizja bezposrednia - partner cookie i przypisany
  switch ($this->aPP['affcomtype']) {
   case "percent":
   #obliczamy procent z wartosci
   $iCookieAffComLevel=round($iTransPrice/100*$this->aPP['affcomlevel']);
   $iInputableAffComLevel=$iCookieAffComLevel;
   break;
   case "single":
   #obliczamy procent z wartosci
   $iCookieAffComLevel=$this->aPP['affcomlevel'];
   $iInputableAffComLevel=$iCookieAffComLevel;
   break;
  }
  ;

  #domyslna prowizja wieczna - partner z cookie i przypisany
  switch ($this->aPP['affeternalcomtype']) {

   case "percent":
   #obliczamy procent z wartosci
   $iCookieAffEternalComLevel=round($iTransPrice/100*$this->aPP['affeternalcomlevel']);
   $iInputableAffEternalComLevel=$iCookieAffEternalComLevel;
   break;
   case "single":
   #obliczamy procent z wartosci
   $iCookieAffEternalComLevel=$this->aPP['affeternalcomlevel'];
   $iInputableAffEternalComLevel=$iCookieAffEternalComLevel;
   break;
  }
  ;


  #jeżeli partner cookie ma indywidualne ustawienia prowizji
  #to je podmieniamy
  if (is_array($aCookieAffComm)) {
   #prowizja indywidualna - bezposrednia
   #print "Partner cookie - prowizje indywidualne<br />";
   #print_pr ($aCookieAffComm);
   #print_pr ("Prowizja indywidualna partnera z cookie<br />");
   switch ($aCookieAffComm['comtype']) {
    case "percent":
    #obliczamy procent z wartosci
    $iCookieAffComLevel=round($iTransPrice/100*$aCookieAffComm['comvalue']);
    #print "naliczam {$aCookieAffComm['comvalue']} procent ($iCookieAffComLevel groszy) prowizji bezposredniej <br />";
    break;
    case "single":
    #obliczamy procent z wartosci
    $iCookieAffComLevel=$aCookieAffComm['comvalue'];
    #print "naliczam {$aCookieAffComm['comvalue']} groszy";
    break;
   }
   ;
   #prowizja indywidualna - wieczna
   switch ($aCookieAffComm['eternalcomtype']) {
    case "percent":
    #obliczamy procent z wartosci
    $iCookieAffEternalComLevel=round($iTransPrice/100*$aCookieAffComm['eternalcomvalue']);
    #print "naliczam {$aCookieAffComm['eternalcomvalue']} procent prowizji wiecznej <br />";
    break;
    case "single":
    #obliczamy procent z wartosci
    $iCookieAffEternalComLevel=$aCookieAffComm['eternalcomvalue'];
    #print "naliczam {$aCookieAffComm['eternalcomvalue']} procent prowizji wiecznej <br />";
    break;
   }
   ;
  }

  #print $iCookieAffComLevel;

  #jeżeli partner przypisany ma indywidualne ustawienia prowizji
  #to je podmieniamy
  if (is_array($aInputableAffComm)) {
   #prowizja bezposrednia
   #print_pr ("Prowizja indywidualna partnera przypisanego");
   #print_pr ($aInputableAffComm);
   switch ($aInputableAffComm['comtype']) {
    case "percent":
    #obliczamy procent z wartosci
    $iInputableAffComLevel=round($iTransPrice/100*$aInputableAffComm['comvalue']);
    break;
    case "single":
    #obliczamy procent z wartosci
    $iInputableAffComLevel=$aInputableAffComm['comvalue'];
    break;
   }
   ;
   #prowizja wieczna
   switch ($aInputableAffComm['eternalcomtype']) {
    case "percent":
    #obliczamy procent z wartosci
    $iInputableAffEternalComLevel=round($iTransPrice/100*$aInputableAffComm['eternalcomvalue']);
    break;
    case "single":
    #obliczamy procent z wartosci
    $iInputableAffEternalComLevel=$aInputableAffComm['eternalcomvalue'];
    break;
   }
   ;
  }

	// II poziom

  #pobieramy id partnera I poziomu partnera z cookie
  if ($iCookieAff) {
   $iCookieAffLevel1=$this->dbGetAffIdLevel1($aCookieAff['id_affiliate']);			# partner 1 poziomu partnera z cookie
  }

  #pobieramy id partnera I poziomu partnera przypisanego
  if ($iInputableAff) {
   $iInputableAffLevel1=$this->dbGetAffIdLevel1($aInputableAff['id_affiliate']);
  }


  switch ($this->aPP['affsecondcomtype']) {

   case "percent":
   #obliczamy procent z wartosci

   #partner I poziomu partnera z cookie - prowizja bezposrednia
   $iCookieAffLevel1ComLevel=round($iCookieAffComLevel/100*$this->aPP['affsecondcomlevel']);
   #partner I poziomu partnera z cookie - prowizja wieczna
   $iCookieAffLevel1EternalComLevel=round($iCookieAffEternalComLevel/100*$this->aPP['affsecondcomlevel']);

   #partner I poziomu partnera przypisanego - prowizja bezposrednia
   $iInputableAffLevel1ComLevel=round($iInputableAffComLevel/100*$this->aPP['affsecondcomlevel']);
   #partner I poziomu partnera przypisanego - prowizja wieczna
   $iInputableAffLevel1EternalComLevel=round($iInputableAffEternalComLevel/100*$this->aPP['affsecondcomlevel']);

   break;

   case "single":
   #partner I poziomu partnera z cookie - prowizja bezposrednia
   $iCookieAffLevel1ComLevel=$this->aPP['affsecondcomlevel'];
   #partner I poziomu partnera z cookie - prowizja wieczna
   $iCookieAffLevel1EternalComLevel=$this->aPP['affsecondcomlevel'];

   #partner I poziomu partnera przypisanego - prowizja bezposrednia
   $iInputableAffLevel1ComLevel=$this->aPP['affsecondcomlevel'];
   #partner I poziomu partnera przypisanego - prowizja wieczna
   $iInputableAffLevel1EternalComLevel=$this->aPP['affsecondcomlevel'];


   break;
  }
  ;

	// III poziom

  #pobieramy id partnera II poziomu partnera z cookie
  if ($iCookieAffLevel1) {
   $iCookieAffLevel2=$this->dbGetAffIdLevel1($iCookieAffLevel1);			# partner 2 poziomu partnera z cookie
  } else {
  	$iCookieAffLevel2 = 0;
  }

  #pobieramy id partnera I poziomu partnera przypisanego
  if ($iInputableAffLevel1) {
   $iInputableAffLevel2=$this->dbGetAffIdLevel1($iInputableAffLevel1);
  }


  switch ($this->aPP['affthirdcomtype']) {

   case "percent":
   #obliczamy procent z wartosci

   #partner I poziomu partnera z cookie - prowizja bezposrednia
   $iCookieAffLevel2ComLevel=round($iCookieAffLevel1ComLevel/100*$this->aPP['affthirdcomlevel']);
   #partner I poziomu partnera z cookie - prowizja wieczna
   $iCookieAffLevel2EternalComLevel=round($iCookieAffLevel1EternalComLevel/100*$this->aPP['affthirdcomlevel']);

   #partner I poziomu partnera przypisanego - prowizja bezposrednia
   $iInputableAffLevel2ComLevel=round($iInputableAffLevel1ComLevel/100*$this->aPP['affsecondcomlevel']);
   #partner I poziomu partnera przypisanego - prowizja wieczna
   $iInputableAffLevel2EternalComLevel=round($iInputableAffLevel1EternalComLevel/100*$this->aPP['affthirdcomlevel']);

   break;

   case "single":
   #partner I poziomu partnera z cookie - prowizja bezposrednia
   $iCookieAffLevel2ComLevel=$this->aPP['affthirdcomlevel'];
   #partner I poziomu partnera z cookie - prowizja wieczna
   $iCookieAffLevel2EternalComLevel=$this->aPP['affthirdcomlevel'];

   #partner I poziomu partnera przypisanego - prowizja bezposrednia
   $iInputableAffLevel2ComLevel=$this->aPP['affthirdcomlevel'];
   #partner I poziomu partnera przypisanego - prowizja wieczna
   $iInputableAffLevel2EternalComLevel=$this->aPP['affthirdcomlevel'];


   break;
  }
  ;

  /*
  print_pr ("partner [cookie] prowizja [bezpo¶rednia] = $iCookieAffComLevel<br />
  partner [cookie] prowizja [wieczna] = $iCookieAffEternalComLevel<br />
  partner [przypisany] prowizja [bezpo¶rednia] = $iInputableAffComLevel<br />
  partner [przypisany] prowizja [wieczna] = $iInputableAffEternalComLevel<br />
 ");
	*/
  #jeżeli brak partnera z cookie a jest przypisany
  if ($iCookieAff==0 and $iInputableAff==1) {
   #partner przypisany dostaje wieczna prowizje	- poziom 1
   $iRes=$this->dbInsertAffCommision($aInputableAff['id_affiliate'],$sTransId,$this->aPP['name'],0,$iInputableAffEternalComLevel, $sStatus);

   #prowizja producenta
   $iRes=$this->dbInsertProCommision($sTransId,$this->aPP['name'],$iProComLevel,$sStatus);

   # jeżeli partner przypisany ma partnera na wyzszym poziomie
   if ($iInputableAffLevel1>0) {
    # prowizja wieczna dla partnera z poziomu 1
    $iRes=$this->dbInsertAffCommision($iInputableAffLevel1,$sTransId,$this->aPP['name'],0,$iInputableAffLevel2EternalComLevel, $sStatus, 2);
   }
   if ($iInputableAffLevel2>0) {
    # prowizja wieczna dla partnera z poziomu 2
    $iRes=$this->dbInsertAffCommision($iInputableAffLevel2,$sTransId,$this->aPP['name'],0,$iInputableAffLevel2EternalComLevel, $sStatus, 3);
   }

   return 1;
  }

  #jeżeli partner cookie i partner przypisany
  if ($iCookieAff==1 and $iInputableAff==1) {
   #jeżeli identyfikator w cookie ten sam co przypisanego to
   #partner dostaje wieczna i bezposrednia prowizje
   #print_pr ("$sCookieAff==$sInputableAff");
   if ($sCookieAff==$sInputableAff) {
    $iRes=$this->dbInsertAffCommision($aCookieAff['id_affiliate'],$sTransId,$this->aPP['name'],$iCookieAffComLevel,$iInputableAffEternalComLevel, $sStatus);
    if ($iCookieAffLevel1>0) {
     $iRes=$this->dbInsertAffCommision($iCookieAffLevel1,$sTransId,$this->aPP['name'],$iCookieAffLevel1ComLevel,$iInputableAffLevel1EternalComLevel, $sStatus,2);
    }
    if ($iCookieAffLevel2>0) {
     $iRes=$this->dbInsertAffCommision($iCookieAffLevel2,$sTransId,$this->aPP['name'],$iCookieAffLevel2ComLevel,$iInputableAffLevel2EternalComLevel, $sStatus,3);
    }

    $iRes=$this->dbInsertProCommision($sTransId,$this->aPP['name'],$iProComLevel,$sStatus);
    return 1;
   }
   else {
    #partner cookie dostaje prowizje bezposrednia
    $iRes=$this->dbInsertAffCommision($aCookieAff['id_affiliate'],$sTransId,$this->aPP['name'],$iCookieAffComLevel,0,$sStatus);
    if ($iCookieAffLevel1>0) {
     $iRes=$this->dbInsertAffCommision($iCookieAffLevel1,$sTransId,$this->aPP['name'],$iCookieAffLevel1ComLevel,0,$sStatus,2);
    }
    if ($iCookieAffLevel2>0) {
     $iRes=$this->dbInsertAffCommision($iCookieAffLevel2,$sTransId,$this->aPP['name'],$iCookieAffLevel1ComLevel,0,$sStatus,3);
    }

    #print $iRes;
    # a partner przypisany prowizje wieczna
    $iRes=$this->dbInsertAffCommision($aInputableAff['id_affiliate'],$sTransId,$this->aPP['name'],0,$iInputableAffEternalComLevel,$sStatus);

    if ($iInputableAffLevel1>0) {
     $iRes=$this->dbInsertAffCommision($iInputableAffLevel1,$sTransId,$this->aPP['name'],0,$iInputableAffLevel1EternalComLevel,$sStatus,2);
    }
    if ($iInputableAffLevel2>0) {
     $iRes=$this->dbInsertAffCommision($iInputableAffLevel2,$sTransId,$this->aPP['name'],0,$iInputableAffLevel1EternalComLevel,$sStatus,3);
    }


    #prowizja producenta
    $iRes=$this->dbInsertProCommision($sTransId,$this->aPP['name'],$iProComLevel,$sStatus);
    return 1;
   }
  }


  #jeżeli wszystko OK. to wysylamy maila do partnera
  #if ($iRes) {
  #$iAffComLevel=$iAffComLevel/100;
  #mail($this->aAff['email'], "pp.netina.com.pl - Nowa prowizja na Twoim koncie","Wlasnie przed chwila zostala naliczona prowizja.\nTym razem jest to: $iAffComLevel zl.\nProwizja dotyczy produktu: [{$this->aPP['name']}]\nStatus prowizji: nowa");
  #}
  return 1;
 }

 /*
 Indywidualna prowizja partnera do produktu
 wej¶cie:	iIdAff - id partnera,
            iIdP - id produktu
 wyjscie:	-1 - error
 				0 - brak danych
 				tablica
 */

 function dbGetAffiliateProductCommision($iIdAff, $iIdP) {

  #print "$iIdAff, $iIdP";
  $sQ="
  SELECT *
  FROM pp_affiliates_com
  WHERE
   `id_affiliate`=?
	AND `id_product`=?
  ";

  $aRes=$this->oDb->getRow($sQ, array($iIdAff,$iIdP), DB_FETCHMODE_ASSOC);
  #print_pr ($oDb);
  if (PEAR::isError($aRes)) {

   return -1;
   #die($aRes->getMessage() . ' | ' . $aRes->getDebugInfo());
  }
  if (is_array($aRes) and count($aRes)>0) return $aRes;
  return 0;
 }

 function dbSetPPProduct($iPPProductId) {
  $sQ="
  SELECT
   *
  FROM
   `pp_products`
  WHERE
   `id_product`=?
  ";

  $aRes = $this->oDb->getRow($sQ,array($iPPProductId),DB_FETCHMODE_ASSOC);
  //print_pr($aRes);
  if (DB::isError($aRes)) {

   #trigger_error($aRes->getMessage(), E_USER_WARNING);
   return 0;
  }
  ;
  if (count($aRes)==0) return 0;
  $this->aPP=$aRes;
  $this->iPP=1;
  return 1;
 }


 /* Funkcja przypisujaca klienta do polecajacego w ramach danego produktu
 parametry:
 1. id produktu z systemu PP
 2. id partnera z systemu PP
 3. $sBuyerId - ID klienta
 return:
 1 : succes
 0 : error
 */

 function dbAddAffiliateToBuyer ($iProductId, $iAffiliateId, $sBuyerId) {

  if (empty($sBuyerId)) return 0;

  $sQ="
	INSERT INTO
	 pp_relations
	SET
	 id_product=?,
	 id_affiliate=?,
	 idClient_ext=?
  ";
  $oRes=$this->oDb->query($sQ, array($iProductId, $iAffiliateId, $sBuyerId));
  if (DB::isError($oRes)) {
   #trigger_error($oRes->getMessage(), E_USER_WARNING);
   return 0;
  }
  return 1;
 }


 /*
 Pobieramy dane partnera z bazy i podstawia pod tablice w obiekcie
 wejscie: 	string - nazwa partnera w systemie jest to "identity"
 wyjscie: 	0 - blad
 				1 - OK
 */

 function dbSetAffiliateFromIdentity($sAffIdentity) {

  $sQ="
  SELECT
   *
  FROM
   `pp_affiliates`
  WHERE
   `identity`=?
  ";

  $aRes = $this->oDb->getRow($sQ,array($sAffIdentity),DB_FETCHMODE_ASSOC);
  #print_pr($this->oDb);
  if (DB::isError($aRes)) {

   #trigger_error($aRes->getMessage(), E_USER_WARNING);
   return 0;
  }
  ;

  if (count($aRes)==0) {
   $this->iAff=0;
   return 0;
  }

  $this->aAff=$aRes;
  $this->iAff=1;
  return 1;
 }


 /*
 Zmiana statusu tranzakcji
 wejscie:
 1. id produktu
 2. id tranzakcji
 2. status (new, confirmed, canceled)
 wyjscie:
 1. -1 - blad
 2. 1 - OK.
 */

 function dbChangeCommisionStatus($iProductId, $sTransId, $status='confirmed') {
	$sQ="
   	SELECT status FROM `pp_commisions_aff` WHERE
	 `id_product`=?
	 AND `idTrans_ext`=?
  ";
	$aRes=$this->oDb->getRow($sQ, array($iProductId,$sTransId), DB_FETCHMODE_ASSOC);
	if (is_array($aRes) and ($aRes['status'] == $status)) {
		return 1;
	}

  $sQ="
   UPDATE
	 `pp_commisions_aff`
	SET
	 `status`=?
	 ,updateTime=NOW()
	WHERE
	 `id_product`=?
	 AND `idTrans_ext`=?
  ";

  $oRes=$this->oDb->query($sQ, array($status,$iProductId,$sTransId));

  $sQ="
   UPDATE
	 `pp_commisions_pro`
	SET
	 `status`=?
	 ,updateTime=NOW()
	WHERE
	 `id_product`=?
	 AND `idTrans_ext`=?

  ";

  $oRes=$this->oDb->query($sQ, array($status,$iProductId,$sTransId));

  if (DB::isError($oRes)) {
   #trigger_error($oRes->getMessage(), E_USER_WARNING);
   return -1;
  }

  include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."emailTpl.php";
  include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."emailTpl_DAO.php";

  $DAO_EmailTpl=new EmailTpl_DAO($this->oDb);
  $oEmailCommisionTpl=$DAO_EmailTpl->dbGetEmailTplByAction($status."Commision");

  #print_pr($oEmailCommisionTpl);
  #exit;

  if ($oEmailCommisionTpl->sending=='Y') {


   include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."affiliateCommision.php";
   include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."affiliateCommision_DAO.php";
   include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."affiliate.php";
   include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."affiliate_DAO.php";

   $DAO_AffComm=new AffiliateCommision_DAO($this->oDb); # fabryka prowizji partnerow
   $DAO_Affiliate=new Affiliate_DAO($this->oDb); #fabryka partnerow

   # pobierami wszystkie prowizje o danych parametrach
   $aAffComm=$DAO_AffComm->doGetCommisionByProductTrans($iProductId, $sTransId);
	if (is_null($aAffComm)) return null;

	#print_pr ($aAffComm);
   for ($i=0; $i<count($aAffComm); $i++) {

    $oAffiliate=$DAO_Affiliate->doGetAffiliateFromId($aAffComm[$i]->id_affiliate); #partner którego dotyczy prowizja

    #zamiana ciagow
    $aChangeFrom = array("[firstname]", "[commisionName]", "[commisionLevel]", "[directCommisionValue]",  "[eternalCommisionValue]");
    $aChangeTo   = array($oAffiliate->name, $aAffComm[$i]->name, $aAffComm[$i]->level, $aAffComm[$i]->directcom/100, $aAffComm[$i]->eternalcom/100);

	$subject = str_replace($aChangeFrom, $aChangeTo, $oEmailCommisionTpl->title);
    $textPlain = str_replace($aChangeFrom, $aChangeTo, $oEmailCommisionTpl->template);
	$iMail = sendMail_PP(array($oEmailCommisionTpl->fromemail=>$oEmailCommisionTpl->fromname), $oAffiliate->email, $subject, $textPlain);

    $sLogsStr=$_SERVER['REMOTE_ADDR']." | ".date("Y-m-d H:i")." | {$oAffiliate->email} | status:$iMail\n";
   }
  }
  else {
   $sLogsStr=$_SERVER['REMOTE_ADDR']." | ".date("Y-m-d H:i")." | status: not sent\n";
  }

  # log operacji do pliku
  $sLogsFile=$GLOBALS['NETIGO_PP']['CONFIG']['DOCUMENT_ROOT_PATH'].$this->logPath."commisionAffEmail.log";
  $oFile=fopen($sLogsFile,"a");
  fwrite($oFile,$sLogsStr);
  fclose($oFile);

//  if ($oEmailCommisionTpl->sending=='Y') {
//   include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."producerCommision.php";
//   include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."producerCommision_DAO.php";
//   include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."producer.php";
//   include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."producer_DAO.php";
//
//	$DAO_ProComm=new ProducerCommision_DAO($this->oDb); #print_pr ($DAO_ProComm);
//   $DAO_Producer=new Producer_DAO($this->oDb); #print_pr ($DAO_Producer);
//
//	$oProComm=$DAO_ProComm->doGetCommisionByProductTrans($iProductId, $sTransId); #prowizja producenta
//	#print_pr ($oProComm);
//
//   $oProducer=$DAO_Producer->doGetProducerFromId($oProComm->id_producer); #producent którego dotyczy prowizja
//
//   #zamiana ciagow	na
//   $aChangeFrom = array("[firstname]", "[commisionName]", "[commisionLevel]", "[directCommisionValue]",  "[eternalCommisionValue]");
//   $aChangeTo   = array($oProducer->producername, $oProComm->name, 0, $oProComm->commision/100, 0);
//   $subject = str_replace($aChangeFrom, $aChangeTo, $oEmailCommisionTpl->title);
//   $body = str_replace($aChangeFrom, $aChangeTo, $oEmailCommisionTpl->template);
//   $to = $oProducer->email;
//	sendMail_PP($to, $to, $subject, $body, null);
//
//   $sLogsStr=$_SERVER['REMOTE_ADDR']." | ".date("Y-m-d H:i")." | {$oProducer->email} | status:$iMail\n";
//  }
//  else {
//   $sLogsStr=$_SERVER['REMOTE_ADDR']." | ".date("Y-m-d H:i")." | {$oProducer->email} | status: not sent\n";
//  }

  # log operacji do pliku
  $sLogsFile=$GLOBALS['NETIGO_PP']['CONFIG']['DOCUMENT_ROOT_PATH'].$this->logPath."commisionProEmail.log";
  $oFile=fopen($sLogsFile,"a");
  fwrite($oFile,$sLogsStr);
  fclose($oFile);

  return 1;
 }

 /*
 Pobranie statusu tranzakcji
 wejscie:
 1. type = rodzaj tabeli {'aff', 'pro'} domy¶lnie 'aff'
 2. id produktu
 3. id tranzakcji
 wyjscie:
 1. 0 - brak danych
 lub
 2. status: 'new', 'confirmed', 'canceled'
 */

 function dbGetCommisionStatus($type='aff', $iProductId, $sTransId) {

  $sQ="
   SELECT
	 `status`
	FROM
	 `pp_commisions_$type`
	WHERE
	 `id_product`=?
	 AND `idTrans_ext`=?
	LIMIT 0,1
  ";

  $aRes=$this->oDb->getRow($sQ, array($iProductId,$sTransId), DB_FETCHMODE_ASSOC);
  #print_r ($aRes);
  if (DB::isError($aRes)) {
   #trigger_error($oRes->getMessage(), E_USER_WARNING);
   return 0;
  }
  if (is_array($aRes)) return $aRes['status'];
  return null;
 }


 /*
 Tworzenie wpisu prowizji dla partnera
 wejscie:
  * $iAffId - ID partnera z systemu PP
  * $sTransId - ID tranzakcji z systemu Tranzakcji
  * $iDirectCom - wartosc prowizji partnera
  * $iEternalCom - wartosc prowizji producenta
  * $iStatus - status "new", "confirmed"
 */

 function dbInsertAffCommision($iAffId,$sTransId,$sName,$iDirectCom,$iEternalCom, $sStatus="new", $iLevel=1) {

 $aStatusAll=array("new","confirmed","canceled");
  $aStatusNotNew=array("confirmed","canceled");

  $qStatus="new";
  if (in_array($sStatus,$aStatusAll)) $qStatus=$sStatus;

  $qupdateTime="";
  if (in_array($sStatus,$aStatusNotNew)) $qupdateTime=",updateTime=NOW()";

  $qLevel= intval($iLevel);

  $sQ="
  INSERT INTO
   `pp_commisions_aff`
  SET
   `id_product`=?
	,`id_affiliate`=?
	,`idTrans_ext`=?
	,`name`=?
	,`directcom`=?
	,`eternalcom`=?
	,createtime=NOW()
	$qupdateTime
	,level=?
  	,status=?
  ";

  $oRes=$this->oDb->query($sQ, array($this->aPP['id_product'], $iAffId, $sTransId, $sName, $iDirectCom,$iEternalCom,$qLevel,$qStatus));


  # wczytujemy klase obslugujaca szablony email
  include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."emailTpl.php";
  include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."emailTpl_DAO.php";

  $DAO_EmailTpl=new EmailTpl_DAO($this->oDb); #print_r ($DAO_EmailTpl);
  $oEmailCommisionTpl=$DAO_EmailTpl->dbGetEmailTplByAction($sStatus."Commision");
  #print_pr ($oEmailCommisionTpl)	;

  include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."affiliate.php";
  include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."affiliate_DAO.php";
  $DAO_Affiliate=new Affiliate_DAO($this->oDb);
  $oPartner=$DAO_Affiliate->doGetAffiliateFromId($iAffId);
  if ($oEmailCommisionTpl->sending=='Y') {



   #zamiana ciagow
   $aChangeFrom = array("[firstname]", "[commisionName]", "[commisionLevel]", "[directCommisionValue]",  "[eternalCommisionValue]");
   $aChangeTo   = array($oPartner->name, $sName, $iLevel, $iDirectCom/100, $iEternalCom/100);
   $oEmailCommisionTpl->template = str_replace($aChangeFrom, $aChangeTo, $oEmailCommisionTpl->template);
   $oEmailCommisionTpl->title=str_replace($aChangeFrom, $aChangeTo, $oEmailCommisionTpl->title);


	$from = array($oEmailCommisionTpl->fromemail => $oEmailCommisionTpl->fromname);
	$to = $oPartner->email;
	$subject = $oEmailCommisionTpl->title;
	$textPlain = $oEmailCommisionTpl->template;

	$iMail = sendMail_PP($from, $to, $subject, $textPlain);

   $sLogsStr=$_SERVER['REMOTE_ADDR']." | ".date("Y-m-d H:i")." | {$oPartner->email} | status:$iMail\n";
  }
  else {
   $sLogsStr=$_SERVER['REMOTE_ADDR']." | ".date("Y-m-d H:i")." | {$oPartner->email} | status: not sent\n";
  }

  # log operacji do pliku
  $sLogsFile=$GLOBALS['NETIGO_PP']['CONFIG']['DOCUMENT_ROOT_PATH'].$this->logPath."commisionAffEmail.log";
  $oFile=fopen($sLogsFile,"a");
  fwrite($oFile,$sLogsStr);
  fclose($oFile);


  if (DB::isError($oRes)) {
   #trigger_error($oRes->getMessage(), E_USER_WARNING);
   return -1;
  }
  return 1;
 }

 /*
 Tworzenie wpisu prowizji dla producenta
 wejscie:
  * $iAffId - ID partnera z systemu PP
  * $sTransId - ID tranzakcji z systemu Tranzakcji
  * $iDirectCom - wartosc prowizji partnera
  * $iStatus - status "new", "confirmed"
 */

 function dbInsertProCommision($sTransId,$sName,$iDirectCom,$sStatus="new") {
  if ($this->addProducerCommision==0) return;

  $aStatusAll=array("new","confirmed","canceled");
  $aStatusNotNew=array("confirmed","canceled");

  $qStatus="new";
  if (in_array($sStatus,$aStatusAll)) $qStatus=$sStatus;

  $qupdateTime="";
  if (in_array($sStatus,$aStatusNotNew)) $qupdateTime=",updateTime=NOW()";

  $qLevel=1;

  $sQ="
  INSERT INTO
   `pp_commisions_pro`
  SET
   `id_producer`=?
	,`id_product`=?
	,`idTrans_ext`=?
	,`name`=?
	,`commision`=?
	,createtime=NOW()
	$qupdateTime
	,level=?
  	,status=?
  ";

  $oRes=$this->oDb->query($sQ, array($this->aPP['id_producer'],$this->aPP['id_product'], $sTransId, $sName, $iDirectCom, $qLevel,$qStatus));
  if (DB::isError($oRes)) {
   #trigger_error($oRes->getMessage(), E_USER_WARNING);
   return -1;
  }

  # wczytujemy klase obslugujaca szablony email
  include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."emailTpl.php";
  include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."emailTpl_DAO.php";

  $DAO_EmailTpl=new EmailTpl_DAO($this->oDb); #print_r ($DAO_EmailTpl);
  $oEmailCommisionTpl=$DAO_EmailTpl->dbGetEmailTplByAction($sStatus."Commision");
  #print_pr ($oEmailCommisionTpl)	;

  if ($oEmailCommisionTpl->sending=='Y') {


   #include "producerCommision.php";
   #include "producerCommision_DAO.php";

   #$DAO_ProComm=new ProducerCommision_DAO($this->oDb);
   #$oProComm=$DAO_ProComm->doGetCommisionByProductTrans($this->aPP['id_product'], $sTransId); #prowizja producenta

   include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."producer.php";
   include_once $GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH']."producer_DAO.php";

   $DAO_Producer=new Producer_DAO($this->oDb);
   $oProducer=$DAO_Producer->doGetProducerFromId($this->aPP['id_producer']); #producent którego dotyczy prowizja

   #zamiana ciagow
   $aChangeFrom = array("[firstname]", "[commisionName]", "[commisionLevel]", "[directCommisionValue]",  "[eternalCommisionValue]");
   $aChangeTo   = array($oProducer->producername, $sName, 1, $iDirectCom/100, 0);


	$subject = str_replace($aChangeFrom, $aChangeTo, $oEmailCommisionTpl->title);
    $textPlain = str_replace($aChangeFrom, $aChangeTo, $oEmailCommisionTpl->template);
	$iMail = sendMail_PP(array($oEmailCommisionTpl->fromemail=>$oEmailCommisionTpl->fromname), $oProducer->email, $subject, $textPlain);

   $sLogsStr=$_SERVER['REMOTE_ADDR']." | ".date("Y-m-d H:i")." | {$oProducer->email} | status:$iMail\n";
  }
  else {
   $sLogsStr=$_SERVER['REMOTE_ADDR']." | ".date("Y-m-d H:i")." | {$oProducer->email} | status: not sent\n";
  }

  # log operacji do pliku
  $sLogsFile=$GLOBALS['NETIGO_PP']['CONFIG']['DOCUMENT_ROOT_PATH'].$this->logPath."commisionProEmail.log";
  $oFile=fopen($sLogsFile,"a");
  fwrite($oFile,$sLogsStr);
  fclose($oFile);

  return 1;
 }


 /*
 Opis: zwraca tablice istniejacych identyfikatorow polecajacych
 przypisanych do kupujacego (prowizja wieczna)
 input: int $buyerId - systemowe id kupuj±cego
 output: 0 - blad lub brak danych
         identyfikatory polecajacych
 */

 function dbGetAffiliatesIdentityFromBuyer($iProductId, $sBuyerId) {

  $sQ = "
  SELECT
   a.identity as identity
  FROM
   pp_relations r,
	pp_affiliates a
  WHERE
	r.idClient_ext like ?
	AND r.id_product=?
	AND r.id_affiliate=a.id_affiliate
  ";
  $aRes = $this->oDb->getAll($sQ,array($sBuyerId,$iProductId),DB_FETCHMODE_ASSOC);
  #print_pr($this->oDb);
  if (DB::isError($aRes)) {
   #trigger_error($aRes->getMessage(), E_USER_WARNING);
   return -1;
  }
  ;
  if (!is_array($aRes)) return 0;
  if (is_array($aRes) and count($aRes)==0) return 0;
  else return $aRes;
 }

 /*
 Opis: zwraca tablice istniejacych identyfikatorow polecajacych
 przypisanych do kupujacego (prowizja wieczna)
 input: id produktu, identyfikator parnera, czy_unikalny
 output: 0 - brak danych
         1 - ok.
			-1 - blad sql
 */

 function dbInsertHitsStat($iProductId,$sPartnerIdentity,$sUni="false") {

  return $this->dbInsertStat("hits",$iProductId,$sPartnerIdentity,$sUni);
 }

 function dbInsertFormsStat($iProductId,$sPartnerIdentity,$sUni="false") {

  return $this->dbInsertStat("forms",$iProductId,$sPartnerIdentity,$sUni);
 }

 function dbInsertVisitsStat($iProductId,$sPartnerIdentity,$sUni="false") {

  return $this->dbInsertStat("visits",$iProductId,$sPartnerIdentity,$sUni);
 }

 function dbInsertStat($sType,$iProductId,$sPartnerIdentity,$sUni="false") {

  if (is_numeric($sPartnerIdentity) and (int)$sPartnerIdentity==0) $sPartnerIdentity=$this->defaultAffiliate;

  $this->dbSetAffiliateFromIdentity($sPartnerIdentity);
  $this->dbSetPPProduct($iProductId);

  if ($this->iAff==0 or $this->iPP==0) return 0;

  $sData=date("Y-m-d");
  if ($sUni=="true") {
   #print "true";
   $sQ="
	INSERT INTO
	 pp_stats
	SET
	 id_affiliate=?
	 ,id_product=?
	 ,data=?
	 ,{$sType}_uni=1
   ON DUPLICATE KEY UPDATE
	 {$sType}_uni={$sType}_uni+1;
	";
   $aRes = $this->oDb->query($sQ,array($this->aAff['id_affiliate'],$this->aPP['id_product'],$sData),DB_FETCHMODE_ASSOC);
   if (DB::isError($aRes)) {
    #trigger_error($aRes->getMessage(), E_USER_WARNING);
    return -1;
   }
   ;
  }

  $sQ = "
  INSERT INTO
   pp_stats
  SET
   id_affiliate=?
	,id_product=?
	,data=?
	,{$sType}_all=1
  ON DUPLICATE KEY UPDATE
   {$sType}_all={$sType}_all+1;
  ";
  #print $sQ;
  $aRes = $this->oDb->query($sQ,array($this->aAff['id_affiliate'],$this->aPP['id_product'],$sData),DB_FETCHMODE_ASSOC); #print_pr($this->oDb);
  //print_pr($aRes);
  if (DB::isError($aRes)) {
   #trigger_error($aRes->getMessage(), E_USER_WARNING);
   return -1;
  }
  ;
  return 1;
 }

 /*
 Opis: zwraca tablice zawierajaca identyfikatory do zewnetrznych PP
 input: int $buyerId - systemowe id kupuj±cego
 output: 0 - blad lub brak danych
         identyfikatory polecajacych
 */

 function dbListPPLinks($sIdentity="") {

  if (trim($sIdentity)<>"") {
   $aAffTemp=$this->aAff;
   $iAffTemp=$this->iAff;
   $this->dbSetAffiliateFromIdentity($sIdentity);
   if ($this->iAff==0) $sIdentity="";
   else $iAffId=$this->aAff['id_affiliate'];
   $this->aAff=$aAffTemp;
   $this->iAff=$iAffTemp;
  }

  if (trim($sIdentity)=="") {
   $sQ = "
   SELECT pp.*,REPLACE(pp.url,'[id]',pp.identity) as ppurl
   FROM pp_pplinks pp
   ";
  }
  else {
   $sQ = "
   SELECT pp.sysname, REPLACE(pp.url,'[id]',ppa.identity) as affppurl, pp.url, ppa.*
   FROM pp_affiliates_pplinks ppa, pp_pplinks pp
   WHERE
    id_affiliate='$iAffId'
	 AND pp.id_pplink=ppa.id_pplink
   ";
  }

  $aRes = $this->oDb->getAll($sQ,array(),DB_FETCHMODE_ASSOC);
  #print_pr($this->oDb);
  if (DB::isError($aRes)) {
   #trigger_error($aRes->getMessage(), E_USER_WARNING);
   return -1;
  }
  ;
  if (!is_array($aRes)) return 0;
  if (is_array($aRes) and count($aRes)==0) return 0;
  return $aRes;
 }

 /*
 Opis: zwraca tablice zawierajaca identyfikatory do zewnetrznych PP
 input: int $buyerId - systemowe id kupuj±cego
 output: 0 - blad lub brak danych
         identyfikatory polecajacych
 */

 function dbListPPLinksIdent($sIdentity="") {

  if (trim($sIdentity)<>"") {
   $aAffTemp=$this->aAff;
   $iAffTemp=$this->iAff;
   $this->dbSetAffiliateFromIdentity($sIdentity);
   if ($this->iAff==0) $sIdentity="";
   else $iAffId=$this->aAff['id_affiliate'];
   $this->aAff=$aAffTemp;
   $this->iAff=$iAffTemp;
  }

  if (trim($sIdentity)=="") {
   $sQ = "
   SELECT pp.*,REPLACE(pp.url,'[id]',pp.identity) as ppurl
   FROM pp_pplinks pp
   ";
  }
  else {
   $sQ = "
   SELECT pp.sysname, REPLACE(pp.url,'[id]',ppa.identity) as ppurl, pp.url, ppa.*
   FROM pp_affiliates_pplinks ppa, pp_pplinks pp
   WHERE
    id_affiliate='$iAffId'
	 AND pp.id_pplink=ppa.id_pplink
   ";
  }

  $aRes = $this->oDb->getAll($sQ,array(),DB_FETCHMODE_ASSOC);
  #print_pr($this->oDb);
  if (DB::isError($aRes)) {
   #trigger_error($aRes->getMessage(), E_USER_WARNING);
   return -1;
  }
  ;

  if (!is_array($aRes) or (is_array($aRes) and count($aRes)==0)) return 0;

  $aPPLinks=array();
  for ($iI=0; $iI<count($aRes); $iI++) {
   $sIdPPLink=$aRes[$iI]['sysname']; #identyfikator link programu PP
   $aPPLinks[$sIdPPLink]=array();
   $aPPLinks[$sIdPPLink]['idurl']=$aRes[$iI]['url'];
   $aPPLinks[$sIdPPLink]['identity']=$aRes[$iI]['identity'];
   $aPPLinks[$sIdPPLink]['ppurl']=$aRes[$iI]['ppurl'];
  }

  return $aPPLinks;
 }


 /*
 Funkcja zwraca liste partnerów II poziomu
 wejscie: ID partnera poziomu I
 wyj¶cie: -1 error
 			 0 brak
			 tablica partnerów II poziomu
 */

 function dbGetAffLevel2($iIdLevel1) {

  $sQ="
  SELECT
   pa.*
  FROM
   pp_affiliates_levels pal
   ,pp_affiliates pa
  WHERE
   pal.id_affiliate_level2=pa.id_affiliate
   AND pal.id_affiliate_level1=?
  ";

  $aRes = $this->oDb->getAll($sQ,array($iIdLevel1),DB_FETCHMODE_ASSOC);
  #print_pr($this->oDb);
  if (DB::isError($aRes)) {
   #trigger_error($aRes->getMessage(), E_USER_WARNING);
   return -1;
  }
  ;
  if (!is_array($aRes)) return 0;
  if (is_array($aRes) and count($aRes)==0) return 0;
  return $aRes;
 }

 /*
 Funkcja zwraca ID partnera I poziomu
 wejscie: ID partnera poziomu II
 wyj¶cie: -1 error
 			 0 brak
			 id partnera I poziomu
 */

 function dbGetAffIdLevel1($iIdLevel2) {

  $sQ="
  SELECT
   id_affiliate_level1
  FROM
   pp_affiliates_levels
  WHERE
   id_affiliate_level2=?
  ";

  $aRes = $this->oDb->getRow($sQ,array($iIdLevel2),DB_FETCHMODE_ASSOC);
  #print_pr($this->oDb);
  if (DB::isError($aRes)) {
   #trigger_error($aRes->getMessage(), E_USER_WARNING);
   return -1;
  }
  ;
  if (!is_array($aRes)) return 0;
  if (is_array($aRes) and count($aRes)==0) return 0;
  return $aRes['id_affiliate_level1'];
 }


   function dbCheckAddPartnerToBuyer($idProduct, $partner, $sBuyerId) {

  # pobieramy przypisanych partnerow
  $aBuyerAffIdentity=$this->dbGetAffiliatesIdentityFromBuyer($idProduct,$sBuyerId);
  #print_r ($aBuyerAffIdentity);

  # jeżeli wynik nie jest tablica tablica - exit
  if (is_array($aBuyerAffIdentity) and count($aBuyerAffIdentity)>0) return 0;

  # ustawiamy partnera z cookie
  $this->dbSetAffiliateFromIdentity($partner);

  # jeżeli nie ma takiego partnera to exit
  if (!$this->iAff) return 0;

  # jest partner cookie to dodajemy partnera do klienta
  if ($this->iAff==1) {
   #przypisujemy partnera do klienta
   $iRes=$this->dbAddAffiliateToBuyer($idProduct, $this->aAff['id_affiliate'],$sBuyerId);
  }

 }

}

?>