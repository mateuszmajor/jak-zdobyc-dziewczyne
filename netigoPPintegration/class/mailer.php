<?php

function sendMail_PP($from, $to, $subject, $textPlain = null, $textHTML = null, $charset = 'UTF-8') {
	if (!$GLOBALS['NETIGO_PP']['CONFIG']['MAIL']['enabled']) {
		return true;
	}

	require_once($GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH'].'xpm/MAIL4.php');

	// set time limit
	$timelimit = ini_get('max_execution_time');
	if ($timelimit != 0) {
		set_time_limit($timelimit + 120);
	}

	$fEmail = $from;
	$fName = null;
	if (is_array($from)) {
		$fName = reset($from);
		$fEmail = key($from);
		$from = ('"'.htmlspecialchars($fName).'" <'.htmlspecialchars($fEmail).'>');
	}

	$m = new MAIL4();

	if (!is_array($to)) {
		$m->AddTo($to);
	} else {
		$_to = $to;
		foreach ($_to as $k => $v) {
			if (is_numeric($k)) {
				echo $v;echo'-';
				$m->AddTo($v);
			} else {
				$m->AddTo($k, $v);
			}
		}
	}

	$m->From($fEmail, $fName);
	$m->Subject($subject, $charset, 'base64');

	if ($textPlain !== null) {
		$m->Text($textPlain, $charset, 'base64');
	}
	if ($textHTML !== null) {
		$m->Html($textHTML, $charset, 'base64');
	}

	if ($GLOBALS['NETIGO_PP']['CONFIG']['MAIL']['mail_function'] == 'smtp') {
		$c = $m->Connect($GLOBALS['NETIGO_PP']['CONFIG']['MAIL']['SMTP']['host'], $GLOBALS['NETIGO_PP']['CONFIG']['MAIL']['SMTP']['port'], $GLOBALS['NETIGO_PP']['CONFIG']['MAIL']['SMTP']['login'], $GLOBALS['NETIGO_PP']['CONFIG']['MAIL']['SMTP']['password']);
		if ($c == false) {
			trigger_error('SMTP error: ' . print_r($m->Result, true));
			return false;
		}
		$result = $m->Send($c);
		if (!$result) {
			trigger_error('Could not send by smtp.');
			return false;
		}
	} else
	if ($GLOBALS['NETIGO_PP']['CONFIG']['MAIL']['mail_function'] == 'mail') {
		$result = $m->Send('local');
		if (!$result) {
			trigger_error('Could not send by mail() function.');
			return false;
		}
	} else
	if ($GLOBALS['NETIGO_PP']['CONFIG']['MAIL']['mail_function'] == 'file') {
		dump_log('Mail::send(): '."\r\n".$m->Send('output'));
	}
	return true;
}

?>
