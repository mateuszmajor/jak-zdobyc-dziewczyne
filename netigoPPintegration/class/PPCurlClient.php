<?php
class PPCurlClient {

 public  $config=array();
 public  $curlStatus;
 public  $sessionErrMsg;

 function __construct($print=false) {
  $this->config['NETINA_PP_SERVER']=$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_SERVER'];
  $this->config['NETINA_PP_PID']=$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_PID'];
  $this->config['NETINA_PP_LOGIN']=$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_LOGIN'];
  $this->config['NETINA_PP_HASH']=$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_HASH'];
  $this->config['NETINA_PP_INFOPRINT']=$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_INFOPRINT'];
  $this->config['NETINA_PP_RESULTCHARSET']=$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_RESULTCHARSET'];

  $this->curlStatus=true;
  $this->sessionErrMsg="";

  if (!function_exists('curl_init')) {
	$this->curlStatus=false;
	$this->sessionErrMsg.="error|no-curl_install <br />";
  }
  # jezeli nie udalo sie polaczyc z serwerem to komunikat
  if (!defined('NETINA_PP_SERVER') and $this->curlTest()==false) {
   $this->curlStatus=false;
	$this->sessionErrMsg.="error|wrong/no-curl_url <br />";
  }
  if ($this->config['NETINA_PP_INFOPRINT'] and $this->curlStatus==false) echo $this->sessionErrMsg;
 }

 function netina_curl($function, $vars) {
  if ($this->curlStatus==false) return;
  $aParam=array();
  $aParam[0] = "login=".$this->config['NETINA_PP_LOGIN'];
  $aParam[1] = "hash=".$this->config['NETINA_PP_HASH'];
  $aParam[2] = "charset=".$this->config['NETINA_PP_RESULTCHARSET'];
  $aParam[3] = "PID=".$this->config['NETINA_PP_PID'];
  $aParam[4] = "function=".$function;

  #print_r ($aParam);
  $variables="";

  $sParam=implode("&", $aParam);
  $sParam.="&".$vars; # print $sParam;

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $this->config['NETINA_PP_SERVER']);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_TIMEOUT, 5);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $sParam);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $sRes = curl_exec($ch);
	//var_dump($aRes);
  curl_close($ch);
  if ($this->config['NETINA_PP_INFOPRINT']==true) print $function."/".$vars."/".$sRes."<br />";
  return trim($sRes);
 }

 function curlTest() {
  $res=$this->netina_curl("curlTest","");
  if (strpos($res,'404')) return false;//print $res;
  return true;
 }

 /*
 Funkcja zarzadzajaca calym procesem tworzenia prowizji
 parametry:
 1. identyfikator polecajacego
 2. id klienta
 3. unikalny numer tranzakcji dla produktu
 4. wartosc tranzakcji w groszach
 5. status
6. uwagi
7. kampania
 */

 function curlAddAffProFee($p1, $p2, $p3, $p4, $p5="new", $p6="",$p7="") {
  $res=$this->netina_curl("curlAddAffProFee","p1=$p1&p2=$p2&p3=$p3&p4=$p4&p5=$p5&p6=$p6&p7=$p7");
 }

 /* zmiana statusu tranzakcji (domyslnie na confirmed)
wejscie:
 p1=id tranzakcji,
 p2=status [new,confirmed,canceled]
*/

 function curlChangeCommisionStatus($p1, $p2='confirmed') {
  $res=$this->netina_curl("curlChangeCommisionStatus","p1=$p1&p2=$p2");
 }


 /*
 Funkcja sprawdzajac status tranzakcji
 zwraca: new, confirmed lub canceled
 gdy brak tranzakcji zwraca: error|no
 @param: $p1 - identyfikator platnosci

 */

 function curlGetCommisionStatus($p1) {
  return $this->netina_curl("curlGetCommisionStatus","p1=$p1");
 }

 /*
Funkcje przekazujace dane do naliczania statystyk;
wejscie:
p1=identyfikator parnera
p2=czy_unikalny [false,true]
wyjscie:
'OK' - naliczenie statystyki
'error:no-partner/product' - gdy brak produktu lub partnera
'error:sql' - gdy blad sql
*/

 function curlInsertHitsStat ($p1, $p2="false",$p3="") {
  $res=$this->netina_curl("curlInsertHitsStat","p1=$p1&p2=$p2&p3=$p3");
 }

 function curlInsertFormsStat ($p1, $p2="false",$p3="") {
  $res=$this->netina_curl("curlInsertFormsStat","p1=$p1&p2=$p2&p3=$p3");
 }

 function curlInsertVisitsStat ($p1, $p2="false",$p3="") {

  $res=$this->netina_curl("curlInsertVisitsStat","p1=$p1&p2=$p2&p3=$p3");
	
 }
 
function curlAddPartnerToBuyer ($p1, $p2) {
  $res=$this->netina_curl("curlAddPartnerToBuyer","p1=$p1&p2=$p2");echo $res;
 }
}

?>