<?php
class Affiliate_DAO {
 
 var $oDB;
 
 function Affiliate_DAO ($db=null) {
  $this->oDB=$db;
 }
 
 function doGetAffiliateFromId($id) {
  $sQ="
   SELECT *
	FROM pp_affiliates
	WHERE id_affiliate=?	
  ";
  
  $aRes=$this->oDB->getRow($sQ, array((int)$id), DB_FETCHMODE_ASSOC);
  
  if (PEAR::isError($aRes)) {
   
   return null;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  if (is_array($aRes) and count($aRes)>1) {
   return $this->doReturnAffiliateFromArray($aRes);
  }
  return null;
 }

 function doGetByHash($hash) {
  $sQ="
   SELECT *
	FROM pp_affiliates
	WHERE hash=?	
  ";
  
  $aRes=$this->oDB->getRow($sQ, array($hash), DB_FETCHMODE_ASSOC);
  
  if (PEAR::isError($aRes)) {
   return null;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  if (is_array($aRes) and count($aRes)>1) {
   return $this->doReturnAffiliateFromArray($aRes);
  }
  return null;
 }

 
 function doReturnAffiliateFromArray($aAff) {
  
  return new Affiliate(
  $aAff['id_affiliate'],
  $aAff['identity'],
  $aAff['pass'],
  $aAff['email'],
  $aAff['hash'],
  $aAff['joinTime'],
  $aAff['name'],
  $aAff['surname'],
  $aAff['companyName'],
  $aAff['street'],
  $aAff['postalCode'],
  $aAff['city'],
  $aAff['country'],
  $aAff['vat'],
  $aAff['nip'],
  $aAff['pesel'],
  $aAff['telephone'],
  $aAff['fax'],		
  $aAff['bankName'],
  $aAff['bankCode'],
  $aAff['bankSwiftCode'],
  $aAff['accountNumber'],
  $aAff['revenueName'],
  $aAff['revenueAddress'],
  $aAff['agreement'],
  $aAff['urls'],
  $aAff['newsletter'],
  $aAff['status']
  );
 }
 
 /*
Funkcja sprawdza czy identyfikator partnera jest zajęty
 @param: string identity
Funkcja zwraca:
 null gdy błąd
 1 gdy identyfikator jest wolny
 0 gdy jest zajęty
*/

 function doIsIdentityFree($sIdentity) {
  
  $sQ="
 SELECT *
 FROM pp_affiliates
 WHERE
  identity=?
 ";
  
  $aRes=$this->oDB->query($sQ, array($sIdentity), DB_FETCHMODE_ASSOC);
  
  if ((is_array($aRes) and count($aRes)==0) or PEAR::isError($aRes)) {
   
   return null;
   //die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  if ($aRes->numRows()>0) return 0;
  return 1;
 }
 
 
 /*
Funkcja sprawdza czy email podany przez partnera jest w uzyciu
@param: string email
Funkcja zwraca:
 null gdy błąd
 0 gdy email jest zajęty
 1 gdy jest wolny
*/

 function doIsEmailFree($sEmail) {
  
  $sQ="
 SELECT *
 FROM pp_affiliates
 WHERE
  email=?
 ";
  
  $aRes=$this->oDB->query($sQ, array($sEmail), DB_FETCHMODE_ASSOC);
  
  if ((is_array($aRes) and count($aRes)==0) or PEAR::isError($aRes)) {
   
   return null;
   //die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  if ($aRes->numRows()>0) return 0;
  return 1;
 }
 
 /*
Funkcja uaktualnia dane partnera na bazie
wejscie: tablica z danymi i parametry sterujace zmiana identyfikatora i hasla
wyjscie: -1 gdy blad
1 gdy wszystko OK
*/
function doSave($oAff) {

 $aQ=array(
 $oAff->identity,
 $oAff->pass,
 $oAff->email,
 $oAff->hash,
 $oAff->name,
 $oAff->surname,
 $oAff->companyName,
 $oAff->street,
 $oAff->postalCode,
 $oAff->city,
 $oAff->country,
 $oAff->vat,
 $oAff->nip,
 $oAff->pesel,
 $oAff->telephone,
 $oAff->fax,
 $oAff->bankName,
 $oAff->bankCode,
 $oAff->bankSwiftCode,
 $oAff->accountNumber,
 $oAff->revenueName,
 $oAff->revenueAddress,
 $oAff->agreement,
 $oAff->urls,
 $oAff->newsletter,
 $oAff->status,
 $oAff->id
 );
 
 
 $sQ="
 UPDATE pp_affiliates
 SET
 identity=?,
 pass=?,
 email=?,
 hash=?,
 name=?,
 surname=?,
 companyName=?,
 street=?,
 postalCode=?,
 city=?,
 country=?,
 vat=?,
 nip=?,
 pesel=?,
 telephone=?,
 fax=?,
 bankName=?,
 bankCode=?,
 bankSwiftCode=?,
 accountNumber=?,
 revenueName=?,
 revenueAddress=?,
 agreement=?,
 urls=?,
 newsletter=?,
 status=?
 WHERE
  `id_affiliate`=?
 ";
 $oRes=$this->oDB->query($sQ, $aQ);
 
 #print_pr( $oRes);
 
 if (PEAR::isError($oRes)) {
  #print ($oRes->getDebugInfo());
  return -1;
 }
 return 1; 
}
 
  /*
Funkcja dodaje partnera
@param: Affiliate Object 
wyjscie: -1 gdy blad
1 gdy wszystko OK
*/
function doAdd($oAff) {

 $aQ=array(
 $oAff->identity,
 $oAff->pass,
 $oAff->email,
 $oAff->hash,
 $oAff->name,
 $oAff->newsletter
 );
 
 $sQ="
 INSERT INTO pp_affiliates
 SET
 identity=?,
 pass=?,
 email=?,
 hash=?,
 name=?,
 newsletter=?
 ";
 $oRes=$this->oDB->query($sQ, $aQ);
 
 #print_pr( $oRes);
 
 if (PEAR::isError($oRes)) {
  #print ($oRes->getDebugInfo());
  return null;
 }
 
 $sQ="
 SELECT max(id_affiliate) as id
 FROM pp_affiliates
 ";
 $iRes=$this->oDB->getRow($sQ,array(), DB_FETCHMODE_ASSOC);
 #print_pr ($iRes);
 return $iRes['id']; 
 
 return 1; 
}
 
 /*
Funkcja aktywuje partnera
*/
function doActivateByHash($sHash) {
 
 $sQ="
 UPDATE pp_affiliates
 SET
  status='active'
 WHERE
  hash=?
  AND status='new'	
 ";
 $oRes=$this->oDB->query($sQ,array($sHash));
 if (PEAR::isError($oRes)) {
  return 0;
 }
 return $this->oDB->affectedRows();
}
 
}

?>