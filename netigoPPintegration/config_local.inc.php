<?php

$GLOBALS['NETIGO_PP']['CONFIG']['disableStats'] = false;
$GLOBALS['NETIGO_PP']['CONFIG']['disableRedirect'] = false;

$GLOBALS['NETIGO_PP']['CONFIG']['redirectTargetDomain'] = 'http://netigo.pl/'; // glowna domena na ktora ida przekierowania linkow partnerskich z innych domen
$GLOBALS['NETIGO_PP']['CONFIG']['DOCUMENT_ROOT_URL'] = 'http://www.jak-zdobyc-dziewczyne.pl/netigoPPintegration/';
$GLOBALS['NETIGO_PP']['CONFIG']['COOKIE_DOMAIN'] = '.jak-zdobyc-dziewczyne.pl';

$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_CURL'] = true;
$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_SERVER'] = 'http://pp.netigo.pl/curl-server.php';
$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_PID'] = '35';
$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_LOGIN'] = 'p.grzywocz@gmail.com';
$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_HASH'] = 'c182486136699ca13dce1e0f578e5881';
$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_INFOPRINT'] = false;
$GLOBALS['NETIGO_PP']['CONFIG']['NETINA_PP_RESULTCHARSET'] = 'latin2';

// ponizsze parametry nie powinny wymagac zmiany

$GLOBALS['NETIGO_PP']['CONFIG']['DOCUMENT_ROOT_PATH'] = realpath(realpath(dirname(__FILE__))).DIRECTORY_SEPARATOR;
if (defined('NETIGO_PP_MAIN')) {
	$GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH'] = realpath(realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR . '..').DIRECTORY_SEPARATOR . 'class/'; // korzystaj z bibliotek PP jesli jestes includowany z PP
} else {
	$GLOBALS['NETIGO_PP']['CONFIG']['CLASS_PATH'] = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR . 'class/';
}

$GLOBALS['NETIGO_PP']['CONFIG']['COOKIE_LIVE_TIME'] = 60*60*24*90;

$GLOBALS['NETIGO_PP']['CONFIG']['defaultPartnerName'] = 'system';
$GLOBALS['NETIGO_PP']['CONFIG']['cookiePartnerNameVar'] = 'partner';
$GLOBALS['NETIGO_PP']['CONFIG']['sessionPartnerNameVar'] = 'partner';
$GLOBALS['NETIGO_PP']['CONFIG']['urlPartnerNameVar'] = 'setaff';

$GLOBALS['NETIGO_PP']['CONFIG']['MAIL'] = array (
	'enabled' 		=> true,
	'mail_function' => 'mail', // smtp, mail, file
	'SMTP' => array (
		'host'			=> 'localhost',
		'port'			=> '25',
		'helo'			=> null,
		'auth'			=> false,
		'login'			=> '',
		'password'		=> '',
	),
);

$GLOBALS['NETIGO_PP']['CONFIG']['BLOCKCTRL']['enable_producers'] = 0;
$GLOBALS['NETIGO_PP']['CONFIG']['defaultPartner']['identity'] = 'system';
$GLOBALS['NETIGO_PP']['CONFIG']['defaultCategoryOptions']['id_producer'] = null;
?>