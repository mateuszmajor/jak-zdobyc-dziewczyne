jQuery(document).ready(function(){
	jQuery(".cookie-info-close").click(function(){
		jQuery(".cookie-info").fadeOut("slow", function() {
			jQuery(".cookie-info").hide()	
			jQuery.cookie('cookie-info', 1, { expires: 365, path: '/' });
		});
		return false;
	});
});
