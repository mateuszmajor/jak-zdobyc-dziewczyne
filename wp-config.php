<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('WP_CACHE', true); //Added by WP-Cache Manager
define(ENABLE_CACHE, true);

// ** MySQL settings - You can get this info from your web host ** //
require_once "wp-config-db.php";

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ph{{,KaT|+eS!GFSB7G:uA[`j`lofZ6q89-hfLP#/=MwNb{z!~V!^3j1zGVJ%fKC');
define('SECURE_AUTH_KEY',  ',WqMu!_l )3^?9yw<]US>qrg1MH[;[*^gk;j;ECa&jUCq`eXnBArh;mEB1 +rfnZ');
define('LOGGED_IN_KEY',    'j?(P=9MC&xOvOY,|)iIbzgJLoOs&+G|j/ZPlv><^$zf*9;?rS-Q7i*GP<@knu9_N');
define('NONCE_KEY',        'yd~*N32!0wE)=2(6!{~aw[8%F<s>9Cw>}z))xMH6^XHLdH:/=bG2-bL|J}+,,Gyd');
define('AUTH_SALT',        'y?FK4H:~&_;9KRvoV+])Zy`zJtj5<hTu9M/(:i/V!GNks!?__~lL.%W&B=)u3r+m');
define('SECURE_AUTH_SALT', 'Y[5F+a?S~/8><a8gz|K,@Lk0Dam#I)a}>db=+%m:x4V?+SOq=,un_9|)}0xm$,mo');
define('LOGGED_IN_SALT',   '+,#oG^>f$YiT:B}PZt<e>Bvlns& 0MblmegtN&2|!ubk0pbTOxClGf]M%^5mPSwM');
define('NONCE_SALT',       '!OL0zZ|?T-$o66QK9#4X<a4Wa^vm|^;=wlQy5,-L;?J|dI+d6RsV`82Gh`j9ZH++');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


define('WP_HOME', 'http://jak-zdobyc-dziewczyne.test');
define('WP_SITEURL', WP_HOME);