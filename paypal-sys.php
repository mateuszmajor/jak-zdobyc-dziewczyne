<?


ini_set("include_path", ini_get("include_path").':' . $_SERVER["DOCUMENT_ROOT"]  . "/class/" );

//ini_set('display_errors','On');
# NETIGO PP BEGIN
include_once('netigoPPintegration/integrationCurl.inc.php');
# NETIGO PP END

include "class/DBSingleton.php";
include "class/platnosci.php";
include "class/DB.php";
include "functions/main.php";

magicDeep();
include "class/ClientPay.php";
include "class/ClientPayDAO.php";


require "config_platnosci.php";
require "prosponder/pro.php";


function paypal_log($txt) {
	$fp = fopen(PAYPAL_LOG, "a");
        fwrite($fp, date("Y-m-d H:i:s", time()) ." ". $txt . "\n");
        fclose($fp);
}

paypal_log("---BEGIN--- : $PAYPAL_URL");

// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';

foreach ($_POST as $key => $value) {
	$value = urlencode(stripslashes($value));
	$req .= "&$key=$value";
}
paypal_log("_POST: $req");

foreach ($_GET as $key => $value) {
	$value = urlencode(stripslashes($value));
	$get_req .= "&$key=$value";
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://$PAYPAL_URL/cgi-bin/webscr");

curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: $PAYPAL_URL", 'Connection: Close'));
// In wamp like environment where the root authority certificate doesn't comes in the bundle, you need
// to download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
// of the certificate as shown below.
curl_setopt($ch, CURLOPT_CAINFO,  $_SERVER["DOCUMENT_ROOT"] . '/cacert.pem');
if( !($res = curl_exec($ch)) ) {
    paypal_log("Got " . curl_error($ch) . " when processing IPN data");
    curl_close($ch);
    exit;
}
else {
}

curl_close($ch);

// assign posted variables to local variables
$txn_type = $_POST['txn_type'];
$item_name = $_POST['item_name'];
$item_number = $_POST['item_number'];
$payment_status = $_POST['payment_status'];
$payment_amount = $_POST['mc_gross'];
$payment_currency = $_POST['mc_currency'];
$txn_id = $_POST['txn_id'];
$receiver_email = $_POST['receiver_email'];
$payer_email = $_POST['payer_email'];
$residence_country=$_POST['residence_country'];
$address_name=$_POST['address_name'];
		
		$session_id = $_GET['session_id'];
		paypal_log("session_id = $session_id");
		if (strcmp ($res, "VERIFIED") == 0) {
	
			paypal_log("VERIFIED, $payer_email, $txn_id, $payment_status, $payment_amount, $payment_currency");
			
			
			if (!empty($session_id))
				$isTransExists=ClientPayDAO::isTransactionExist($session_id);
			else 
				paypal_log("no session id");	

			if ($isTransExists == 1) { // istnieje nowy lub zapisany

				$oClientData = ClientPayDAO::doGetByPayId($session_id);
				$tr_id = $oClientData->idClientPay;
				$tr_fname = $oClientData->clientFirstName;
				$tr_name = $oClientData->clientFullName;
				$tr_address = $oClientData->clientAddress;
				$tr_code = $oClientData->clientPostCode;
				$tr_city = $oClientData->clientCity;
				$tr_nip = $oClientData->clientNIP;
				$tr_email = $oClientData->clientEmail;
				$tr_amount = $oClientData->payAmount;
				$tr_title = $oClientData->payTitle;
				$tr_partner = $oClientData->partner;
				$tr_new = $oClientData->isNew;

				if ($payment_status === 'Completed' and $receiver_email === $RCV_EMAIL and $payment_currency === 'PLN' )  {
		
// OK check the payment_status is Completed	
// check that txn_id has not been previously processed
// OK check that receiver_email is your Primary PayPal email
// OK check that payment_amount/payment_currency are correct
// process payment
			
					paypal_log ("OK: $payer_email $payment_amount $txn_id");
					// zapisz
			
					//duplikat?

					//if ( !ClientPayDAO::isDuplicate($session_id, $txn_id)) {

					if ($txn_type === 'web_accept') { // zwykla platnosc
						paypal_log ("web_accept sess: $session_id");
						//ClientPayDAO::doAddAutoresponder($session_id, "client_pay");
						$oClientData = ClientPayDAO::doGetByPayId($session_id);
						$tr_id = $oClientData->idClientPay;

						$tr_fname = $oClientData->clientFirstName;	
						
						$tr_email = $oClientData->clientEmail;
						$tr_amount=$payment_amount;

						
	$product = $tr_new = $oClientData->payTitle;


	$uwagi = "Klient zapłacił $tr_amount zł";

	$uwagi = iconv("UTF-8","ISO-8859-2", $uwagi);

	$_p = explode(",", $oClientData->partner);
	$oClientData->partner = $_p[0];
	if (!empty($_p[1]))
		$kampania = $_p[1];

   	
	
	
$mail_tpl='templates/mail_po_oplaceniu.'.$tr_new.'.inc';
	if (!file_exists($mail_tpl))
		$mail_tpl='templates/mail_po_oplaceniu.1.inc';

  	include ($mail_tpl);

mail  ( $tr_email, "Dziekuje za oplacenie zamowienia :: JakZdobycDziewczyne.pl", 
$tresc . "

", 
"From: Pawel Grzywocz <support@jakzdobycdziewczyne.pl> \nContent-Type: text/plain;charset=utf-8\n\n");


mail  ( "support@jakzdobycdziewczyne.pl", "Nowe zamowienie PayPal :: JakZdobycDziewczyne.pl", 
"
Id zamowienia: $tr_id
Email: $tr_email
Kwota: $tr_amount
Typ: $tr_new

", 
"From: Pawel Grzywocz <support@jakzdobycdziewczyne.pl> \nContent-Type: text/plain;charset=utf-8\n\n");

paypal_log("$tr_new, $tr_email,  $tr_fname");





if ($tr_new == 14 or $tr_new == 15 or $tr_new == 26 or $tr_new == 27) {

if ($tr_new == 14 or $tr_new == 26 or $tr_new == 27) // cala
	$l = 1613;

if ($tr_new == 15) // cala
	$l = 1614;

addUser($tr_email, $l, $tr_fname, '');

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);
}
else if ($tr_new == 16) {

$l = 1615;
addUser($tr_email, $l, $tr_fname, '');

$l = 1614;
deleteUser($tr_email, $l);

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);
}
else if ($tr_new == 17) {


$l = 1616;
addUser($tr_email, $l, $tr_fname, '');

$l = 1615;
deleteUser($tr_email, $l);

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);
}
else if ($tr_new == 19 or $tr_new == 30 or $tr_new == 35) {

$l = 1868;
addUser($tr_email, $l, $tr_fname, '');

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);		
}
else if ($tr_new == 24 or $tr_new == 33) {
	
$l = 2162;
addUser($tr_email, $l, $tr_fname, '');

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);

	if ($tr_new == 24) {
		$l = 1868;
		addUser($tr_email, $l, $tr_fname, '');
	}		
}
else if ($tr_new == 25) {
$l = 2346;
addUser($tr_email, $l, $tr_fname, '');
	
}
else if ($tr_new == 7) {
$l = 2347;
addUser($tr_email, $l, $tr_fname, '');
	
$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);	
}
else if ($tr_new == 10) {
$l = 2348;
addUser($tr_email, $l, $tr_fname, '');
	
$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);	
}
else if ($tr_new == 1) {
$l = 2349;
addUser($tr_email, $l, $tr_fname, '');

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);	
}
else if ($tr_new == 4) {
$l = 2350;
addUser($tr_email, $l, $tr_fname, '');

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);	
}
else if ($tr_new == 20 or $tr_new == 29) {
$l = 2351;
addUser($tr_email, $l, $tr_fname, '');

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);	
}
else if ($tr_new == 21) {
$l = 2352;
addUser($tr_email, $l, $tr_fname, '');

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);	
}
else if ($tr_new == 22) {
$l = 2353;
addUser($tr_email, $l, $tr_fname, '');

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);	
}
else if ($tr_new == 23) {
$l = 2354;
addUser($tr_email, $l, $tr_fname, '');

$l = 2346;
deleteUser($tr_email, $l);
$l = 2356;
deleteUser($tr_email, $l);	
}
else if ($tr_new == 40) {
$l = 2356;
addUser($tr_email, $l, $tr_fname, '');
	
}
else if ($tr_new == 36 or $tr_new == 37 or  $tr_new == 38) {
$l = 2373;
addUser($tr_email, $l, $tr_fname, '');

}
else if ($tr_new == 39) {
$l = 2374;
addUser($tr_email, $l, $tr_fname, '');
}
else if ($tr_new == 41) {
$l = 2382;
addUser($tr_email, $l, $tr_fname, '');
	
}
else if ($tr_new == 42) {
$l = 2384;
addUser($tr_email, $l, $tr_fname, '');
	
}
else if ($tr_new == 43) {
$l = 2382;
addUser($tr_email, $l, $tr_fname, '');
	
$l = 2384;
deleteUser($tr_email, $l);	
	
}
else if ($tr_new == 44) {
$l = 2388;
addUser($tr_email, $l, $tr_fname, '');

$l = 2384;
deleteUser($tr_email, $l);
}
else if ($tr_new == 45) {
$l = 2389;
addUser($tr_email, $l, $tr_fname, '');

	
$l = 2388;
deleteUser($tr_email, $l);			
}
else if ($tr_new == 28) {
$l = 1868;

addUser($tr_email, $l, $tr_fname, '');
	
$l = 2162;
addUser($tr_email, $l, $tr_fname, '');
	
}
else if ($tr_new == 46) {
	$l = 9946;
	addUser($tr_email, $l, $tr_fname, '');
}
else if ($tr_new == 47) {
	$l = 9947;
	addUser($tr_email, $l, $tr_fname, '');
}
else if ($tr_new == 48) {
	$l = 9948;
	addUser($tr_email, $l, $tr_fname, '');
}
else if ($tr_new == 49) {
	$l = 9949;
	addUser($tr_email, $l, $tr_fname, '');
}
else if ($tr_new == 50) {
	$l = 9950;
	addUser($tr_email, $l, $tr_fname, '');
}
else if ($tr_new == 51) {
	$l = 9951;
	addUser($tr_email, $l, $tr_fname, '');
}
else if ($tr_new == 52) {
	$l = 9952;
	addUser($tr_email, $l, $tr_fname, '');
	
	$l = 9946;
	deleteUser($tr_email, $l);		
}
else if ($tr_new == 53) {
	$l = 9953;
	addUser($tr_email, $l, $tr_fname, '');
}
else if ($tr_new == 54) {
	$l = 9954;
	addUser($tr_email, $l, $tr_fname, '');
}



paypal_log("zapis do listy: $l");

						paypal_log("PP: $uwagi $kampania" . $oClientData->partner . " " . $oClientData->clientEmail . " " . $session_id  . " " . $tr_amount*100);
					
					
						ClientPayDAO::doUpdate($session_id,"paypal.pl",$txn_id,$payment_amount);	

						# zmiana statusu na potwierdzony
						ClientPayDAO::doChangeTransactionStatusById($session_id,"confirmed");

	
					
if ($paypal_typ_test != 1) {

	if (in_array($tr_new, $ebooks)) {
		$netto = $tr_amount / 1.05;
		$ebook = "ebook";
	}
	else 
		$netto = $tr_amount;


	# NETIGO PP BEGIN
		//$GLOBALS['NETIGO_PP']['OBJECT_CURL']->curlAddAffProFee($oClientData->partner, $oClientData->clientEmail, $session_id, $tr_amount*100, "confirmed", $uwagi, $kampania);
		
		$GLOBALS['NETIGO_PP']['OBJECT_CURL']->curlAddAffProFee($oClientData->partner, $oClientData->clientEmail, $ebook . '_' . $session_id, $netto*100, "confirmed", $uwagi, $kampania);

		
		//$GLOBALS['NETIGO_PP']['OBJECT_CURL']->curlChangeCommisionStatus($session_id, 'confirmed');
	# NETIGO PP END
$parameters = "pos_id=". $tr_id . "&amount=". $tr_amount . "&product=". $tr_new . "&email=" .$tr_email . "&session_id=" .$session_id. "&pay_form=".$txn_id;
//if ($residence_country != "US") {
if (1) {
	
	$ch = curl_init();
	 curl_setopt($ch, CURLOPT_URL, "http://soap.netina.pl/paragon_jzd.php");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$platnosci_response = curl_exec($ch);
	curl_close($ch);
	paypal_log("CURL faktura wyslany");
}
else {
	$parameters = "pos_id=". $tr_id . "&amount=". $tr_amount . "&name=JZD:$residence_country:$address_name";
	paypal_log ("POMIJAM PARAGON DLA: $parameters");
	mail  ( "zamowienia@netina.com.pl", "POMIJAM PARAGON :: JakZdobycDziewczyne.pl :: $residence_country", 
$parameters, 
"From: Pawel Grzywocz <support@jakzdobycdziewczyne.pl> \nContent-Type: text/plain;charset=utf-8\n\n");

	$ch = curl_init();
	 curl_setopt($ch, CURLOPT_URL, "http://soap.netina.pl/paragon_pomijam.php");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$platnosci_response = curl_exec($ch);
	curl_close($ch);

}	

	$l = 1810;
	addUser($tr_email, $l, $tr_fname, '');


}


					}
				}	
				else {
			
				//ClientPayDAO::doChangeTransactionStatusById($session_id,"invalid");
				}	 
			}
			else {
				paypal_log ("ERR - nie istniejacy numer sesji: $session_id");
			}
		}
		else if (strcmp ($res, "INVALID") == 0) {
		// log for manual investigation
			paypal_log ("ERR - INVALID");
		}
/*	}
	fclose ($fp);
}*/

paypal_log("---END---");
?>
