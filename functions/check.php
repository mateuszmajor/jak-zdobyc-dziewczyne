<?php

function createSEO($sText)
{
	// pozbywamy si蟰olskich znak������iakrytycznych
	$sText = clearDiacritics($sText);
	// dla przejrzysto¶ci wszystko z ma3ych liter
	$sText = strtolower($sText);
	// wszystkie spacje zamieniamy na my¶lniki
	$sText = str_replace(' ', '-', $sText);
	// usu�zytko co jest niedozwolonym znakiem
	$sText = preg_replace('/[^0-9a-z\-]+/', '', $sText);
	// zredukuj liczb蟭y¶lnik������o jednego obok siebie
	$sText = preg_replace('/[\-]+/', '-', $sText);
	// usuwamy mo¿liwe my¶lniki na pocz±tku i ko������
	$sText = trim($sText, '-');
	return $sText;
}


function checkDateTime($ANo) {
 
 if (ereg('^[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}$', $ANo) == True) {
  $date = explode(':', $ANo);
  if (($date[0] < 24) AND ($date[1] < 60) AND ($date[2] < 60)) return True;
  else return False;
 } else return False;

}

function randomTxt($iL=8) {
 $mr_x = '0123456789abcdefghijklmnopqrstuvwxyz';
 $mr_z = '';
 for ($i = 0; $i < $iL; $i++) {
  $mr_y = $mr_x[mt_rand(0,35)];
  $mr_z = $mr_z.$mr_y;
 }
 return $mr_z;
}

function checkEmail2 ($email1, $email2) {
 // return "err" on false
 
 $w="([a-zA-Z0-9]+|[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])";
 $err=preg_match("^$w(\.$w)*@$w(\.$w)*\.[a-z]{2,4}$^",$email);
 if ($err) return "txt";
 if ($email1<>$email2) return "err";
 return "err";
}

function checkEmail ($email, $null=0) {
 // return "err" on false
 
 if ($email=="" and $null==1) return "txt";
 $w="([a-zA-Z0-9]+|[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])";
 $err=preg_match("^$w(\.$w)*@$w(\.$w)*\.[a-z]{2,4}$^",$email);
 if ($err) return "txt";
 return "err";
}


function checkText ($sText, $iMin, $iMax=255, $sReg=0, $iNull=0) {
 if ($iNull==1 and trim($sText)=="") return 'txt';
 if (strlen(trim($sText))<$iMin or strlen(trim($sText))>$iMax) return "err";
 if ($sReg and !ereg("^$sReg$",$sText)) return "err";
 return "txt";
}

function checkPass ($sPass1, $sPass2, $iMin, $iMax=255, $sReg=0) {
 if ($sPass1<>$sPass2) return "err";
 if (strlen(trim($sPass1))<$iMin or strlen(trim($sPass1))>$iMax) return "err";
 if ($sReg and !ereg("^$sReg$",$sPass1)) return "err";
 return "txt";
}


function checkURL($url,$mode) {
//echo "url: $url. $mode";
 switch ($mode) {
  case "ereg":{
   if (!preg_match('/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\//i', $url)) {	
     $err = 'err';
   } else $err="";
  } break;
	
  case "curl":
   $ch = curl_init();
 	curl_setopt($ch, CURLOPT_URL, "$url");
 	curl_setopt($ch, CURLOPT_HEADER, true);
 	curl_setopt($ch, CURLOPT_NOBODY, true);
 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
 	curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
 	$data = curl_exec($ch);
 	curl_close($ch);
 	preg_match_all("/HTTP\/1\.[1|0]\s(\d{3})/",$data,$matches);
 	$code = end($matches[1]);
	if (!$data or $code==200) $err="err";
 	if ($code==404) $err="";	
  break;
	
  /*case "socket":
   $ch = fsockopen($url, 80, &$errno, &$errstr, 5);
   if ($ch) $err="";
	else $err="err";
  break;		*/			
 }
 return $err;
} 

function formCheck ($aErr, $sErr) {
 
 foreach ($aErr as $sKey=>$sValue) {
   if ($sValue==$sErr) return 0;
 }
 return 1;
}

?>