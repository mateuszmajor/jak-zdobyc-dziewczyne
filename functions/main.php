<?php

function documentRoot() {
 $root=$_SERVER["DOCUMENT_ROOT"];
 $len=strlen($root);
 $last=substr($root,$len-1,$len);
 if ($last=="/") return $root;
 else return $root.="/";
}

function print_pr ($array,$cookie="") {
 if ($cookie<>"" and !isset($_COOKIE[$cookie])) return;
 print "<pre>";
 print_r ($array);
 print "</pre>";
}

function test() {
 print "<h1>TEST</h1>";
}
function magicDeep() {
 if (get_magic_quotes_gpc()) {
  $_POST = stripslashesDeep($_POST);
  $_GET = stripslashesDeep($_GET);
  $_COOKIE = stripslashesDeep($_COOKIE);
 }
}

function stripslashesDeep($value) {
 if (is_array($value)) $value = array_map('stripslashesDeep', $value);
 elseif (!empty($value) && is_string($value)) $value = @stripslashes($value);
 return $value;
}

function trimDeep($value) {
 if (is_array($value)) $value = array_map('trimDeep', $value);
 elseif (!empty($value) && is_string($value)) $value = @trim($value);
 return $value;
}

function iconvdeepUI2($value) {
 if (is_array($value)) $value = array_map('iconvdeepUI2', $value);
 elseif (!empty($value) && is_string($value)) $value = iconv("UTF-8","ISO-8859-2",$value);
 return $value;
}

function iconvdeepI2U($value) {
 if (is_array($value)) $value = array_map('iconvdeepI2U', $value);
 elseif (!empty($value) && is_string($value)) $value = @iconv("ISO-8859-2","UTF-8",$value);
 return $value;
}

?>