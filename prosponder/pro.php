<?

define ("TOKEN", '3027cd3357dfcaf6a89380355105b684ef5dd2d3');
define ("USER_NAME", "netina");
define ("PROSPONDER_URL", 'http://www.prosponder-system.pl/xml.php');


ini_set("include_path", ini_get("include_path").':' . $_SERVER["DOCUMENT_ROOT"]  );


require_once("HTML/Template/IT.php");

function prepareXML($method, $email, $name='', $partnerlink='', $list_nr='',$active="yes") {

	
	$local_tpl = new HTML_Template_IT();
	
	switch($method) {
		case "add":
		
		$local_tpl->loadTemplateFile("prosponder/add.ihtml", true, true);
		$local_tpl->setVariable("EMAIL", $email);
		$local_tpl->setVariable("FIRSTNAME", $name);
		$local_tpl->setVariable("PARTNERLINK", $partnerlink);
		$local_tpl->setVariable("LIST_NR",  $list_nr);
		$local_tpl->setVariable("ACTIVE",  $active);
		

		break;

		case "exists":
		$local_tpl->loadTemplateFile("prosponder/exists.ihtml", true, true);
		$local_tpl->setVariable("EMAIL", $email);
		$local_tpl->setVariable("LIST_NR",  $list_nr);
		break;

		case "update":
		$local_tpl->loadTemplateFile("prosponder/update.ihtml", true, true);
		$local_tpl->setVariable("EMAIL", $email);
		$local_tpl->setVariable("LIST_NR",  $list_nr);
		$local_tpl->setVariable("PARTNERLINK", $partnerlink);
		break;

		case "delete":
		$local_tpl->loadTemplateFile("prosponder/delete.ihtml", true, true);
		$local_tpl->setVariable("EMAIL", $email);
		$local_tpl->setVariable("LIST_NR",  $list_nr);
		
		break;

	}
	$local_tpl->setVariable("TOKEN", TOKEN);
	$local_tpl->setVariable("USER_NAME", USER_NAME);

	
	save_12all_log("$method, $email, $name, $partnerlink, $list_nr");
	

	$local_tpl->parse();
	$xml = $local_tpl->get();
	//save_12all_log($xml);
	return $xml;
}


function sendXML($xml) {

	$ch = curl_init(PROSPONDER_URL);
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
	$result = curl_exec($ch);
	save_12all_log(curl_error($ch));
	if($result === false) {
		return false;
	}	
	else {
		$xml_doc = simplexml_load_string(trim($result));
		//echo 'Status is ', $xml_doc->status, '<br/>';

		save_12all_log ($xml_doc->status);
		if ($xml_doc->status == 'SUCCESS') {
			//print_r($result);
			return $xml_doc->data;
				
		} else {
			return $xml_doc->errormessage;
		}
	}
}


function addUser($email, $list_nr, $name, $partnerlink='',$active="yes" ) {
	if ($list_nr == 189)
		return "";
	//$xml = prepareXML("add", $email ,$name, $partnerlink, $list_nr,$active);
	//$ret = sendXML($xml);
	require_once("config_platnosci.php");

	//najpier dodać na JZDwszyscyklienci
$imie = $name;
$JZDwszyscyklienci["uid"] = "120080";
$JZDwszyscyklienci["key"] = "e3b11a871ebcfa840c77db8d3ba5c5ca";
$kod = MD5($email.$JZDwszyscyklienci["key"]); 
$uid = $JZDwszyscyklienci["uid"];
//dodaj
$url = "http://www.implebot.net/get.php?uid=$uid&email=$email&kod=$kod&imie=$imie"; 
$ch = curl_init(); 
curl_setopt ($ch, CURLOPT_URL, $url); 
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
$file = curl_exec($ch); 
curl_close($ch); 
$komunikat = explode(":", $file); 
$wynik = $komunikat[0]; // OK lub ERROR 
$komunikat = $komunikat[1]; // opis bledu 

$ret = "ADD: JZDwszyscyklienci: $email $wynik $komunikat";

global $implebot;
global $implebot_uids;

// kopiowanie na inną listę
$kod_dst = MD5($email.$implebot[$list_nr]); 
$uid_dst = $implebot_uids[$list_nr];
$url = "http://www.implebot.net/get.php?uid=$uid&email=$email&kod=$kod&cp=$uid_dst&key2=$kod_dst";
$ch = curl_init(); 
curl_setopt ($ch, CURLOPT_URL, $url); 
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
$file = curl_exec($ch); 
curl_close($ch); 
$komunikat = explode(":", $file); 
$wynik = $komunikat[0]; // OK lub ERROR 
$komunikat = $komunikat[1]; // opis bledu 

$ret .= "COPY: $list_nr: $email $wynik $komunikat";
save_12all_log($ret);

}


function deleteUser($email, $list_nr) {
	//$xml = prepareXML("delete", $email ,'', '', $list_nr);
	
	//$ret = sendXML($xml);
	
require_once("config_platnosci.php");
global $implebot;
global $implebot_uids;

$kod = MD5($email.$implebot[$list_nr]); 
$uid = $implebot_uids[$list_nr];
$url = "http://www.implebot.net/get.php?uid=$uid&email=$email&kod=$kod&r=1";
$ch = curl_init(); 
curl_setopt ($ch, CURLOPT_URL, $url); 
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
$file = curl_exec($ch); 
curl_close($ch); 
$komunikat = explode(":", $file); 
$wynik = $komunikat[0]; // OK lub ERROR 
$komunikat = $komunikat[1]; // opis bledu 


$ret .= " DELETE: $email $list_nr: $wynik $komunikat";
save_12all_log($ret);
}

function updatePartnerLink($email, $partnerlink, $list_nr) {

	$xml = prepareXML("exists", $email,"", "", $list_nr);
	$ret = sendXML($xml);
	$id_subscriber = strip_tags($ret->asXML());

	//zmien mu link part
	$xml = prepareXML("update", $id_subscriber,"", $partnerlink, $list_nr);
	$ret = sendXML($xml);
}

define ("FILE_12ALL_LOG", $_SERVER["DOCUMENT_ROOT"] . "/log/pro.log");

function save_12all_log($txt) {
	$fp = fopen(FILE_12ALL_LOG, "a");
        fwrite($fp, date("Y-m-d H:i:s", time()) ." ". $txt . "\n");
        fclose($fp);
}

//$ret = addUser("agnieszka@kochamniemiecki.pl", 128, "Zyta", "szkolenia");
//var_dump($ret);
//updatePartnerLink("agnieszka@linkor.com.pl", "szkolenia", 72);
?>
