<?php

class ClientPayDAO {


 function doGetPlatnosciTransationList($email) {
 
 $sQ="
 SELECT *
 FROM
  client_pay
 WHERE
  (status='confirmed' or status='trial')	
  AND clientEmail=?										
 ";
 //payType='platnosci.pl'   AND
 $oDB=DBSingleton::getInstance();
 
 $aRes=$oDB->getAll($sQ, array(trim($email)), DB_FETCHMODE_ASSOC);
 
 if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
 }
 
 if (empty($aRes) or !is_array($aRes)) return 0;
 return $aRes;
 
 }
 
 function doGetByPayId($sesId) {
 
 $sQ="
 SELECT *
 FROM
  client_pay
 WHERE 
  payId=?											
 ";
 
 $oDB=DBSingleton::getInstance();
 
 $aRes=$oDB->getAll($sQ, array($sesId), DB_FETCHMODE_ASSOC);
 #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  if (is_array($aRes) and count($aRes)==1) {
   return new ClientPay(
	$aRes[0]['idClientPay'],
	
	$aRes[0]['payId'],
	$aRes[0]['payCreateDate'],
	$aRes[0]['payUpdateDate'],
	$aRes[0]['payType'],
	$aRes[0]['payForm'],
	
	$aRes[0]['payTitle'],
	$aRes[0]['payAmount'],
	$aRes[0]['payInvoice'],
	$aRes[0]['clientEmail'],
	$aRes[0]['clientFirstName'],
	$aRes[0]['clientFullName'],
	$aRes[0]['clientAddress'],
	$aRes[0]['clientPostCode'],
	$aRes[0]['clientCity'],
	
	$aRes[0]['clientNIP'],
	
	$aRes[0]['nextInstalmentDate'],
	$aRes[0]['instalmentNoticeCount'],
	
	$aRes[0]['partner'],
	$aRes[0]['status'],
	$aRes[0]['txt_id'],
	$aRes[0]['bonus'],
	$aRes[0]['new'],
	$aRes[0]['rabat']
	
	);
  }
  return 0;
 
 }
 

function doGetByPayIdPayPal($sesId) {
 
 $sQ="
 SELECT *
 FROM
  client_pay_paypal
 WHERE
  payId=?	
  AND (status='new' or status='subscr_signup')										
 ";
 
 $oDB=DBSingleton::getInstance();
 
 $aRes=$oDB->getAll($sQ, array($sesId), DB_FETCHMODE_ASSOC);
 #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  if (is_array($aRes) and count($aRes)>=1) {
   return new ClientPay(
	$aRes[0]['idClientPay'],
	
	$aRes[0]['payId'],
	$aRes[0]['payCreateDate'],
	$aRes[0]['payUpdateDate'],
	$aRes[0]['payType'],
	$aRes[0]['payForm'],
	
	$aRes[0]['payTitle'],
	$aRes[0]['payAmount'],
	$aRes[0]['payInvoice'],
	$aRes[0]['clientEmail'],
	$aRes[0]['clientFirstName'],
	$aRes[0]['clientFullName'],
	$aRes[0]['clientAddress'],
	$aRes[0]['clientPostCode'],
	$aRes[0]['clientCity'],
	
	$aRes[0]['clientNIP'],
	
	$aRes[0]['nextInstalmentDate'],
	$aRes[0]['instalmentNoticeCount'],
	
	$aRes[0]['partner'],
	$aRes[0]['status'],
	$aRes[0]['txt_id'],
	$aRes[0]['bonus'],
	$aRes[0]['new'],
	$aRes[0]['rabat']
	
	);
  }
  return 0;
 
 }
 

 function doGetByClientPayId($payId) {
 
 $sQ="
 SELECT *
 FROM
  client_pay
 WHERE
  idClientPay=?											
 ";
 
 $oDB=DBSingleton::getInstance();
 
 $aRes=$oDB->getAll($sQ, array($payId), DB_FETCHMODE_ASSOC);
 #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  if (is_array($aRes) and count($aRes)==1) {
   return new ClientPay(
	$aRes[0]['idClientPay'],
	
	$aRes[0]['payId'],
	$aRes[0]['payCreateDate'],
	$aRes[0]['payUpdateDate'],
	$aRes[0]['payType'],
	$aRes[0]['payForm'],
	
	$aRes[0]['payTitle'],
	$aRes[0]['payAmount'],
	$aRes[0]['payInvoice'],
	$aRes[0]['clientEmail'],
	$aRes[0]['clientFirstName'],
	$aRes[0]['clientFullName'],
	$aRes[0]['clientAddress'],
	$aRes[0]['clientPostCode'],
	$aRes[0]['clientCity'],
	
	$aRes[0]['clientNIP'],
	
	$aRes[0]['nextInstalmentDate'],
	$aRes[0]['instalmentNoticeCount'],
	
	$aRes[0]['partner'],
	$aRes[0]['status'],
	$aRes[0]['txn_id'],
$aRes[0]['bonus'],
$aRes[0]['new'],
$aRes[0]['rabat']
	
	);
  }
  return 0;
 
 }
 
 function doAdd(ClientPay $oUP) {
  #print_pr($oUP);
 $aQ=array(
 $oUP->payId,
 $oUP->payCreateDate,
 $oUP->payTitle,
 $oUP->payInvoice,
   
 $oUP->clientEmail,
 $oUP->clientFirstName,
 $oUP->clientFullName,
 $oUP->clientAddress,
 $oUP->clientPostCode,
 $oUP->clientCity,

 $oUP->clientNIP,
 
 $oUP->partner,
 $oUP->status,
$oUP->isNew,
$oUP->rabat
 );
 
 $sQ="
 INSERT INTO
  client_pay
 SET
  payId=?,
  payCreateDate=?,
  payTitle=?,	
  payInvoice=?,	
	
  clientEmail=?,
  clientFirstName=?,
  clientFullName=?,
  clientAddress=?,
  clientPostCode=?,
  clientCity=?,
  
  clientNIP=?,
	
  partner=?,	
  status=?,
  new=?,
  rabat=?
  
 ";
 
 $oDB=DBSingleton::getInstance();
 $oRes=$oDB->query($sQ, $aQ); #print_pr($oRes);
 
 if (PEAR::isError($oRes)) {
  print ($oRes->getDebugInfo());
  return -1;
 }
 
 return 1; 
 
 }
 
 /*
function doAddPayPal(ClientPay $oUP) {
  #print_pr($oUP);
 $aQ=array(
 $oUP->payId,
 $oUP->payCreateDate,
 $oUP->payTitle,
 $oUP->payInvoice,
   
 $oUP->clientEmail,
 $oUP->clientFirstName,
 $oUP->clientFullName,
 $oUP->clientAddress,
 $oUP->clientPostCode,
 $oUP->clientCity,

 $oUP->clientNIP,
 
 $oUP->partner,
 $oUP->status,
 $oUP->payAmount,
 $oUP->txn_id,
 $oUP->bonus,
 $oUP->isNew

 );
 
 $sQ="
 INSERT INTO
  client_pay_paypal
 SET
  payId=?,
  payCreateDate=?,
  payTitle=?,	
  payInvoice=?,	
	
  clientEmail=?,
  clientFirstName=?,
  clientFullName=?,
  clientAddress=?,
  clientPostCode=?,
  clientCity=?,
  
  clientNIP=?,
	
  partner=?,	
  status=?,
  payAmount=?,
  txn_id=?,
  bonus=?,	
  new=?	
 ";
 
 $oDB=DBSingleton::getInstance();
 $oRes=$oDB->query($sQ, $aQ); 


ob_start();
print_pr($oRes);
$txt = ob_get_contents();
ob_end_clean();

 
 if (PEAR::isError($oRes)) {
  #print ($oRes->getDebugInfo());
  return -1;
 }
 
 return 1; 
 
 }
 
*/

 function doUpdate($sId,$type,$form,$amount) {
 
 $aQ=array(
  $type,
  $form,	
  $amount,
  $sId	
 );
 
 $sQ="
 UPDATE
  client_pay
 SET
  payType=?,	
  payForm=?,	
  payAmount=?
 WHERE
  payId=?
 ";
 
 $oDB=DBSingleton::getInstance();
 $oRes=$oDB->query($sQ, $aQ); #print_pr($oRes);
 

 if (PEAR::isError($oRes)) {
  #print ($oRes->getDebugInfo());
  return -1;
 }
 
 return 1; 
 
 }
 
function doUpdatePayPal($sId,$amount,$txn_id) {
 
 $aQ=array(
  $txn_id,
  $amount,
  $sId	
 );
 
 $sQ="
 UPDATE
  client_pay_paypal
 SET
  txn_id=?,
  payAmount=?
 WHERE
  payId=?
 ";
 
 $oDB=DBSingleton::getInstance();
 $oRes=$oDB->query($sQ, $aQ); #print_pr($oRes);
 

ob_start();
print_pr($oRes);
$txt = ob_get_contents();
ob_end_clean();
paypal_log2($txt);


 if (PEAR::isError($oRes)) {
  #print ($oRes->getDebugInfo());
  return -1;
 }
 
 return 1; 
 
 }
 
 
 function doGetTransactionStatus($sesId) {
 
  $sQ="
   SELECT status
	FROM client_pay
	WHERE payId=?
  ";
	
  $oDB=DBSingleton::getInstance(); #print_pr ($oDB);

  $aRes=$oDB->getAll($sQ, array($sesId), DB_FETCHMODE_ASSOC); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
  if (is_array($aRes) and count($aRes)==1) {
	//print_r ($aRes);
   return $aRes[0]['status'];
  }
  return 0;
 }
 
 function doGetEmailNameByPayId($payId, $source="client_pay") {
 
  $sQ="
   SELECT clientEmail,clientFirstName, partner
	FROM $source
	WHERE payId=?
  ";
	
  $oDB=DBSingleton::getInstance(); #print_pr ($oDB);

  $aRes=$oDB->getAll($sQ, array($payId), DB_FETCHMODE_ASSOC); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }

  if (is_array($aRes) and count($aRes)>=1) {
   return $aRes[0];
  }
  return 0;
 }
 
 function isTransactionExist($sesId, $table="client_pay") {
 
  $sQ="
   SELECT payId
	FROM $table
	WHERE payId=?
  ";
	
  $oDB=DBSingleton::getInstance(); #print_pr ($oDB);

  $aRes=$oDB->getAll($sQ, array($sesId), DB_FETCHMODE_ASSOC); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
    #die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  if (is_array($aRes) and count($aRes)>0) return 1;
  return 0;
 }
 
function isTransactionNewExist($sesId) {
 
  $sQ="
   SELECT payId
	FROM client_pay_paypal
	WHERE payId=?
	AND ( status='new' or status='subscr_signup')
  ";
	
  $oDB=DBSingleton::getInstance(); #print_pr ($oDB);

  $aRes=$oDB->getAll($sQ, array($sesId), DB_FETCHMODE_ASSOC); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  if (is_array($aRes) and count($aRes)>0) return 1;
  return 0;
 }


function isTransactionFirstPayment($sesId) {
 
  $sQ="
   SELECT *
	FROM client_pay_paypal
	WHERE payId=?
	AND ( status='subscr_payment')
  ";
	
  $oDB=DBSingleton::getInstance(); #print_pr ($oDB);

  $aRes=$oDB->getAll($sQ, array($sesId), DB_FETCHMODE_ASSOC); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  if (is_array($aRes) and count($aRes)==1) return 1;
  return 0;
 }



function isDuplicate($session_id, $txn_id) {
 
  $sQ="
   SELECT payId
	FROM client_pay_paypal
	WHERE payId=?
	AND txn_id=?
  ";
	
  $oDB=DBSingleton::getInstance(); #print_pr ($oDB);

  $aRes=$oDB->getAll($sQ, array($session_id, $txn_id), DB_FETCHMODE_ASSOC); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  if (is_array($aRes) and count($aRes)>0) return 1;
  return 0;
 }



 function doChangeTransactionStatusById($payId, $status) {
 
  $sQ="
   UPDATE client_pay
   SET status=?
	WHERE payId=?
  ";
	
  $oDB=DBSingleton::getInstance(); #print_pr ($oDB);

  $aRes=$oDB->query($sQ, array($status,$payId)); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  return $oDB->affectedRows();
 }
 
function doChangeTransactionStatusByIdPayPal($payId, $status) {
  
  $sQ="
   UPDATE client_pay_paypal
   SET status=?
	WHERE payId=?
	AND status='new'
  ";
	
  $oDB=DBSingleton::getInstance(); #print_pr ($oDB);

  $aRes=$oDB->query($sQ, array($status,$payId)); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  return $oDB->affectedRows();
 }
 

 function doChangeInstalmentDate($payId, $bonus=0) {
 
	if ($bonus == 0) {
  $sQ="
   update client_pay
   SET
    client_pay.nextInstalmentDate=DATE_ADD(client_pay.payCreateDate,INTERVAL 1 MONTH)
    WHERE payId=?
  ";
	}
else
	$sQ="
   update client_pay
   SET
    client_pay.nextInstalmentDate=DATE_ADD(client_pay.payCreateDate,INTERVAL 15 DAY)
    WHERE payId=?
  ";
	
  $oDB=DBSingleton::getInstance(); #print_pr ($oDB);

  $aRes=$oDB->query($sQ, array($payId)); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  return $oDB->affectedRows();
 }
 
 
 function doChangeInstalmentNoticeCount($email,$payId) {
 
  $sQ="
   UPDATE client_pay
   SET
	 instalmentNoticeCount=-1
   WHERE
	 payId <> ?
	 AND clientEmail=?
	 AND payType='platnosci.pl'
	 AND status='confirmed'
  ";
	
  $oDB=DBSingleton::getInstance(); 

  $aRes=$oDB->query($sQ, array($payId,$email)); 
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  return $oDB->affectedRows();
 }
 
 function doChangePayUpdateDate($email,$payId) {
 
  $sQ="
   UPDATE client_pay
   SET
	 payUpdateDate=CURDATE()
   WHERE
	 idClientPay =?
	 AND clientEmail=?
	 AND payType='paypal.pl'
	 AND status='confirmed'
  ";
	
  $oDB=DBSingleton::getInstance(); 

  $aRes=$oDB->query($sQ, array($payId,$email)); 
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  return $oDB->affectedRows();
 }
 
/*
function doChangePayAmount($email,$payId) {
 
  $sQ="
   UPDATE client_pay
   SET
	 payAmount = payAmount + 127
   WHERE
	 idClientPay =?
	 AND clientEmail=?
	 AND payType='paypal.pl'
	 AND status='confirmed'
  ";
	
  $oDB=DBSingleton::getInstance(); 

  $aRes=$oDB->query($sQ, array($payId,$email)); 
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  return $oDB->affectedRows();
 }	
*/
 function doAddAutoresponder($session_id, $source='client_pay_paypal', $list_nr=128) {
  global $mailing_list;	
	
  $aClient=self::doGetEmailNameByPayId($session_id, $source);

  if (is_array($aClient) and $mailing_list==1) {
	
	//echo "wysy?am maila $ret";

	$_p = explode(",", $aClient['partner']);
	$aClient['partner'] = $_p[0];
	require "prosponder/pro.php";
	addUser($aClient['clientEmail'], $list_nr, $aClient['clientFirstName'], $aClient['partner']);

	}

	
 }


  function usedRabat($rabat,$source) {
 
  if ($source == "fastdeal.pl")
	$sQ="select * from client_pay where rabat like '$rabat,%' and status='confirmed' and payType='$source'";
  else
	$sQ="select * from client_pay where rabat='$rabat' and status='confirmed' and payType='$source'";
  $oDB=DBSingleton::getInstance(); 
  $aRes=$oDB->getAll($sQ, array(), DB_FETCHMODE_ASSOC); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  if (is_array($aRes) and count($aRes)>0) 
	return true;
  else
   	return false;

  
 }
/*
 
  function countBonus() {
 
  $sQ="select * from client_pay where status='confirmed' and bonus = 1";
	
  $oDB=DBSingleton::getInstance(); 
  $aRes=$oDB->getAll($sQ, array(), DB_FETCHMODE_ASSOC); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  if (is_array($aRes) and count($aRes)>0) return count($aRes);
  return 0;

  
 }
*/
	
  function lastInsertID() {
 
  $sQ="select LAST_INSERT_ID() as last from client_pay_paypal";
	
  $oDB=DBSingleton::getInstance(); 
  $aRes=$oDB->getAll($sQ, array(), DB_FETCHMODE_ASSOC); #print_pr ($aRes);
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  if (is_array($aRes) and count($aRes)>0) 
	return $aRes[0]['last'];
  else
   	return 0;

  
 }

/*
  function doSetBonus($email,$payId) {
 
  $sQ="
   UPDATE client_pay
   SET
	 bonus = 1
   WHERE
	 idClientPay =?
	 AND clientEmail=?
	 AND payType='paypal.pl'
	 AND status='confirmed'
  ";
	
  $oDB=DBSingleton::getInstance(); 

  $aRes=$oDB->query($sQ, array($payId,$email)); 
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  return $oDB->affectedRows();
 }	
*/

function doSetTxnId($payId, $txn_id) {
 
  $sQ="
   UPDATE client_pay
   SET
	 txn_id = $txn_id
   WHERE
	 idClientPay =?
	 AND payType='paypal.pl'
	 AND status='confirmed'
  ";
	
  $oDB=DBSingleton::getInstance(); 

  $aRes=$oDB->query($sQ, array($payId,$email)); 
	
  if (PEAR::isError($aRes)) {
   return -1;
   # die($res->getMessage() . ' | ' . $res->getDebugInfo());
  }
	
  return $oDB->affectedRows();
 }	

}

?>