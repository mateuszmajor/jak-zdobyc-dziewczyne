<?php

class ClientPay {
 var $idClientPay;
 
 var $payId;
 var $payCreateDate;
 var $payUpdateDate;
 var $payType;
 var $payForm;
 var $payTitle;
 var $payAmount;
 var $payInvoice;

 var $clientEmail;
 var $clientFirstName;
 var $clientFullName;
 var $clientAddress;
 var $clientPostCode;
 var $clientCity;

 var $clientNIP;
 
 var $nextInstalmentDate;
 var $instalmentNoticeCount;
 
 var $partner;
 var $status;
 var $txn_id;
 var $bonus;
 var $isNew;	
 var $rabat;
 
 function ClientPay(
 $idClientPay=null,
 $payId=null,
 
 $payCreateDate=null,
 $payUpdateDate=null,
 $payType=null,
 $payForm=null,
 $payTitle=null,
 $payAmount=null,
 $payInvoice=null,
 
 $clientEmail=null,
 $clientFirstName=null,
 $clientFullName=null,
 $clientAddress=null,
 $clientPostCode=null,
 $clientCity=null,
 
 $clientNIP=null,
 
 $nextInstalmentDate=null,
 $instalmentNoticeCount=null,
 
 $partner=null,
 $status=null,
 $txn_id=null,
 $bonus=null,$isNew=0,$rabat=null) {
 
 $this->idClientPay=$idClientPay;
 $this->payId=$payId;
 
 $this->payCreateDate=$payCreateDate;
 $this->payUpdateDate=$payUpdateDate;
 $this->payType=$payType;
 $this->payForm=$payForm;
 $this->payTitle=$payTitle;
 $this->payAmount=$payAmount;
 $this->payInvoice=$payInvoice;

 $this->clientEmail=$clientEmail;
 $this->clientFirstName=$clientFirstName;
 $this->clientFullName=$clientFullName;
 $this->clientAddress=$clientAddress;
 $this->clientPostCode=$clientPostCode;
 $this->clientCity=$clientCity;
 
 $this->clientNIP=$clientNIP;
 
 $this->nextInstalmentDate=$nextInstalmentDate;
 $this->instalmentNoticeCount=$instalmentNoticeCount;
 
 $this->partner=$partner;
 $this->status=$status;
 $this->txn_id=$txn_id;
 $this->bonus=$bonus;
 $this->isNew=$isNew;	
$this->rabat=$rabat;	 
}
}

?>