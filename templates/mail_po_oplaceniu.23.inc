<?

$tresc = '
Jak zdobyć dziewczynę?
___________________________________________________________

Witaj ' . $tr_fname . ',

Dziękuję za udział w trzeciej i czwartej części szkolenia:

"Tranzycje, czyli Przejścia ze zwykłej rozmowy do Flirtu".

Trzeci moduł zawiera wyjaśnienia poszczególnych technik flirtu i droczenia się 
z dziewczyną, a także humor niewerbalny i nietypowe pomysły na randki.

Zwróć również szczególną uwagę na moduł 4 o budowaniu poczucia własnej wartości
i zrób ćwiczenia. Rób je codziennie, ponieważ pewność siebie i poczucie wartości
to zawsze POZIOM.

Nigdy nie jest na stałym poziomie. To ciągły proces i ciągła świadoma praca nad
swoimi myślami.

To jest jak utrzymywanie zdrowej sylwetki albo masy mięśniowej. Jeśli nie
dostarczysz sobie czegoś pozytywnego, to negatywne myśli i nawyki zwyciężą. Nie
pozwól na to! Praktykuj zdobytą wiedzę każdego jednego dnia tak długo dopóki nie
zbudujesz nowych, budujących nawyków.

Na dole ostatniego modułu są również wszystkie materiały w Wordzie do pobrania,
które rozdaliśmy na szkoleniu i uzupełnialiśmy podczas ćwiczeń z samooceny.

Zobacz nagrania i pobierz materiały ze szkolenia tutaj: 

http://pewnoscsiebiew90dni.pl/kurs/tranzycje-modul-nr-3-i-4-techniki-flirtu-humor-niewerbalny-i-pomysly-na-randki-oraz-budowanie-poczucia-wartosci

Twoje hasło:

tran69st

Skopiuj dokładnie bez spacji z przodu lub z tyłu.

Zobacz także trzeci poszkoleniowy webinar na temat opowiadania historii w interesujący sposób tutaj: 

http://pewnoscsiebiew90dni.pl/kurs/webinary/jak-opowiadac-historie-aby-zainteresowac-soba-dziewczyne

Pozdrawiam serdecznie,
Paweł Grzywocz
Jak-Zdobyc-Dziewczyne.pl
PewnoscSiebieW90Dni.pl 

P.S.

Proszę, daj mi znać, co myślisz o tym szkoleniu na stronie:
http://Jak-zdobyc-dziewczyne.pl/moja-historia/opinie  
W nagrodę wyślę Ci niepublikowany nigdzie raport: 
"7 zachowań, utrzymujących wysokie zainteresowanie kobiety 
pod jednych dachem." 

To bardzo ważne, ponieważ dzięki temu sam podsumujesz, czego się nauczyłeś 
i czy wdrażasz materiał w życie, a dla mnie jest to cenna informacja o tym, 
na czym się skupić, aby Cię bardziej motywować oraz na czym się bardziej skupiać. 

Wielki dzięki za Twoją współpracę. Do następnego! 


';



?>